/* eslint-disable jsx-a11y/click-events-have-key-events */
import './styles.css';
import { useState, useRef, useEffect } from 'react';
import { profileImage, AlertSvg, chatPNG, EoxegenLogoColour, UserProfile, Settings, Signout, UpdateProfile } from '@assets/index';
import { Link, useLocation } from 'react-router-dom';
import { motion } from 'framer-motion';
import { Card } from 'react-bootstrap';
import { useOnClickOutside } from 'usehooks-ts';
import { useAuth } from '@hooks/useAuth';
import { UseAuthType } from 'Types/types';

/** make sure to put svg in userMenu */

const Header = () => {
    const [userPopup, setUserPopup] = useState(false);
    const [notificationPopup, setNotificationPopup] = useState(false);
    const [active, setActive] = useState('home');
    const notificationRef = useRef(null);
    const profileRef = useRef(null);
    const { logout }: UseAuthType = useAuth();
    const location = useLocation();

    useEffect(() => {
        setActive(location.pathname.slice(1));
    }, [location]);

    const handleClickOutside = () => {
        if (userPopup) {
            setUserPopup(false);
        } else if (notificationPopup) {
            setNotificationPopup(false);
        }
    };

    const handleuserProfile = () => {
        alert('hi from userprofile');
    };
    const handleSettings = () => {
        alert('hi from settings');
    };
    const handleSignout = () => {
        if (logout) {
            logout();
        }
    };

    const userMenu = {
        'User Profile': { Icon: UpdateProfile, handler: handleuserProfile },
        Settings: { Icon: Settings, handler: handleSettings },
        'Sign Out': { Icon: Signout, handler: handleSignout },
    };

    useOnClickOutside(notificationRef, handleClickOutside);
    useOnClickOutside(profileRef, handleClickOutside);
    return (
        <motion.main initial={{ scale: 0.994 }} animate={{ scale: 1 }} id="app-header">
            <section>
                <Link to="/home">
                    <EoxegenLogoColour />
                </Link>
            </section>
            <nav className="pages-nav">
                <ul>
                    <Link to="/home" onClick={() => setActive('home')} data-active={active === 'home' ? active : null}>
                        Home
                    </Link>
                    <Link to="/claims" onClick={() => setActive('claims')} data-active={active === 'claims' ? active : null}>
                        Claims
                    </Link>
                    <Link to="/policy" onClick={() => setActive('policy')} data-active={active === 'policy' ? active : null}>
                        Policy
                    </Link>
                    <Link to="/providers" onClick={() => setActive('providers')} data-active={active === 'providers' ? active : null}>
                        Providers
                    </Link>
                    {/* <Link to="/fitness" onClick={() => setActive('fitness')} data-active={active === 'fitness' ? active : null}>
                        Fitness
                    </Link>
                    <Link to="/gamezone" onClick={() => setActive('gamezone')} data-active={active === 'gamezone' ? active : null}>
                        GameZone
                    </Link> */}
                </ul>
            </nav>
            <section className="pages-header">
                <Link to="/home">
                    <img src={chatPNG} height={24} width={24} alt="chat" />
                </Link>
                <i
                    style={{ cursor: 'pointer' }}
                    role="button"
                    onKeyDown={() => {}}
                    tabIndex={0}
                    onClick={() => {
                        setNotificationPopup((np) => !np);
                        setUserPopup(false);
                    }}
                >
                    <AlertSvg />
                </i>
                <i
                    style={{ cursor: 'pointer' }}
                    role="button"
                    onKeyDown={() => {}}
                    tabIndex={0}
                    onClick={() => {
                        setUserPopup((up) => !up);
                        setNotificationPopup(false);
                    }}
                >
                    <UserProfile />
                </i>
                {notificationPopup && (
                    <motion.div ref={notificationRef} className="notification-popup rounded-1r">
                        <Card className="border-0">
                            <Card.Body>
                                <section className="notificationHeader">
                                    <div>
                                        <strong>Notifications</strong>
                                    </div>
                                    <div className="mark-all">
                                        <strong>Mark all as read</strong>
                                    </div>
                                </section>
                            </Card.Body>
                        </Card>
                    </motion.div>
                )}
                {userPopup && (
                    <motion.div ref={profileRef} className="user-dropdown-popup rounded-1r">
                        <Card className="border-0">
                            <Card.Body>
                                <section className="user-head">
                                    <img src={profileImage} width={100} height={100} alt="profilePic" />
                                    <div className="user-details">
                                        <h2 style={{ fontWeight: '600' }}>Rahul Saxena</h2>
                                        <p>Senior Manager</p>
                                        <p>rahulsaxena@company.com</p>
                                    </div>
                                </section>
                                <ul className="uluser" style={{ listStyle: 'none', padding: 0 }}>
                                    {Object.entries(userMenu).map(([key, value]) => {
                                        return (
                                            // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
                                            <li key={`userprofile__${key}`} onClick={value.handler}>
                                                <hr />
                                                <value.Icon />
                                                &nbsp;&nbsp;&nbsp; {key}
                                            </li>
                                        );
                                    })}
                                </ul>
                            </Card.Body>
                        </Card>
                    </motion.div>
                )}
            </section>
        </motion.main>
    );
};

export default Header;
