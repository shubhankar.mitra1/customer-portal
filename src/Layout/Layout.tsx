import Header from './Header/Header';
import './styles.css';

type TreeProp = {
    children: JSX.Element;
};

const Layout = ({ children }: TreeProp) => {
    return (
        <div className="background">
            <Header />
            {children}
        </div>
    );
};

export default Layout;
