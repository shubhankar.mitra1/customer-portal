import { ApexOptions } from 'apexcharts';
import { AxiosResponse } from 'axios';

export type UseAuthType = {
    login?: (args: object) => void;
    user?: object | null;
    logout?: () => void;
};

export type ClaimsCardType = {
    name?: string;
    date?: string;
    context?: string;
    status?: string;
    amount?: string;
    displayPic?: any;
};

export type BarChartType = {
    options?: object;
    series?: ApexOptions['series'];
    width?: string;
    height?: string;
    plotOptions?: ApexOptions['plotOptions'];
};

export type ClaimsSummaryCardType = {
    name?: string;
    location?: string;
    country?: string;
    contact?: string;
    dateofservice?: string;
    relation?: string;
    reason?: string;
    claimtype?: string;
    coveragetype?: string[];
    networktype?: string;
    status?: string;
    hraclaimno?: string;
    amountbilled?: string;
    amountdeducted?: string;
    plainpaid?: string;
    paymentmode?: string;
    paymentdetails?: string;
};

export type PlanBalancesCardType = {
    name?: string;
    figure?: ApexOptions;
    table: object;
};

export type DocumentCardType = {
    name?: string;
    companyname?: string;
    issuedate?: string;
};

export type PolicyCardType = {
    policynumber?: string;
    scheme?: string;
    amount?: string;
    renewdate?: string;
    policystartdate?: string;
    policyterm?: string;
    membercount?: number;
    handleView?: (e) => void;
    active?: boolean | string;
};

export type ProviderCardType = {
    name?: string;
    rating?: number;
    image?: any;
    networkstatus?: string;
    address?: string;
    contact?: string;
    distance?: string;
    lat?: string;
    long?: string;
    url?: string;
};

export type ApiResultData = AxiosResponse<any, any>;
export type ApiErrorResultData = { ok: boolean; data: AxiosResponse<any, any>; error: null } | { ok: boolean; data: null; error: any | unknown };

export type ClaimIntimationType = {
    memberNo: string;
    member: string;
    providerName: string;
    admissionDate: string;
    dischargeDate: string;
    hospitalContact: string;
    estimatedAmount: string;
    diseaseName: string;
    doctorName: string;
    claimCategory: string;
};

export type FamilyMemberDataType = {
    memberShipNo: string;
    dependantName: string;
};

export type ExtraValues = {
    memberShipNo?: string;
    file?: string;
    documentType?: string;
    pageNo?: number;
    claimID?: string;
    relation?: string;
    dateType?: string;
    pinCode?: string;
    status?: string;
    claimType?: string;
};
