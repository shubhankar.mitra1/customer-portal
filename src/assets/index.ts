import useProfilePng from './user-profile.png';
import chat from './chat.png';
import ssoBackground from './sso-background.cff9afa0.png';
import userCard from './user.png';
import familyFloater from './family-floater.png';
import providerImage from './provider-image.webp';

export const chatPNG = chat;
export const profileImage = useProfilePng;
export const ssoBackgroundPNG = ssoBackground;
export const UserCard = userCard;
export const familyFloaterPNG = familyFloater;
export const providerImageSVG = providerImage;

export { UserProfile } from './userProfile';
export { AlertSvg } from './alert';
export { EoxegenLogo } from './eOxegenLogo';
export { EoxegenLogoColour } from './eOxegenLogoColour';
export { UpdateProfile } from './updateProfile';
export { Settings } from './settings';
export { Signout } from './signout';
export { AdobeIcon } from './adobeIcon';
