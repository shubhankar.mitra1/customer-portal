export const INPROCESS = 'IN PROCESS';
export const REJECTED = 'REJECTED';
export const PAID = 'PAID';

export const CLAIM_FORM_HEADERS = {
    'Member Ship No': 'Membership No.',
    'Dependant Name': 'Name',
    'Member Dob': 'DOB',
};

export const DURATION = {
    '1 MONTH': '1M',
    '3 MONTHS': '3M',
    '6 MONTHS': '6M',
    '9 MONTHS': '9M',
    '1 YEAR': '1Y',
};
