import './App.css';
import { lazy, Suspense } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Spinner from '@components/Spinner/Spinner';
import { ProtectedRoute } from '@components/ProtectedRoute/ProtectedRoute';
import { AuthProvider } from '@hooks/useAuth';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';

const Home = lazy(() => import('@pages/Home/Home'));
const Layout = lazy(() => import('@layout/Layout'));
const Login = lazy(() => import('@pages/Login/Login'));
const Claims = lazy(() => import('@pages/Claims/Claims'));
const Policy = lazy(() => import('@pages/Policy/Policy'));
const Providers = lazy(() => import('@pages/Providers/Providers'));
const Fitness = lazy(() => import('@pages/Fitness/Fitness'));
const GameZone = lazy(() => import('@pages/GameZone/GameZone'));

function App() {
    return (
        <main>
            <Routes>
                <Route
                    path="/home"
                    element={
                        <ProtectedRoute>
                            <Layout>
                                <Suspense fallback={<Spinner suspense />}>
                                    <Home />
                                </Suspense>
                            </Layout>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/claims"
                    element={
                        <ProtectedRoute>
                            <Layout>
                                <Suspense fallback={<Spinner suspense />}>
                                    <Claims />
                                </Suspense>
                            </Layout>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/policy"
                    element={
                        <ProtectedRoute>
                            <Layout>
                                <Suspense fallback={<Spinner suspense />}>
                                    <Policy />
                                </Suspense>
                            </Layout>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/providers"
                    element={
                        <ProtectedRoute>
                            <Layout>
                                <Suspense fallback={<Spinner suspense />}>
                                    <Providers />
                                </Suspense>
                            </Layout>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/fitness"
                    element={
                        <ProtectedRoute>
                            <Layout>
                                <Suspense fallback={<Spinner suspense />}>
                                    <Fitness />
                                </Suspense>
                            </Layout>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/gamezone"
                    element={
                        <ProtectedRoute>
                            <Layout>
                                <Suspense fallback={<Spinner suspense />}>
                                    <GameZone />
                                </Suspense>
                            </Layout>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/"
                    element={
                        <Suspense fallback={<Spinner suspense />}>
                            <Login />
                        </Suspense>
                    }
                />
                <Route
                    path="/*"
                    element={
                        <ProtectedRoute>
                            <Suspense fallback={<Spinner suspense />}>
                                <h1>Not Found</h1>
                            </Suspense>
                        </ProtectedRoute>
                    }
                />
            </Routes>
        </main>
    );
}

const queryClient = new QueryClient();

const WrappedApp = () => {
    return (
        <QueryClientProvider client={queryClient}>
            <BrowserRouter>
                <AuthProvider>
                    <App />
                </AuthProvider>
            </BrowserRouter>
            <ReactQueryDevtools initialIsOpen position="bottom-right" />
        </QueryClientProvider>
    );
};

export default WrappedApp;
