import cogoToast from 'cogo-toast';
import { ExtraValues } from './Types/types';

export const getPayLoad = (endpoint: string, extraValues: ExtraValues | undefined): object | undefined => {
    switch (endpoint) {
        case 'policy': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }
        case 'recentClaims': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                claimCategory: 'REIMBURSEMENT',
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }
        case 'recentPreauth': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                claimCategory: 'CASHLESS',
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }
        case 'familyMembers': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }
        case 'fetchSelectedMemberDetails': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                selectMemberNo: extraValues?.memberShipNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }

        case 'fetchServiceType': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                dbField: 'SERVICE_TYPE',
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }

        case 'fetchClaimType': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                dbField: 'CLAIM_TYPE',
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }
        case 'fetchClaimSubType': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                dbField: 'CLAIM_SUB_TYPE',
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }
        case 'claimsList': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
                pageNo: extraValues?.pageNo,
                pageSize: 10,
            };
        }
        case 'getByClaimType': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }
        case 'getByTreatment': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }
        case 'getByGender': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }
        case 'getByLocation': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }
        case 'getByAgeGroup': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }
        case 'getByDurationOfStay': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }
        case 'providers': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
                pageSize: 10,
                pageNo: extraValues?.pageNo,
                pinCode: extraValues?.pinCode,
                hospitalName: '',
            };
        }
        case 'populateDocumentType': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
                dbField: 'HC_DOC_TYPE',
                parentParamID: 'IPD',
            };
        }
        case 'uploadDocument': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
                entryBy: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                file: extraValues?.file,
                documentType: extraValues?.documentType,
                claimID: extraValues?.claimID,
            };
        }
        case 'memberclaimannualmade': {
            return {
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
            };
        }
        case 'viewDoc': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
                claimID: extraValues?.claimID,
            };
        }
        case 'memberclaimmadeonpoliciesrelation': {
            return {
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
            };
        }
        case 'memberclaimmadepoliciestotalamountgr': {
            return {
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                relation: extraValues?.relation,
                dateType: extraValues?.dateType,
            };
        }
        case 'memberclaimmadepoliciestotalamount': {
            return {
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                relation: extraValues?.relation,
                dateType: extraValues?.dateType,
            };
        }

        case 'memberclaimsprovider': {
            return {
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                relation: extraValues?.relation,
                pageSize: 10,
                pageNo: extraValues?.pageNo,
                status: extraValues?.status,
                claimCategory: extraValues?.claimType,
            };
        }
        case 'populatestatus': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
                dbField: 'CLAIM_STAGE',
            };
        }
        case 'membercoveredpolicy': {
            return {
                memberNo: JSON.parse(sessionStorage.getItem('user') || '{}')?.actualMemberNo,
                tokenID: JSON.parse(sessionStorage.getItem('user') || '{}')?.tokenID,
            };
        }

        default: {
            cogoToast.error('Payload.ts : Wrong endpoint, please check payload');
        }
    }
};
