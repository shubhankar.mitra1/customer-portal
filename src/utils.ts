/* eslint-disable @typescript-eslint/naming-convention */
import { ApiErrorResultData, ApiResultData } from './Types/types';
import _ from './lodash.config';

export const injectID = (arr: [] | any = []) => {
    return arr.map((item: object) => {
        if (typeof item === 'object' && item instanceof Object) {
            return { data: { ...item }, id: crypto?.randomUUID?.() };
        }
        if (typeof item === 'string' || typeof item === 'number') {
            return { data: item, id: crypto?.randomUUID?.() };
        }
        return [];
    });
};

/**
 *
 * @param {*} data API raw data
 * @returns nested result object
 */
export const getResultFromData = (data: ApiResultData | any) => data?.data?.data?.result || data?.data?.result;

/**
 *
 * @param {*} data API error data
 * @returns nested error object
 */
export const getErrorResultFromData = (data: ApiErrorResultData | any) => data?.error?.response?.data;

/**
 * @param {*} data camelCase String
 * @returns Capitalized Spaced String
 */
export const getCapitallisedFromCamelCase = (data: string) => {
    const regex = /([^A-Za-z0-9.$])|([A-Z])(?=[A-Z][a-z])|([^\-$.0-9])(?=\$?[0-9]+(?:\.[0-9]+)?)|([0-9])(?=[^.0-9])|([a-z])(?=[A-Z])/g;

    return _.startCase(_.toLower(data.replace(regex, '$2$3$4$5 ')));
};

/**
 *
 * @param {*} input Date as yyyy-MM-dd format
 * @returns Date as dd-MM-yyyy format
 */
export const formatDate__ddMMyyyy = (input: any = '') => {
    const datePart = input.match(/\d+/g);
    const year = datePart?.[0]?.substring(0, 4);
    const month = datePart?.[1];
    const day = datePart?.[2];

    return `${day}/${month}/${year}`;
};

/**
 *
 * @param {*} input Form elements list **HTMLFormControlsCollection**
 * @returns Values from elements list
 */
export const getFormValues = (input: any = {}, ...propNames: string[]) => {
    const values = {};

    propNames.forEach((propName) => {
        if (input[propName]) {
            values[propName] = input[propName].value;
        }
    });

    return values;
};

/**
 *
 * @param {*} str string representation of number e.g "10,000.00"
 * @returns converted number form 10000
 */
export const getNormalizedNumberFromString = (str) => {
    if (typeof str === 'string') {
        return +str.split('.')[0].split(',').join('');
    }
    if (typeof str === 'number') {
        return str;
    }
    return 0;
};
