import { ServiceInstance, SubmitServiceInstance } from '../../../axiosConfig';

export const getByClaimType = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberclaimtype', payLoad);
};

export const getByTreatment = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberclaimtreatment', payLoad);
};

export const getByGender = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberclaimgender', payLoad);
};
export const getByLocation = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberclaimlocation', payLoad);
};
export const getByAgeGroup = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberclaimagegroup', payLoad);
};
export const getByDurationOfStay = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberclaimduration', payLoad);
};
