import { ServiceInstance, SubmitServiceInstance } from '../../../axiosConfig';

export const uploadFiles = async (payLoad: { tokenID: any; providerID: any; userID: any; file: any; barCode: any }) => {
    let result;
    try {
        result = await ServiceInstance.post('/uploadClaimDocuemts', payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error };
    }
};

export const getClaimDropDownvalues = async (payLoad: { tokenID: any; memberNo: any }) => {
    let result;
    try {
        result = await ServiceInstance.post('/clauses', payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error };
    }
};

export const getCurrencyDropDownvalues = async (payLoad: { tokenID: any; memberNo: any }) => {
    let result;
    try {
        result = await ServiceInstance.post('/currencies', payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error };
    }
};

export const getMemberFamilyDetails = async (payLoad: { tokenID: any; memberNo: any }) => {
    let result;
    try {
        result = await ServiceInstance.post('/getcustomernofamily', payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error };
    }
};

export const getExpenseTypeDropDownvalues = async (payLoad: { tokenID: any; providerID: any; userID: any; providerType: any }) => {
    let result;
    try {
        result = await ServiceInstance.post('/serviceclause', payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error };
    }
};

export const saveClaimDetails = async (payLoad: { [key: string]: any }) => {
    let result;
    try {
        result = await SubmitServiceInstance.post('/saveClaimDetails', payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error };
    }
};

export const getRelation = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberclaimpreferred', payLoad);
};

export const getSelectedRelationDetails = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberclaimpreferredsearchbymemno', payLoad);
};

export const saveMemberIntimationDetails = async (payLoad: object | undefined) => {
    return SubmitServiceInstance.post('/saveclaimdetails', payLoad);
};

export const saveMemberClaimnDetails = async (payLoad: object | undefined) => {
    return SubmitServiceInstance.post('/saveclaimsubmitdetails', payLoad);
};

export const getServiceTypeValues = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/populateServicetype', payLoad);
};

export const getClaimTypeAndSubTypeValues = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/populateClaimAndSubtype', payLoad);
};

export const getListForDocuments = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/claimdetails', payLoad);
};
