import { ServiceInstance } from '../../../axiosConfig';

export const getRecentClaims = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberclaimrecent', payLoad);
};

export const getGraphRelation = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberclaimmadeonpoliciesrelation', payLoad);
};

export const getAmountData = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberclaimmadepoliciestotalamount', payLoad);
};
export const getGraphDataFromApi = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberclaimmadepoliciestotalamountgr', payLoad);
};
