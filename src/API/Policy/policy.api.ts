import { ServiceInstance } from '../../../axiosConfig';

export const getPolicyDetails = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberfamilyoverview', payLoad);
};

export const getMemberCoveredPolicy = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/membercoveredpolicy', payLoad);
};
