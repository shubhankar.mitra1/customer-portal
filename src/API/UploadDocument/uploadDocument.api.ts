import { ServiceInstance, SubmitServiceInstance } from '../../../axiosConfig';

export const populateDocumentType = async (payLoad: object | undefined) => {
    let result;
    try {
        result = await ServiceInstance.post('/populatedocumenttype', payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error };
    }
};

export const uploadDocument = async (payLoad: object | undefined) => {
    let result;
    try {
        result = await ServiceInstance.post('/uploadClaimDocuemts', payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error };
    }
};

export const viewUploadedDocument = async (payLoad: object | undefined) => {
    let result;
    try {
        result = await ServiceInstance.post('/viewclaimdocuments', payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error };
    }
};
