import { ServiceInstance, SubmitServiceInstance } from '../../../axiosConfig';

export const getProviderNetwork = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/providers', payLoad);
};
