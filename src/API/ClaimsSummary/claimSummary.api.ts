import { ServiceInstance, SubmitServiceInstance } from '../../../axiosConfig';

export const memberClaimAnnualMade = async (payLoad: object | undefined) => {
    let result;
    try {
        result = await ServiceInstance.post('/memberclaimannualmade', payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error };
    }
};

export const memberClaimsProvider = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberclaimprovider', payLoad);
};

export const getStatus = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/populatestatus', payLoad);
};

export const populateRelation = async (payLoad: object | undefined) => {
    return ServiceInstance.post('/memberclaimmadeonpoliciesrelation', payLoad);
};
