import { ServiceInstance } from '../../../axiosConfig';

export const LoginAPI = async (payLoad: object) => {
    let result;
    try {
        result = await ServiceInstance.post('/validatecustomerlogin', { requiredOTP: 'Y', ...payLoad });
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error };
    }
};

export const verifyWithOTP = async (payLoad: object) => {
    let result;
    try {
        result = await ServiceInstance.post('/validatecustomerloginotp', payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error };
    }
};
