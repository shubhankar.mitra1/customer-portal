/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-no-constructed-context-values */
import React, { createContext, useContext, useMemo } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSessionStorage } from './useSessionStorage';

type TreeProp = {
    children: React.ReactNode;
};

const AuthContext = createContext({});

export const AuthProvider = ({ children }: TreeProp) => {
    const [user, setUser] = useSessionStorage('user', null);
    const navigate = useNavigate();

    // call this function when you want to authenticate the user
    const login = async (data: object) => {
        try {
            setUser(data);
            sessionStorage.setItem('user', JSON.stringify(data));
            navigate('/home');
        } catch (err) {
            alert('Please pass valid JSON');
        }
    };

    // call this function to sign out logged in user
    const logout = () => {
        setUser(null);
        // sessionStorage.removeItem('memberDetails');
        navigate('/', { replace: true });
    };

    const userDetail = useMemo(
        () => ({
            user,
        }),
        [user]
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const value = useMemo(() => ({ ...userDetail, login, logout }), [userDetail]);
    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export const useAuth = () => {
    return useContext(AuthContext);
};
