/* eslint-disable no-nested-ternary */
import { providerImageSVG } from '@assets/index';
import SubLayout from '@layout/SubLayout';
import { Button, Card, Form } from 'react-bootstrap';
import Select from 'react-select';
import Spinner from '@components/Spinner/Spinner';

import './styles.css';
import { useQueries, useQueryClient } from '@tanstack/react-query';
import { getProviderNetwork } from '@api/Providers/providers.api';
import { Key, useEffect, useState } from 'react';
import { uniqueId } from 'lodash';
import { getResultFromData } from '../../utils';
import { getPayLoad } from '../../Payload';
import ProviderCard from './ProviderCard';

const Providers = () => {
    const queryClient = useQueryClient();
    const [pageNo, setPageNo] = useState(0);
    const [pinCode, setPincode] = useState('');
    const [{ data: providerInfo, refetch: getProvider, isLoading, isFetching }] = useQueries({
        queries: [
            {
                queryKey: ['providers', pageNo],
                queryFn: () => getProviderNetwork(getPayLoad('providers', { pageNo, pinCode })),
                refetchOnWindowFocus: false,
                cacheTime: 0,
                staleTime: 0,
                retry: 1,
                select(data) {
                    return getResultFromData(data)?.map((item) => ({ ...item, id: uniqueId() }));
                },

                onError() {
                    queryClient.setQueryData(['providers', pageNo], null);
                },
            },
        ],
    });

    const filterList = () => {
        getProvider();
        queryClient.setQueryData(['providers', pageNo], null);
    };

    return (
        <SubLayout>
            <main className="ps-4 pe-4">
                <section className="d-flex justify-content-between align-items-baseline">
                    <section className="d-flex flex-row gap-3 mt-3 basis--one">
                        <Card
                            className="d-flex flex-row gap-3 p-2 font-14 justify-content-around border-0"
                            style={{ width: 'fit-content', cursor: 'pointer' }}
                        >
                            <p className="name">Provider</p>
                            <p className="value">100</p>
                        </Card>
                        <Card
                            className="d-flex flex-row gap-3 p-2 font-14 justify-content-around border-0"
                            style={{ width: 'fit-content', cursor: 'pointer' }}
                        >
                            <p className="name">In Network</p>
                            <p className="value">100</p>
                        </Card>
                        <Card
                            className="d-flex flex-row gap-3 p-2 font-14 justify-content-around border-0"
                            style={{ width: 'fit-content', cursor: 'pointer' }}
                        >
                            <p className="name">Hospital</p>
                            <p className="value">100</p>
                        </Card>
                        <Card
                            className="d-flex flex-row gap-3 p-2 font-14 justify-content-around border-0"
                            style={{ width: 'fit-content', cursor: 'pointer' }}
                        >
                            <p className="name">Labs</p>
                            <p className="value">100</p>
                        </Card>
                        <Card
                            className="d-flex flex-row gap-3 p-2 font-14 justify-content-around border-0"
                            style={{ width: 'fit-content', cursor: 'pointer' }}
                        >
                            <p className="name">Past Admissions</p>
                            <p className="value">100</p>
                        </Card>
                    </section>
                    {/* <section className="basis--two">
                        <input type="search" className="search--box font-14" placeholder="Search By Keywords" />
                    </section> */}
                </section>
                <section className="filter--props mt-4">
                    {/* <Select
                        isSearchable
                        className="font-14 select--location"
                        placeholder="Current Location"
                        options={['Default'].map((item: string) => ({
                            isFixed: true,
                            label: item,
                            value: item.toLowerCase(),
                        }))}
                    /> */}
                    <Form.Group className="col-md-4 py-2 d-flex gap-3">
                        <input
                            id="pincode"
                            placeholder="Search by Pincode"
                            type="text"
                            className="ps-3 form-control inputSize"
                            onChange={(e) => setPincode((e.target as HTMLInputElement).value)}
                        />
                        <Button type="button" className="btn btn-primary" size="sm" onClick={filterList}>
                            Submit
                        </Button>
                    </Form.Group>
                    {/* <section className="d-flex flex-row align-items-baseline gap-3">
                        <p className="font-14 fw-bold location--par">By nearest location</p>
                        <Button className="km--button font-12">Less than 500 m</Button>
                        <Button className="km--button font-12">Less than 1 km</Button>
                        <Button className="km--button font-12">Less than 2 km</Button>
                        <Button className="km--button font-12">Less than 3 km</Button>
                        <Button className="km--button font-12">Less than 4 km</Button>
                    </section> */}
                </section>
                <section className={providerInfo ? 'main--content' : 'not--found'}>
                    {providerInfo ? (
                        providerInfo.map(
                            (item: {
                                id: Key | null | undefined;
                                providerName: string | undefined;
                                providerType: string | undefined;
                                providerAddress: string | undefined;
                                providerContactNo: string | undefined;
                                providerWebSite: string | undefined;
                            }) => (
                                <ProviderCard
                                    key={item.id}
                                    image={providerImageSVG}
                                    name={item.providerName}
                                    rating={4}
                                    networkstatus={item.providerType}
                                    address={item.providerAddress}
                                    // lat="22.508720"
                                    // long="88.301170"
                                    contact={item.providerContactNo}
                                    url={item.providerWebSite}
                                />
                            )
                        )
                    ) : isLoading || isFetching ? (
                        <Spinner suspense={false} width="30px" height="30px" />
                    ) : (
                        <div>
                            <h1>No Data Found</h1>
                        </div>
                    )}
                </section>
            </main>
        </SubLayout>
    );
};

export default Providers;
