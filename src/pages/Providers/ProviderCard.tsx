import { useEffect, useState } from 'react';
import { Card, Spinner } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar, faLocationCrosshairs, faPhone, faEarthAsia } from '@fortawesome/free-solid-svg-icons';
import { faStar as faStarRegular } from '@fortawesome/free-regular-svg-icons';
import { getDistance } from 'geolib';
// import { useScript } from 'usehooks-ts';
import { ProviderCardType } from '../../Types/types';

const ProviderCard = ({ name, rating, image, networkstatus, address, contact, distance, lat, long, url }: ProviderCardType) => {
    // const status = useScript(`https://maps.googleapis.com/maps/api/js?key=${import.meta.env.VITE_Places_APIKEY}&libraries=places`, {
    //     removeOnUnmount: false,
    // });

    const [distancePoints, setDistance] = useState<number>();

    const stars: JSX.Element[] = [];
    let remainder = 5 - Number(rating);
    const getStars = () => {
        for (let i = 1; i <= Number(rating); i += 1) {
            stars.push(<FontAwesomeIcon icon={faStar} color="#6c63ff" size="xs" />);
        }
        while (remainder > 0) {
            stars.push(<FontAwesomeIcon icon={faStarRegular} color="#6c63ff" size="xs" />);
            remainder -= 1;
        }

        return stars;
    };

    // useEffect(() => {
    //     navigator.geolocation.getCurrentPosition(
    //         (pos) => {
    //             const { latitude, longitude } = pos.coords;
    //             setDistance(
    //                 getDistance(
    //                     { latitude, longitude },
    //                     {
    //                         latitude: Number(lat),
    //                         longitude: Number(long),
    //                     }
    //                 ) / 1000
    //             );
    //         },
    //         (err) => {
    //             console.log(err);
    //         },

    //         {
    //             enableHighAccuracy: true,
    //             timeout: 5000,
    //             maximumAge: 0,
    //         }
    //     );
    // }, [lat, long]);

    return (
        <Card className="mt-3 flex-row" style={{ width: 'fit-content' }}>
            <section className="image">
                <img src={image} alt="hospital" width={250} height={250} />
            </section>
            <section className="details">
                <h5 className="hospital--name">{name}</h5>
                {/* <div className="d-flex flex-row gap-1 align-items-baseline">
                    {rating}
                    {getStars()}
                </div> */}
                <div className="network--type font-12">{networkstatus}</div>
                <hr />
                {/* <span>
                    <FontAwesomeIcon icon={faLocationCrosshairs} color="#6c63ff" size="sm" />{' '}
                    {`${distancePoints} km` || <Spinner animation="border" />}
                </span>
                <hr /> */}
                <p className="font-12">{address}</p>

                <hr />
                <div className="d-flex gap-5">
                    <span className="font-12">
                        <FontAwesomeIcon icon={faPhone} color="#6c63ff" size="sm" /> {contact}
                    </span>
                    <span className="font-12">
                        <FontAwesomeIcon icon={faEarthAsia} color="#6c63ff" size="sm" /> {url}
                    </span>
                </div>
            </section>
        </Card>
    );
};

export default ProviderCard;
