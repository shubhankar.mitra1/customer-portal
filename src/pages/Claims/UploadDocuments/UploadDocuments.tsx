// @ts-nocheck
// @ts-ignore

import { getListForDocuments } from '@api/SubmitClaim/submitClaim.api';
import { useMutation, useQueries } from '@tanstack/react-query';
import { omit } from 'lodash';
import { useEffect, useRef, useState } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import { Button, Card, Form } from 'react-bootstrap';
import Spinner from '@components/Spinner/Spinner';
import CustomisedTable from '@components/CustomisedTable/CustomisedTable';
import { populateDocumentType, uploadDocument, viewUploadedDocument } from '@api/UploadDocument/uploadDocument.api';
import uploadFilePic from '@assets/uploadFile.svg';
import { getPayLoad } from '../../../Payload';
import { getCapitallisedFromCamelCase, getErrorResultFromData, getResultFromData } from '../../../utils';
import styles from './styles.module.css';
import cogoToast from 'cogo-toast';

const UploadDocuments = () => {
    const [pageNo, setPageNo] = useState(0);
    const [selectedClaimNo, setSelectedClaimNo] = useState();
    const [selectedDoc, setSelectedDoc] = useState<string | undefined>();
    const [{ data: ClaimList }, { refetch: getDocumentTypes, data: documentTypes }, { refetch: uploadFile, data: dataReturnedFromUpload }] =
        useQueries({
            queries: [
                {
                    queryKey: ['claimsList', pageNo],
                    queryFn: () => getListForDocuments(getPayLoad('claimsList', { pageNo })),
                    refetchOnWindowFocus: false,
                    keepPreviousData: true,
                    select(data: any) {
                        const result = getResultFromData(data);

                        if (result) {
                            const filteredData = result
                                .map((item: any) => omit(item, ['claimID', 'dateService']))
                                .map((item: any, index) => ({
                                    ...item,
                                    upload: (
                                        <Button
                                            size="sm"
                                            type="button"
                                            onClick={() => {
                                                getDocumentTypes();
                                                setSelectedClaimNo(result[index].claimID);
                                                dialogRef?.current?.showModal();
                                            }}
                                        >
                                            Upload
                                        </Button>
                                    ),
                                    view: (
                                        <Button
                                            size="sm"
                                            type="button"
                                            onClick={() => {
                                                viewUploadedDocumentMutate(getPayLoad('viewDoc', { claimID: result[index].claimID }));
                                                dialogRefView?.current?.showModal();
                                            }}
                                        >
                                            View
                                        </Button>
                                    ),
                                }));
                            const columns = Object.keys(filteredData[0]).map((item) => getCapitallisedFromCamelCase(item));

                            return { headers: columns, data: filteredData };
                        }
                        return [];
                    },
                },
                {
                    queryKey: ['populateDocumentType'],
                    queryFn: () => populateDocumentType(getPayLoad('populateDocumentType', undefined)),
                    refetchOnWindowFocus: false,
                    enabled: false,
                },
                {
                    queryKey: ['uploadDocument'],
                    queryFn: () =>
                        uploadDocument(
                            getPayLoad('uploadDocument', {
                                file: selectRef?.current?.files[0],
                                documentType: selectedDoc,
                                claimID: selectedClaimNo,
                            })
                        ),
                    onError(data) {
                        resultedData = getErrorResultFromData(data);
                        // cogoToast.error(data?.message);
                        console.log(resultedData);
                    },
                    refetchOnWindowFocus: false,
                    enabled: false,
                },
            ],
        });

    const { mutate: viewUploadedDocumentMutate, data: uploadedDocuments } = useMutation(viewUploadedDocument);

    const dialogRef = useRef<HTMLDialogElement>(null);
    const dialogRefView = useRef<HTMLDialogElement>(null);

    const selectRef = useRef<HTMLInputElement>(null);

    function handleUpload() {
        if (selectedDoc && selectRef.current?.files?.[0]) {
            uploadFile();
        } else {
            cogoToast.error('Please select document type and upload');
        }
    }

    useEffect(() => {
        if (dataReturnedFromUpload) {
            const result = getResultFromData(dataReturnedFromUpload);

            if (result) {
                dialogRef?.current?.close();

                cogoToast.success('Document Upload Successful');
            } else {
                dialogRef?.current?.close();

                cogoToast.error('Something went wrong');
            }
        }
    }, [dataReturnedFromUpload]);

    useEffect(() => {
        if (uploadedDocuments) {
            const resultData = getResultFromData(uploadedDocuments);
            if (resultData) {
                dialogRefView?.current?.showModal();
            } else {
                cogoToast.error('Please Upload the Documents');
            }
        }
    }, [uploadedDocuments]);

    return (
        <main className="main">
            <Card className="rounded rounded-lg mt-4" style={{ width: '100%' }}>
                <Card.Title className={`rounded-top rounded-lg p-2 ${styles.cardTitleUpload}`}>
                    <h5 className="m-0  ps-3">
                        <strong>Upload Documents</strong>
                    </h5>
                </Card.Title>
                <Card.Body>
                    {ClaimList?.headers && ClaimList?.data ? (
                        <CustomisedTable tableHeaders={ClaimList?.headers} tableData={ClaimList?.data} />
                    ) : (
                        <Spinner suspense={false} />
                    )}

                    {/* <button type="button" onClick={() => setPageNo((page) => page + 1)}>
                Next
            </button> */}
                </Card.Body>
            </Card>
            <dialog ref={dialogRef} className={`rounded rounded-lg ${styles.dialog}`}>
                <Form.Group className={`col-md-12 py-2 ${styles['inner--dialog--content']}`}>
                    <Form.Select
                        defaultValue=""
                        id="doctype"
                        title="Select Document type"
                        className={styles.inputSize}
                        style={{ color: 'grey' }}
                        disabled={!documentTypes}
                        onChange={(e) => setSelectedDoc(e.target.value)}
                    >
                        <option disabled value="">
                            Select Document type
                        </option>
                        {getResultFromData(documentTypes)?.map((item) => (
                            <option key={item?.documentType} value={item?.documentType}>
                                {item?.documentType}
                            </option>
                        ))}
                    </Form.Select>
                    <section className={`d-flex ${styles['upload--section']}`} d-flex>
                        <div className="d-flex">
                            <img src={uploadFilePic} alt="" width={30} />
                            <Form.Control type="file" ref={selectRef} />
                        </div>

                        <Button size="sm" type="button" onClick={handleUpload}>
                            Upload
                        </Button>
                    </section>
                    <Button onClick={() => dialogRef.current?.close()}>Close</Button>
                </Form.Group>
            </dialog>
            {getResultFromData(uploadedDocuments) !== undefined ? (
                <dialog ref={dialogRefView} className={`rounded rounded-lg mw-50  p-4  ${styles.dialog}`}>
                    {getResultFromData(uploadedDocuments)?.map((item) => (
                        <ul key={item?.uploadedDocuments}>
                            <li>
                                <a href={item.docPath} target="_blank" rel="noreferrer">
                                    {item.docType}
                                </a>
                            </li>
                        </ul>
                    ))}
                </dialog>
            ) : null}
        </main>
    );
};

export default UploadDocuments;
