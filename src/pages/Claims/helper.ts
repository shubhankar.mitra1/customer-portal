export const getClaimTypeRefinedData = (PlanBalancesCardProps, isClaimTypeSucceeded) => {
    const claimTypeProps = PlanBalancesCardProps.find((item) => item.name === 'By Claim Type');

    const cashlessValue = isClaimTypeSucceeded.reduce((acc: number, item: { claimCategory: string; count: string }) => {
        let accumulatorValue = acc;
        if (item.claimCategory === 'CASHLESS') {
            accumulatorValue += +item.count;
        }
        return accumulatorValue;
    }, 0);

    const reimbursementValue = isClaimTypeSucceeded.reduce((acc: number, item: { claimCategory: string; count: string }) => {
        let accumulatorValue = acc;
        if (item.claimCategory === 'REIMBURSEMENT') {
            accumulatorValue += +item.count;
        }
        return accumulatorValue;
    }, 0);

    const refineData = {
        ...claimTypeProps,
        figure: { ...claimTypeProps.figure, series: [{ data: [cashlessValue, reimbursementValue] }] },
    };

    return refineData;
};

export const getTreatmentRefinedData = (PlanBalancesCardProps, isTreatmentSucceeded) => {
    const treatmentTypeProps = PlanBalancesCardProps.find((item) => item.name === 'By Treatment');

    const surgical = isTreatmentSucceeded.find((item) => item.treatment === 'OPERATIVE')?.count || 0;
    const nonSurgical = isTreatmentSucceeded.find((item) => item.treatment === 'NON-OPERATIVE')?.count || 0;

    const refineData = {
        ...treatmentTypeProps,
        figure: { ...treatmentTypeProps.figure, series: [{ data: [surgical, nonSurgical] }] },
    };

    return refineData;
};

export const getGenderRefinedData = (PlanBalancesCardProps, isGenderSucceeded) => {
    const genderProps = PlanBalancesCardProps.find((item) => item.name === 'By Gender');

    const male = isGenderSucceeded.find((item) => item.gender === 'Male')?.count || 0;
    const female = isGenderSucceeded.find((item) => item.gender === 'Female')?.count || 0;

    const refineData = {
        ...genderProps,
        figure: { ...genderProps.figure, series: [{ data: [male, female] }] },
    };

    return refineData;
};

export const getAgeGroupRefinedData = (PlanBalancesCardProps, isAgeGroupSucceeded) => {
    const ageGroupProps = PlanBalancesCardProps.find((item) => item.name === 'By Age Group');

    const ageGroup = isAgeGroupSucceeded.map((item) => item.ageGroup);

    const ageCount = isAgeGroupSucceeded.map((item) => +item.count);

    const refineData = {
        ...ageGroupProps,
        figure: {
            ...ageGroupProps.figure,
            series: [{ data: ageCount }],
            options: { ...ageGroupProps.figure.options, xaxis: { ...ageGroupProps.figure.options.xaxis, categories: ageGroup } },
        },
    };

    return refineData;
};

export const getDurationOfStayRefinedData = (PlanBalancesCardProps, isDurationOfStaySucceeded) => {
    const durationOfStayProps = PlanBalancesCardProps.find((item) => item.name === 'By Duration of Stay');

    const duration = isDurationOfStaySucceeded.map((item) => item.duration);

    const durationCount = isDurationOfStaySucceeded.map((item) => +item.count);

    const refineData = {
        ...durationOfStayProps,
        figure: {
            ...durationOfStayProps.figure,
            series: [{ data: durationCount }],
            options: { ...durationOfStayProps.figure.options, xaxis: { ...durationOfStayProps.figure.options.xaxis, categories: duration } },
        },
    };

    return refineData;
};

export const getLocationRefinedData = (PlanBalancesCardProps, isLocationSucceeded) => {
    const locationProps = PlanBalancesCardProps.find((item) => item.name === 'By Location');

    const locations = isLocationSucceeded.map((item) => item.location);
    const data = isLocationSucceeded.map((item) => item.count);

    const refineData = {
        ...locationProps,
        figure: {
            ...locationProps.figure,
            series: [{ data }],
            options: { ...locationProps.figure.options, xaxis: { ...locationProps.figure.options.xaxis, categories: locations } },
        },
    };

    return refineData;
};
