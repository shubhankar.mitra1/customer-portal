<!-- // import React, { useCallback, useEffect, useRef, useState } from 'react';
// import './styles.css';
// import { Container, Row, Col, Form, Button, Card, Spinner, Badge } from 'react-bootstrap';

// import cogoToast from 'cogo-toast';
// import axios from 'axios';
// import AsyncSelect from 'react-select/async';
// import { useImmer } from 'use-immer';
// import BootstrapTable from 'react-bootstrap-table-next';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faCircleCheck } from '@fortawesome/free-solid-svg-icons';
// import moment from 'moment';
// import Dropzone from '@components/Dropzone/Dropzone';
// import _ from '../../../lodash.config';
// import { formatDate__ddMMyyyy, getErrorResultFromData, getFormValues, getResultFromData, injectID } from '../../../utils';
// import { fieldValidateFirstStep, fieldValidateSecondStep, fieldValidateThirdStep, fieldValidateFourthStep } from './Validations';
// import CustomVirtualList from './VirtualList';
// import {
//     getClaimDropDownvalues,
//     getCurrencyDropDownvalues,
//     getExpenseTypeDropDownvalues,
//     getMemberFamilyDetails,
//     uploadFiles,
//     saveClaimDetails,
// } from '../../../API/SubmitClaim/submitClaim.api';
// import { useSnippetStore } from '../../../store/store';
// import { decideENV } from '../../../decideENV';
// import CustomisedTable from '@components/CustomisedTable/CustomisedTable';

// type SnippetTypeValues = {
//     removeSnippet?: (id: string) => void;
//     invoices?: [];
//     setInvoice?: (name: string, data: any[], id: string) => void;
// };

// const masterProxy = {
//     1: {
//         servicetype: '',
//         claimtype: '',
//         checkindate: undefined /* Date.now() */,
//         checkoutdate: undefined /* Date.now() */,
//         serviceamount: '',
//         icd: [],
//         policycode: '',
//         patientname: '',
//         policyperiod: '',
//         currency: '',
//         provider: [],
//     },
//     2: {
//         0: {
//             invoicenumber: '',
//             invoicedate: '',
//             invoiceamount: '',
//             items: [],
//         },
//     },
//     3: undefined,

//     4: {
//         uploadedFiles: [],
//     },
// };

// const SubmitClaim = () => {
//     const [step, setStep] = useState(1);
//     const [masterData, setMasterData] = useState(masterProxy);
//     const [userDetails] = useState(JSON.parse(sessionStorage.getItem('user') || '{}'));
//     const [timeStamp, setTimeStamp] = useImmer({
//         checkindate: { hours: '', mins: '' },
//         checkoutdate: { hours: '', mins: '' },
//     });

//     const { invoices } = useSnippetStore<SnippetTypeValues>((state) => state);

//     const updateForm = (field: string, value: any[]) => {
//         switch (step) {
//             case 1: {
//                 if (field !== 'icd' || field !== 'provider') {
//                     setMasterData((md) => ({
//                         ...md,
//                         [step]: { ...md[step], [field]: value },
//                     }));
//                 } else {
//                     setMasterData((md) => ({
//                         ...md,
//                         [step]: {
//                             ...md[step],
//                             [field]: [
//                                 ...md[step][field],
//                                 ...value.map((data: { value: any; label: any }) => {
//                                     if (field === 'icd') {
//                                         return { diseaseICDCode: data.value, diseaseName: data.label };
//                                     }
//                                     return { providerName: data.label, providerCode: data.value };
//                                 }),
//                             ],
//                         },
//                     }));
//                 }
//                 break;
//             }
//             case 2: {
//                 setMasterData((md) => ({ ...md, [step]: field }));
//                 break;
//             }
//             case 3: {
//                 setMasterData((md) => ({ ...md, [step]: field }));
//                 break;
//             }
//             case 4: {
//                 setMasterData((md) => ({
//                     ...md,
//                     [step]: { ...md[step], [field]: value },
//                 }));
//                 break;
//             }
//             default: {
//                 console.log('Invalid step');
//             }
//         }
//     };

//     const SubmitData = async (finalData: {
//         1: {
//             servicetype: string;
//             claimtype: string;
//             checkindate: undefined;
//             /* Date.now() */ checkoutdate: undefined;
//             /* Date.now() */ serviceamount: string;
//             icd: never[];
//             policycode: string;
//             patientname: string;
//             policyperiod: string;
//             currency: string;
//             provider: never[];
//         };
//         2: { 0: { invoicenumber: string; invoicedate: string; invoiceamount: string; items: never[] } };
//         3: undefined;
//         4: { uploadedFiles: never[] };
//     }) => {
//         const { tokenID, memberNo, providerName, actualMemberNo, employeeCode, productCurrency, countryCode } = userDetails;
//         const _finalData = _.cloneDeep(finalData);
//         console.log(_finalData);
//         const payLoad = {
//             claims: {
//                 membernumber: memberNo /*finalData[1].memberno */,
//                 countrycode: countryCode,
//                 policycurrencycode: productCurrency,
//                 claimamount: _finalData[1].serviceamount,
//                 patientname: _finalData[1].membername,
//                 checkin: `${formatDate__ddMMyyyy(
//                     _finalData[1].checkindate.replaceAll('-', '/')
//                 )} ${`${timeStamp.checkindate.hours}:${timeStamp.checkindate.mins}:00`}`,
//                 checkout: `${formatDate__ddMMyyyy(
//                     _finalData[1].checkoutdate.replaceAll('-', '/')
//                 )} ${`${timeStamp.checkoutdate.hours}:${timeStamp.checkoutdate.mins}:00`}`,
//                 familycode: employeeCode,
//                 currency: _finalData[1].currency,
//                 benefitdesc: _finalData[1].claimtype.name.trim(),
//                 benefitcode: _finalData[1].claimtype.value.trim(),
//                 servicetype: _finalData[1].servicetype,
//                 tokenID,
//                 providername: _finalData[1].provider.providerName.trim(),
//                 providercode: _finalData[1].provider.providerCode.trim(),
//                 userCode: actualMemberNo,
//                 diseases: _.uniqBy(_finalData[1].icd, 'diseaseICDCode'),
//                 invoices: _.omitDeep(invoices, ['id']) /* _finalData[3] */,
//             },
//         };

//         // bulk validate rather here

//         const saveResult = await saveClaimDetails(payLoad);

//         if (!saveResult.ok) {
//             cogoToast.error(getErrorResultFromData(saveResult).message);
//         } else {
//             cogoToast.success('Claim submitted successfully');
//             // removeSnippet();
//         }

//         if (_finalData[4].uploadedFiles.length > 0) {
//             if (saveResult.ok) {
//                 const apiResponse = getResultFromData(saveResult);
//                 const payLoad = {
//                     tokenID,
//                     providerID: providerId,
//                     userID: userCode,
//                     file: _finalData[4].uploadedFiles[0],
//                     barCode: apiResponse?.transactionID?.split(':')[1],
//                 };
//                 const result = await uploadFiles(payLoad);
//                 if (result.ok) {
//                     cogoToast.success('Files uploaded successfully');
//                 } else {
//                     cogoToast.error(getErrorResultFromData(result).message);
//                 }
//             }
//         }
//     };

//     const isValidFirstStep = () => {
//         const { error } = fieldValidateFirstStep.validate(masterData[1]);
//         if (error) {
//             cogoToast.error(error.message);
//             return false;
//         }
//         return true;
//     };

//     const isValidSecondStep = () => {
//         const { error } = fieldValidateSecondStep.validate(masterData[2]);
//         if (error) {
//             cogoToast.error(error.message);
//             return false;
//         }
//         if (Object.values(masterData[2]).reduce((acc, item) => acc + item.invoiceamount ?? 0, 0) === masterData[1].serviceamount) {
//             return true;
//         }
//         cogoToast.error('Collective invoice amount should equal claim amount.Please edit or delete fields');
//         return false;
//     };

//     const isValidThirdStep = () => {
//         const { error } = fieldValidateThirdStep.validate(masterData[3]);
//         if (error) {
//             cogoToast.error(error.message);
//             return false;
//         }
//         if (
//             invoices?.length > 0 &&
//             invoices?.every((data) => data.invoiceamount === data?.items?.reduce((acc: any, item: { amount: any }) => item.amount + acc, 0))
//         ) {
//             return true;
//         }
//         cogoToast.error('Line Items amount should add upto total invoice amount');
//         return false;
//     };

//     const isValidFourthStep = () => {
//         const { error } = fieldValidateFourthStep.validate(masterData[4]);
//         if (error) {
//             cogoToast.error(error.message);
//             return false;
//         }
//         return true;
//     };

//     const proceedToNextStep = (e: { target: { id: any } }) => {
//         const { id } = e.target;
//         const targetStep = id.includes('alt') ? Number(id.slice(id.length - 1)) : Number(id);

//         switch (step) {
//             case 1: {
//                 if (targetStep > step && targetStep - step === 1) {
//                     if (isValidFirstStep()) {
//                         setStep(2);
//                     }
//                 } else if (targetStep - step === 3) {
//                     if (isValidFirstStep()) {
//                         if (isValidSecondStep()) {
//                             if (isValidThirdStep()) {
//                                 setStep(targetStep);
//                             }
//                         }
//                     }
//                 } else if (targetStep - step === 2) {
//                     if (isValidFirstStep()) {
//                         if (isValidSecondStep()) {
//                             setStep(targetStep);
//                         }
//                     }
//                 }

//                 break;
//             }
//             case 2: {
//                 if (targetStep > step && targetStep - step === 1) {
//                     if (isValidSecondStep()) {
//                         setStep(3);
//                     }
//                 } else if (targetStep - step === 2) {
//                     if (isValidSecondStep()) {
//                         if (isValidThirdStep()) {
//                             setStep(targetStep);
//                         }
//                     }
//                 } else if (targetStep - step < 0) {
//                     setStep(targetStep);
//                 }

//                 break;
//             }
//             case 3: {
//                 if (targetStep > step && targetStep - step === 1) {
//                     if (isValidThirdStep()) {
//                         setStep(4);
//                     }
//                 } else {
//                     setStep(targetStep);
//                 }

//                 break;
//             }
//             case 4: {
//                 if (targetStep > step) {
//                     if (isValidFourthStep()) {
//                         SubmitData(masterData);
//                     }
//                 } else {
//                     setStep(targetStep);
//                 }
//                 break;
//             }
//             default: {
//                 return void 0;
//             }
//         }
//     };

//     return (
//         <div>
//             <Container fluid>
//                 <Row className="mt-4">
//                     <Col md={12}>
//                         <Card className="border-0">
//                             <Card.Body>
//                                 <Card.Title>Request for Cashless</Card.Title>
//                                 <Card className="mt-3 col-lg-12 step__flex">
//                                     <Button id="1" className={`${step === 1 ? 'step_actve' : 'steps'}`} onClick={proceedToNextStep}>
//                                         1
//                                     </Button>
//                                     <Button id="2" className={`${step === 2 ? 'step_actve' : 'steps'}`} onClick={proceedToNextStep}>
//                                         2
//                                     </Button>
//                                     <Button id="3" className={`${step === 3 ? 'step_actve' : 'steps'}`} onClick={proceedToNextStep}>
//                                         3
//                                     </Button>
//                                     <Button id="4" className={`${step === 4 ? 'step_actve' : 'steps'}`} onClick={proceedToNextStep}>
//                                         4
//                                     </Button>
//                                 </Card>
//                                 <div style={{ display: step === 1 ? 'block' : 'none' }}>
//                                     <Step1 update={updateForm} existingData={masterData[1]} setTimeStamp={setTimeStamp} timeStamp={timeStamp} />
//                                 </div>
//                                 <div style={{ display: step === 2 ? 'block' : 'none' }}>
//                                     <Step2
//                                         update={updateForm}
//                                         existingData={masterData[2]}
//                                         appendField={setMasterData}
//                                         setTimeStamp={setTimeStamp}
//                                         claimAmount={masterData[1].serviceamount}
//                                     />
//                                 </div>
//                                 <div style={{ display: step === 3 ? 'block' : 'none' }}>
//                                     <Step3 update={updateForm} existingData={masterData[3]} invoiceData={masterData[2]} />
//                                 </div>
//                                 <div style={{ display: step === 4 ? 'block' : 'none' }}>
//                                     <Step4 update={updateForm} />
//                                 </div>

//                                 <Button id={`alt_${step + 1}`} className="mt-3 next_btn" onClick={proceedToNextStep}>
//                                     {step === 4 ? 'Submit' : 'Next'}
//                                 </Button>
//                             </Card.Body>
//                         </Card>
//                     </Col>
//                 </Row>
//             </Container>
//         </div>
//     );
// };

// const Step1 = ({ update: sendData = () => {}, existingData, setTimeStamp = () => {}, timeStamp = {} }) => {
//     const [userDetails] = useState(JSON.parse(sessionStorage.getItem('user') || '{}'));
//     const controllerRef = useRef();
//     const [value, setValue] = useState('');
//     const [valueProvider, setValueProvider] = useState('');
//     const firstRender = useRef(true);
//     const modalRef = useRef(null);
//     const [claimType, setClaimType] = useState([]);
//     const [currencyType, setCurrencyType] = useState([]);
//     const [memberFamilyDetails, setMemberFamilyDetails] = useState([]);
//     // const [providerNames, setProviderNames] = useState([]);
//     const [OTP, setOTP] = useState('');
//     const [isLoading, setIsLoading] = useState({
//         spinner: memberFamilyDetails?.length === 0,
//         icon: existingData?.membername?.length > 0,
//     });

//     const handleOTP = async (e: { preventDefault: () => void }) => {
//         e.preventDefault();
//         if (String(OTP).length !== 6) {
//             cogoToast.error('OTP must be 6 digits');
//             return;
//         }

//         const { tokenID, userCode, providerId } = userDetails;
//         const payLoad = {
//             OTP: OTP.toString(),
//             memberNo: existingData.memberno,
//             userID: userCode,
//             tokenID,
//             providerID: providerId,
//         };

//         const result = await ValidateMemberWithOTP(payLoad);
//         if (result.ok) {
//             setOTP('');
//             setIsLoading((val) => ({ ...val, spinner: false }));
//             setIsLoading((val) => ({ ...val, icon: true }));
//             // Fill details
//             const resultFromData = getResultFromData(result);
//             sendData('policycode', resultFromData.policyNo);
//             sendData('membername', resultFromData.memberName);
//             sendData(
//                 'policyperiod',
//                 `${resultFromData.policyStartDate.replaceAll('-', '/')} - ${resultFromData.policyExpireDate.replaceAll('-', '/')}`
//             );
//             sendData('firstenrollmentdate', resultFromData.policyStartDate);
//             sendData('productcurrency', resultFromData.productCurrency);
//             sendData('countrycode', resultFromData.countryCode);
//             sendData('familycode', resultFromData.empCode);

//             modalRef.current.close();
//         } else {
//             setOTP('');
//             setIsLoading((val) => ({ ...val, spinner: false }));
//             setIsLoading((val) => ({ ...val, icon: false }));
//             cogoToast.error('Something went wrong');
//             sendData('policycode', '');
//             sendData('policyperiod', ``);
//             sendData('membername', '');
//             sendData('firstenrollmentdate', '');
//             modalRef.current.close();
//         }
//     };
//     // eslint-disable-next-line
//     const debouncedSearch = useCallback(
//         _.debounce((value, callBack) => {
//             if (controllerRef.current) {
//                 controllerRef.current.abort();
//             }
//             controllerRef.current = new AbortController();
//             try {
//                 if (value.length > 0) {
//                     const { providerId, tokenID, actualMemberNo } = userDetails;

//                     const payLoad = {
//                         tokenID,
//                         providerID: providerId,
//                         searchICDorName: value,
//                         memberNo: actualMemberNo,
//                     };
//                     axios({
//                         url: `${decideENV() === 'DEV' ? import.meta.env.VITE_BaseURL_DEV : import.meta.env.VITE_BaseURL_PROD}/populatediseases`,
//                         method: 'post',
//                         data: payLoad,
//                         signal: controllerRef.current.signal,
//                         headers: {
//                             'eO2-Secret-Code': import.meta.env.VITE_EO2_SECRET_CODE,
//                             'Content-Type': 'multipart/form-data',
//                         },
//                     }).then((data) => {
//                         if (data.data.result) {
//                             callBack(
//                                 data.data.result.map((item: { diseaseICDCode: any; diseaseDesc: string }) => ({
//                                     value: item.diseaseICDCode,
//                                     label: `${item.diseaseDesc.toUpperCase().slice(0, 30)}...`,
//                                 }))
//                             );
//                         } else {
//                             cogoToast.error(data.data.message);
//                         }
//                     });
//                 }
//             } catch (err) {
//                 console.log(err);
//             }
//         }, 1000),
//         []
//         // eslint-disable-next-line react-hooks/exhaustive-deps
//     );

//     const debouncedSearchProvider = useCallback(
//         _.debounce((value, callBack) => {
//             if (controllerRef.current) {
//                 controllerRef.current.abort();
//             }
//             controllerRef.current = new AbortController();
//             try {
//                 if (value.length > 2) {
//                     const { tokenID, actualMemberNo } = userDetails;

//                     const payLoad = {
//                         tokenID,
//                         // providerID: providerId,
//                         searchProvider: value,
//                         memberNo: actualMemberNo,
//                     };
//                     axios({
//                         url: `${decideENV() === 'DEV' ? import.meta.env.VITE_BaseURL_DEV : import.meta.env.VITE_BaseURL_PROD}/viewProviders`,
//                         method: 'post',
//                         data: payLoad,
//                         signal: controllerRef.current.signal,
//                         headers: {
//                             'eO2-Secret-Code': import.meta.env.VITE_EO2_SECRET_CODE,
//                             'Content-Type': 'multipart/form-data',
//                         },
//                     }).then((data) => {
//                         if (data.data.result) {
//                             callBack(
//                                 data.data.result.map((item: { providerCode: any; providerName: string }) => ({
//                                     value: item.providerCode,
//                                     label: `${item.providerName.toUpperCase()}`,
//                                 }))
//                             );
//                         } else {
//                             cogoToast.error(data.data.message);
//                         }
//                     });
//                 } else {
//                     cogoToast.info('Please type atleast 3 charchters');
//                 }
//             } catch (err) {
//                 console.log(err);
//             }
//         }, 1000),
//         []
//         // eslint-disable-next-line react-hooks/exhaustive-deps
//     );

//     const handleChange = (change: React.SetStateAction<string>) => {
//         sendData('icd', change);
//         setValue(change);
//     };

//     const handleChangeProvider = (change: React.SetStateAction<string>) => {
//         sendData('provider', change);
//         setValueProvider(change);
//     };

//     const customStyles = {
//         menuList: (provided: any, state: any) => ({
//             ...provided,
//             marginBottom: 10,
//         }),
//     };

//     const addMins = (time: string, minsToAdd: number) => {
//         function D(J: number) {
//             return (J < 10 ? '0' : '') + J;
//         }
//         const piece = time.split(':');
//         const mins = piece[0] * 60 + +piece[1] + +minsToAdd;

//         return `${D(((mins % (24 * 60)) / 60) | 0)}:${D(mins % 60)}:${piece[2]}`;
//     };

//     useEffect(() => {
//         if (existingData.servicetype) {
//             sendData('checkindate', moment(new Date()).format('YYYY-MM-DD'));
//             sendData('checkoutdate', moment(new Date()).format('YYYY-MM-DD'));

//             if (existingData.servicetype === 'IP') {
//                 setTimeStamp((ts: { checkindate: { hours: string; mins: string }; checkoutdate: { hours: string; mins: string } }) => {
//                     ts.checkindate.hours = '09';
//                     ts.checkindate.mins = '00';
//                     ts.checkoutdate.hours = '23';
//                     ts.checkoutdate.mins = '59';
//                 });
//             } else if (existingData.servicetype === 'OP') {
//                 setTimeStamp((ts: { checkindate: { hours: string; mins: string }; checkoutdate: { hours: string; mins: string } }) => {
//                     ts.checkindate.hours = '09';
//                     ts.checkindate.mins = '00';
//                     ts.checkoutdate.hours = '09';
//                     ts.checkoutdate.mins = '10';
//                 });
//             }
//         }
//     }, [existingData.servicetype]);

//     useEffect(() => {
//         async function getDropDownValues() {
//             const { tokenID, actualMemberNo: memberNo, policyCode, policyPeriodFrom, policyPeriodUpto } = userDetails;

//             sendData('policycode', policyCode);
//             sendData('policyperiod', `${policyPeriodFrom} - ${policyPeriodUpto}`);

//             const payLoad = {
//                 tokenID,
//                 memberNo,
//             };
//             const [resultClaimDropDown, resultCurrencyDropDown, resultMemberFamilyDetails] = await Promise.all([
//                 getClaimDropDownvalues(payLoad),
//                 getCurrencyDropDownvalues(payLoad),
//                 getMemberFamilyDetails(payLoad),
//             ]);

//             if (resultClaimDropDown.ok) {
//                 setClaimType(getResultFromData(resultClaimDropDown));
//             }
//             if (resultCurrencyDropDown.ok) {
//                 setCurrencyType(getResultFromData(resultCurrencyDropDown));
//             }
//             if (resultMemberFamilyDetails.ok) {
//                 setMemberFamilyDetails(getResultFromData(resultMemberFamilyDetails));
//                 setIsLoading((loaders) => ({ ...loaders, spinner: false, icon: true }));
//             } else {
//                 setIsLoading((loaders) => ({ ...loaders, spinner: false }));
//             }
//         }
//         if (firstRender.current) {
//             getDropDownValues();
//             firstRender.current = false;
//         }
//         // eslint-disable-next-line react-hooks/exhaustive-deps
//     }, []);

//     return (
//         <main>
//             <Row>
//                 <Card className="mt-3 border-0">
//                     {/* <Card.Title>Claim Registration</Card.Title> */}
//                     <Form className="form__el" style={{ columnGap: '10rem' }}>
//                         <Form.Label className="mt-4  col-lg-5" style={{ maxHeight: '62px' }}>
//                             Family Member
//                             <Form.Select
//                                 required
//                                 disabled={memberFamilyDetails?.length === 0}
//                                 value={existingData.patientname}
//                                 onChange={(key) => sendData('patientname', key.target.value)}
//                             >
//                                 <option disabled value="">
//                                     Select
//                                 </option>
//                                 {memberFamilyDetails?.map((member: { memberName: string; membershipNo: string }) => (
//                                     <option value={member.memberName} key={member?.membershipNo}>
//                                         {member.memberName}
//                                     </option>
//                                 ))}
//                             </Form.Select>
//                             {/* {isLoading?.icon && (
//                                 <FontAwesomeIcon
//                                     icon={faCircleCheck}
//                                     style={{
//                                         color: '#5cb85c',
//                                         position: 'relative',
//                                         left: 'calc(100% - 1.8rem)',
//                                         top: 'calc(100% - 5.7rem)',
//                                     }}
//                                 />
//                             )}{' '} */}
//                             {isLoading?.spinner && (
//                                 <div
//                                     style={{
//                                         color: '#556ee6',
//                                         position: 'relative',
//                                         left: 'calc(100% - 1.8rem)',
//                                         top: 'calc(100% - 5.7rem)',
//                                         width: '50px',
//                                         height: '50px',
//                                     }}
//                                 >
//                                     <Spinner animation="border" />
//                                 </div>
//                             )}
//                         </Form.Label>{' '}
//                         <Form.Label className="mt-4  col-lg-5">
//                             Policy Code
//                             <Form.Control type="text" disabled value={existingData.policycode} placeholder="Policy Code" />
//                         </Form.Label>{' '}
//                         {/* <Form.Label className="mt-4  col-lg-5">
//                             Member Name
//                             <Form.Control type="text" disabled value={existingData.membername} placeholder="Member Name" />
//                         </Form.Label>{' '} */}
//                         <Form.Label className="mt-4  col-lg-5">
//                             Policy Period
//                             <Form.Control type="text" disabled value={existingData.policyperiod} placeholder="Policy Period" />
//                         </Form.Label>{' '}
//                         {/* <Form.Label className="mt-4  col-lg-5">
//                             First Enrollment Date
//                             <Form.Control type="text" disabled value={existingData.firstenrollmentdate} placeholder="First Enrollment Date" />
//                         </Form.Label>{' '} */}
//                         <Form.Label className="mt-4  col-lg-5">
//                             Service Type
//                             <Form.Select
//                                 defaultValue=""
//                                 variant="primary"
//                                 disabled={!isLoading?.icon}
//                                 value={existingData.servicetype}
//                                 id="service__step1"
//                                 title="Select Service Type"
//                                 onChange={(key) => sendData('servicetype', key.target.value)}
//                             >
//                                 <option disabled value="">
//                                     Select
//                                 </option>
//                                 <option value="IP">IP</option>
//                                 <option value="OP">OP</option>
//                             </Form.Select>
//                         </Form.Label>{' '}
//                         <Form.Label className="mt-4  col-lg-5">
//                             Claim Type
//                             <Form.Select
//                                 variant="primary"
//                                 disabled={!isLoading?.icon}
//                                 value={existingData.claimtype}
//                                 id="claimtype__step1"
//                                 title="Select Claim Type"
//                                 onChange={(key) => sendData('claimtype', { value: key.target.value, name: key.target.value })}
//                             >
//                                 <option disabled value="">
//                                     Select
//                                 </option>
//                                 {injectID(claimType)?.map((ct) => (
//                                     <option key={ct.id} value={ct.data.clauseCode}>
//                                         {ct.data.clauseDesc}
//                                     </option>
//                                 ))}
//                             </Form.Select>
//                         </Form.Label>{' '}
//                         <Form.Label className="mt-4  col-lg-5">
//                             Check-In Date
//                             <div className="timeGroup">
//                                 <Form.Control
//                                     type="date"
//                                     disabled={!isLoading?.icon}
//                                     value={existingData.checkindate || moment(new Date()).format('YYYY-MM-DD')}
//                                     onChange={(e) => {
//                                         if (existingData.servicetype.length > 0) {
//                                             sendData('checkindate', e.target.value);

//                                             if (existingData.servicetype === 'OP') {
//                                                 sendData('checkoutdate', e.target.value);
//                                             }
//                                         } else {
//                                             cogoToast.error('Please select service type first');
//                                         }
//                                     }}
//                                 />
//                                 <Form.Select
//                                     variant="primary"
//                                     disabled={!isLoading?.icon}
//                                     value={timeStamp.checkindate.hours}
//                                     id="timestampcheckinhrs__step1"
//                                     title="Hours"
//                                     onChange={(key) => {
//                                         setTimeStamp((ts: { checkindate: { hours: string } }) => {
//                                             ts.checkindate.hours = key.target.value;
//                                         });

//                                         if (existingData.servicetype === 'OP') {
//                                             setTimeStamp((ts: { checkoutdate: { hours: string } }) => {
//                                                 ts.checkoutdate.hours = key.target.value;
//                                             });
//                                         }
//                                     }}
//                                 >
//                                     <option disabled selected value="">
//                                         Hours
//                                     </option>
//                                     {injectID(Array.from({ length: 24 }, (_, index) => (index < 10 ? `0${index}` : index))).map((hr) => (
//                                         <option key={hr.id} value={hr.data}>
//                                             {hr.data}
//                                         </option>
//                                     ))}
//                                 </Form.Select>
//                                 <Form.Select
//                                     variant="primary"
//                                     disabled={!isLoading?.icon}
//                                     value={timeStamp.checkindate.mins}
//                                     id="timestampcheckinmins__step1"
//                                     title="Mins"
//                                     onChange={(key) => {
//                                         setTimeStamp((ts: { checkindate: { mins: string } }) => {
//                                             ts.checkindate.mins = key.target.value;
//                                         });
//                                         if (existingData.servicetype === 'OP') {
//                                             setTimeStamp(
//                                                 (ts: { checkindate: { hours: any; mins: any }; checkoutdate: { hours: string; mins: string } }) => {
//                                                     const time = addMins(`${ts.checkindate.hours}:${ts.checkindate.mins}:` + `00`, 10);
//                                                     ts.checkoutdate.hours = time.split(':')[0];
//                                                     ts.checkoutdate.mins = time.split(':')[1];
//                                                 }
//                                             );
//                                         }
//                                     }}
//                                 >
//                                     <option disabled selected value="">
//                                         Mins
//                                     </option>
//                                     {injectID(Array.from({ length: 60 }, (_, index) => (index < 10 ? `0${index}` : index))).map((hr) => (
//                                         <option key={hr.id} value={hr.data}>
//                                             {hr.data}
//                                         </option>
//                                     ))}
//                                 </Form.Select>
//                             </div>
//                         </Form.Label>{' '}
//                         <Form.Label className="mt-4  col-lg-5">
//                             Check-Out Date
//                             <div className="timeGroup">
//                                 <Form.Control
//                                     type="date"
//                                     disabled={!isLoading?.icon}
//                                     value={existingData.checkoutdate || moment(new Date()).format('YYYY-MM-DD')}
//                                     onChange={(e) => {
//                                         if (existingData.servicetype.length > 0) {
//                                             sendData('checkoutdate', e.target.value);
//                                         } else {
//                                             cogoToast.error('Please select service type first');
//                                         }
//                                     }}
//                                 />{' '}
//                                 <Form.Select
//                                     variant="primary"
//                                     disabled={!isLoading?.icon}
//                                     value={timeStamp.checkoutdate.hours}
//                                     id="timestampcheckouthrs__step1"
//                                     title="Hours"
//                                     onChange={(key) =>
//                                         setTimeStamp((ts: { checkoutdate: { hours: string } }) => {
//                                             ts.checkoutdate.hours = key.target.value;
//                                         })
//                                     }
//                                 >
//                                     <option disabled selected value="">
//                                         Hours
//                                     </option>
//                                     {injectID(Array.from({ length: 24 }, (_, index) => (index < 10 ? `0${index}` : index))).map((hr) => (
//                                         <option key={hr.id} value={hr.data}>
//                                             {hr.data}
//                                         </option>
//                                     ))}
//                                 </Form.Select>
//                                 <Form.Select
//                                     variant="primary"
//                                     disabled={!isLoading?.icon}
//                                     value={timeStamp.checkoutdate.mins}
//                                     id="timestampcheckoutmins__step1"
//                                     title="Mins"
//                                     onChange={(key) =>
//                                         setTimeStamp((ts: { checkoutdate: { mins: string } }) => {
//                                             ts.checkoutdate.mins = key.target.value;
//                                         })
//                                     }
//                                 >
//                                     <option disabled selected value="">
//                                         Mins
//                                     </option>
//                                     {injectID(Array.from({ length: 60 }, (_, index) => (index < 10 ? `0${index}` : index))).map((hr) => (
//                                         <option key={hr.id} value={hr.data}>
//                                             {hr.data}
//                                         </option>
//                                     ))}
//                                 </Form.Select>
//                             </div>
//                         </Form.Label>{' '}
//                         <Form.Label className="mt-4  col-lg-5">
//                             Service Amount
//                             <Form.Control
//                                 type="number"
//                                 onWheel={(e) => console.log(e)}
//                                 disabled={!isLoading?.icon}
//                                 value={existingData.serviceamount}
//                                 placeholder="Enter Service Amt."
//                                 onChange={(e) => sendData('serviceamount', e.target.valueAsNumber)}
//                             />
//                         </Form.Label>
//                         <Form.Label className="mt-4  col-lg-5">
//                             Currency
//                             <Form.Select
//                                 variant="primary"
//                                 disabled={!isLoading?.icon}
//                                 value={existingData.currency}
//                                 id="currency__step1"
//                                 title="Select Currency"
//                                 onChange={(key) => sendData('currency', key.target.value)}
//                             >
//                                 <option disabled value="">
//                                     Select
//                                 </option>
//                                 {currencyType?.map((ct) => (
//                                     <option value={ct.currencyCode}>{ct.currencyName}</option>
//                                 ))}
//                             </Form.Select>
//                         </Form.Label>
//                         <Form.Label className="mt-4  col-lg-5" id="async_select" data-isloading={`${!isLoading?.icon}`}>
//                             ICD
//                             <div className="mb-3">
//                                 <AsyncSelect
//                                     menuPlacement="top"
//                                     isMulti
//                                     key={`__asyncSelect${Math.random() * 10}`}
//                                     cacheOptions
//                                     styles={customStyles}
//                                     noOptionsMessage={() => 'No results to display'}
//                                     onChange={handleChange}
//                                     value={value}
//                                     loadOptions={debouncedSearch}
//                                     placeholder="Search ICD"
//                                     components={{ MenuList: CustomVirtualList }}
//                                 />
//                             </div>
//                         </Form.Label>
//                         <Form.Label className="mt-4  col-lg-5" id="async_select" data-isloading={`${!isLoading?.icon}`}>
//                             Provider
//                             <div className="mb-3">
//                                 <AsyncSelect
//                                     menuPlacement="top"
//                                     key={`__asyncSelect${Math.random() * 10}`}
//                                     cacheOptions
//                                     styles={customStyles}
//                                     noOptionsMessage={() => 'No results to display'}
//                                     onChange={handleChangeProvider}
//                                     value={valueProvider}
//                                     loadOptions={debouncedSearchProvider}
//                                     placeholder="Search Providers"
//                                     components={{ MenuList: CustomVirtualList }}
//                                 />
//                             </div>
//                         </Form.Label>
//                     </Form>
//                 </Card>
//             </Row>
//         </main>
//     );
// };

// const Step2 = ({ update: sendData = () => {}, existingData, setTimeStamp = () => {}, claimAmount }) => {
//     const [data, setData] = useImmer({
//         0: {
//             invoicenumber: '',
//             invoicedate: '',
//             invoiceamount: '',
//             items: [],
//         },
//     });
//     const firstRender = useRef(true);
//     const [id, setId] = useState(0);
//     const [tableData, setTableData] = useState([]);
//     const [tableHeaders, setTableHeaders] = useState<string[]>([]);
//     const [currentEditableRow, setCurrentEditableRow] = useState();
//     const modalRef = useRef(null);
//     const existingDataCopy = useRef(null);

//     const saveValues = (field: string | number, value: any, id: string | number) => {
//         setData((data) => {
//             if (data[id]) {
//                 data[id][field] = value;
//                 data[id].items = []; // Important
//             } else {
//                 data[id] = {};
//                 data[id][field] = value;
//                 data[id].items = []; // Important
//             }
//         });
//         firstRender.current = false;
//     };

//     const add = () => {
//         if (data[id]) {
//             const { invoicenumber, invoicedate, invoiceamount } = data[id];
//             if (typeof invoicenumber === 'string' && invoicedate?.length > 0 && typeof invoiceamount === 'number') {
//                 if (!Object.values(tableData).some((data) => data.invoicenumber === invoicenumber)) {
//                     sendData(data);
//                     setId((id) => ++id);
//                 } else {
//                     cogoToast.error('Invoice Number should be unique');
//                 }
//             } else {
//                 cogoToast.error('Fill all the fields');
//             }
//         } else {
//             cogoToast.error('Start entering values in all the fields');
//         }
//     };

//     const handleDelete = (row: { invoicenumber: any }) => {
//         const replacedResults = {};
//         Object.values(_.cloneDeep(existingDataCopy.current))
//             .filter((item) => item.invoicenumber !== row.invoicenumber)
//             .flat()
//             .forEach((item, key) => {
//                 replacedResults[key] = item;
//             });
//         sendData(replacedResults);
//         setData(replacedResults);

//         if (id > 0) {
//             setId((id) => --id);
//         }
//     };

//     const handleEdit = (e: { preventDefault: () => void; persist: () => void; target: { elements: any } }) => {
//         e.preventDefault();
//         e.persist();
//         const formValues = getFormValues(e.target.elements, 'invoicenumber', 'invoicedate', 'invoiceamount');

//         if (!_.isEmpty(formValues)) {
//             formValues.invoicenumber = formValues.invoicenumber === '' ? currentEditableRow.invoicenumber : formValues.invoicenumber;
//             formValues.invoiceamount = formValues.invoiceamount === '' ? currentEditableRow.invoiceamount : Number(formValues.invoiceamount);
//             formValues.invoicedate =
//                 formValues.invoicedate === ''
//                     ? currentEditableRow.invoicedate
//                     : `${formatDate__ddMMyyyy(formValues.invoicedate)} ${new Date().toTimeString().substring(0, 8)}`;

//             setData((data) => {
//                 if (data.invoicenumber === currentEditableRow.invoiceamount) {
//                     data.invoicenumber = formValues.invoicenumber;
//                     data.invoiceamount = formValues.invoiceamount;
//                     data.invoicedate = formValues.invoicedate;
//                 }
//             });

//             const _existingData = _.cloneDeep(existingData);

//             for (const [key, value] of Object.entries(_existingData)) {
//                 if (value.invoicenumber === currentEditableRow.invoicenumber) {
//                     _existingData[key].invoicenumber = formValues.invoicenumber;
//                     _existingData[key].invoiceamount = formValues.invoiceamount;
//                     _existingData[key].invoicedate = formValues.invoicedate;
//                 }
//             }

//             sendData(_existingData);
//         }
//         document.getElementById('Step2EditForm').reset();
//     };

//     const generateTable = (tableData: { [s: string]: never } | ArrayLike<never> | ({ [s: string]: unknown } | ArrayLike<unknown>)[]) => {
//         const tableHeaderContext = [
//             {
//                 dataField: 'invoicenumber',
//                 text: 'Invoice No.',
//                 sort: false,
//             },
//             {
//                 dataField: 'invoicedate',
//                 text: 'Invoice Date',
//                 sort: false,
//             },
//             {
//                 dataField: 'invoiceamount',
//                 text: 'Invoice Amount',
//                 sort: false,
//             },
//             {
//                 dataField: 'edit',
//                 text: 'Edit',
//                 sort: false,
//                 formatter: (cellContent: any, row: React.SetStateAction<undefined>) => {
//                     return (
//                         <Button
//                             style={{ width: 'max-content' }}
//                             onClick={() => {
//                                 setCurrentEditableRow(row);
//                                 modalRef.current.showModal();
//                             }}
//                         >
//                             Edit
//                         </Button>
//                     );
//                 },
//             },
//             {
//                 dataField: 'delete',
//                 text: 'Delete',
//                 sort: false,
//                 formatter: (cellContent: any, row: any) => {
//                     return (
//                         <Button style={{ width: 'max-content' }} variant="danger" onClick={() => handleDelete(row)}>
//                             Delete
//                         </Button>
//                     );
//                 },
//             },
//         ];
//         setTableHeaders(tableHeaderContext);
//         const testObject = Object.keys(tableData);
//         if (
//             testObject?.length === 1 &&
//             testObject[0] === '0' &&
//             Object.values(tableData[0]).every((data) => data === '' || (Array.isArray(data) && data.length === 0))
//         ) {
//             void 0;
//         } else {
//             setTableData(Object.values(tableData));
//         }
//     };

//     useEffect(() => {
//         if (existingData) {
//             existingDataCopy.current = existingData;
//             generateTable(existingData);
//         }
//         // eslint-disable-next-line react-hooks/exhaustive-deps
//     }, [existingData]);

//     useEffect(() => {
//         setData(existingData);
//         // eslint-disable-next-line react-hooks/exhaustive-deps
//         return () => {
//             setData({
//                 0: {
//                     invoicenumber: '',
//                     invoicedate: '',
//                     invoiceamount: '',
//                     items: [],
//                 },
//             });
//         };
//         // eslint-disable-next-line react-hooks/exhaustive-deps
//     }, []);

//     return (
//         <main>
//             <Row>
//                 <Card className="mt-3 border-0">
//                     <Card.Title>Invoice Details</Card.Title>
//                     <InvoiceDetails
//                         key={`invoice_${id}`}
//                         existingData={data[id]}
//                         sendData={saveValues}
//                         id={id}
//                         add={add}
//                         setTimeStamp={setTimeStamp}
//                     />

//                     <dialog ref={modalRef} method="dialog" onSubmit={handleEdit}>
//                         <Form className="form__el" id="Step2EditForm">
//                             <Form.Label className="mt-4  col-md-3">
//                                 Invoice No.
//                                 <Form.Control type="text" onWheel={(e) => e.preventDefault()} name="invoicenumber" placeholder="Enter Invoice No." />
//                             </Form.Label>{' '}
//                             <Form.Label className="mt-4  col-md-3">
//                                 Invoice Date
//                                 <Form.Control type="date" name="invoicedate" placeholder="Enter Invoice Date." />
//                             </Form.Label>{' '}
//                             <Form.Label className="mt-4  col-md-3">
//                                 Invoice Amount
//                                 <Form.Control type="number" onWheel={(e) => e.preventDefault()} name="invoiceamount" placeholder="Enter Invoice " />
//                             </Form.Label>{' '}
//                             <Button
//                                 className="mt-5 next_btn"
//                                 style={{ marginLeft: 'unset' }}
//                                 type="submit"
//                                 onClick={() => {
//                                     modalRef.current.close();
//                                 }}
//                             >
//                                 Save
//                             </Button>
//                         </Form>
//                     </dialog>
//                 </Card>
//             </Row>
//         </main>
//     );
// };

// const Step3 = ({ update: sendData = () => {}, existingData, invoiceData }) => {
//     const [renderSnippet, setRenderSnippet] = useImmer();
//     const { setInvoice, removeSnippet } = useSnippetStore<SnippetTypeValues>((state) => state);
//     useEffect(() => {
//         if (Object.values(invoiceData).every((item) => item.invoicenumber !== '' && item.invoicedate !== '' && item.invoiceamount !== '')) {
//             setInvoice('invoices', Object.values(invoiceData));
//         }
//         // eslint-disable-next-line react-hooks/exhaustive-deps
//     }, [invoiceData]);

//     useEffect(() => {
//         setRenderSnippet(
//             Object.values(invoiceData).reduce(
//                 (acc, item) => ({
//                     ...acc,
//                     [item.invoicenumber]: [
//                         <InvoiceFormSnippet
//                             id={`${item.invoicenumber}_${crypto?.randomUUID?.() || Math.random().toString(36).slice(2)}`}
//                             stepData={item}
//                             remove={remove}
//                         />,
//                     ],
//                 }),
//                 {}
//             )
//         );
//         // eslint-disable-next-line react-hooks/exhaustive-deps
//     }, [invoiceData]);

//     const remove = (id: string | (string | number)[], invoice: any) => {
//         setRenderSnippet((rs: { [x: string]: any }) => {
//             if (rs?.[id.split('_')[0]]?.length > 1) {
//                 rs[id.split('_')[0]] = rs[id.split('_')[0]].filter((Item: { props: { id: any } }) => {
//                     return Item.props.id !== id;
//                 });
//             } else {
//                 console.log(rs[id[0]]);
//             }
//         });
//         // remove the data;
//         removeSnippet(id);
//     };
//     return (
//         <main>
//             {' '}
//             <Row>
//                 <Card className="mt-3 border-0">
//                     <Card.Title>Items</Card.Title>
//                     {Object.values(invoiceData).map((item, key) => (
//                         <>
//                             <Badge bg="primary" style={{ width: 'fit-content', fontSize: '1.25rem' }}>
//                                 Invoice Number: {item.invoicenumber}
//                             </Badge>
//                             {Array.isArray(renderSnippet?.[item.invoicenumber]) &&
//                                 renderSnippet?.[item.invoicenumber]?.map((Snippet: any, key: any) => Snippet)}
//                             <Button
//                                 className="snippetButton"
//                                 onClick={() =>
//                                     setRenderSnippet((val: { [x: string]: JSX.Element[] }) => {
//                                         val[item.invoicenumber].push(
//                                             <InvoiceFormSnippet
//                                                 id={`${item.invoicenumber}_${
//                                                     crypto?.randomUUID?.() || Math.random().toString(36).slice(2) /* val[item.invoicenumber].length */
//                                                 }`}
//                                                 stepData={item}
//                                                 remove={remove}
//                                             />
//                                         );
//                                     })
//                                 }
//                             >
//                                 +
//                             </Button>
//                         </>
//                     ))}
//                 </Card>
//             </Row>
//         </main>
//     );
// };

// const Step4 = ({ update: sendData = () => {} }) => {
//     const getFileData = (data = []) => {
//         sendData('uploadedFiles', data);
//     };
//     return (
//         <Row>
//             <Card className="mt-3 border-0">
//                 <Card.Title>File Upload</Card.Title>
//                 <Form className="form__el">
//                     <Form.Label className="mt-4  col-lg-5" style={{ width: '100%' }}>
//                         Attach Document
//                         <Dropzone disabled={false} multiple={false} getData={getFileData} />
//                     </Form.Label>
//                 </Form>
//             </Card>
//         </Row>
//     );
// };

// export default SubmitClaim;

// function InvoiceDetails({ sendData, id, add = () => {} }) {
//     return (
//         <Form className="form__el">
//             <Form.Label className="mt-4  col-md-3">
//                 Invoice No.
//                 <Form.Control
//                     type="text"
//                     onWheel={(e) => e.preventDefault()}
//                     defaultValue=""
//                     /* value={existingData.invoicenumber} */ placeholder="Enter Invoice No."
//                     onChange={(e) => sendData('invoicenumber', e.target.value, id)}
//                 />
//             </Form.Label>{' '}
//             <Form.Label className="mt-4  col-md-3">
//                 Invoice Date
//                 <Form.Control
//                     type="date"
//                     defaultValue=""
//                     /* value={existingData.invoicedate} */ placeholder="Enter Invoice Date."
//                     onChange={(e) => {
//                         sendData('invoicedate', `${formatDate__ddMMyyyy(e.target.value)} ${new Date().toTimeString().substring(0, 8)}`, id);
//                     }}
//                 />
//             </Form.Label>{' '}
//             <Form.Label className="mt-4  col-md-3">
//                 Invoice Amount
//                 <Form.Control
//                     type="number"
//                     onWheel={(e) => e.preventDefault()}
//                     defaultValue=""
//                     /* value={existingData.invoiceamount} */ placeholder="Enter Invoice "
//                     onChange={(e) => sendData('invoiceamount', e.target.valueAsNumber, id)}
//                 />
//             </Form.Label>{' '}
//             <Button className="mt-5 next_btn" style={{ marginLeft: 'unset' }} onClick={add}>
//                 Add
//             </Button>
//         </Form>
//     );
// }

// function InvoiceFormSnippet({ invoice, handleFields: sendData = () => {}, id, remove = () => {}, stepData }) {
//     const [userDetails] = useState(JSON.parse(sessionStorage.getItem('user') || '{}'));
//     const [expenseType, setExpenseType] = useState([]);
//     const [expenseTypeValue, setExpenseTypeValue] = useState('');
//     const controllerRef = useRef();
//     const formRef = useRef();
//     const [snippetdata, setsnippetData] = useState();

//     const { invoicenumber } = stepData;

//     const { invoices, setInvoice } = useSnippetStore((state) => state);

//     useEffect(() => {
//         const data = invoices
//             .map((item: { items: any }) => item.items)
//             .flat(1)
//             .filter((item: { id: any }) => item?.id === id);
//         if (data.length > 0) {
//             setsnippetData(data[0]);
//         }
//     }, [invoices]);

//     const getDropDownValues = async () => {
//         const { providerId, tokenID, userCode, employeeCode, actualMemberNo } = userDetails;

//         const payLoadExpenseType = {
//             memberNo: actualMemberNo,
//             tokenID,
//             providerID: providerId,
//             userID: userCode,
//             providerType: employeeCode,
//         };

//         const resultExpenseType = await getExpenseTypeDropDownvalues(payLoadExpenseType);
//         setExpenseType(getResultFromData(resultExpenseType));
//     };

//     const debouncedSearch = useCallback(
//         _.debounce((value, callBack) => {
//             if (controllerRef.current) {
//                 controllerRef.current.abort();
//             }
//             controllerRef.current = new AbortController();
//             try {
//                 if (value.value.length > 0) {
//                     const { providerId, tokenID, userCode, providerTypeID, actualMemberNo } = userDetails;
//                     const payLoad = {
//                         tokenID,
//                         memberNo: actualMemberNo,
//                         groupCode: value.groupCode,
//                         searchValue: value.value,
//                     };
//                     axios({
//                         url: `${decideENV() === 'DEV' ? import.meta.env.VITE_BaseURL_DEV : import.meta.env.VITE_BaseURL_PROD}/fetchingItem`,
//                         method: 'post',
//                         data: payLoad,
//                         signal: controllerRef.current.signal,
//                         headers: {
//                             'eO2-Secret-Code': import.meta.env.REACT_APP_EO2_SECRET_CODE,
//                             'Content-Type': 'multipart/form-data',
//                         },
//                     }).then((data) => {
//                         if (data.data.result) {
//                             callBack(
//                                 data.data.result.map((item: { itemCode: any; itemName: any }) => ({
//                                     value: item.itemCode,
//                                     label: item.itemName,
//                                 }))
//                             );
//                         } else {
//                             cogoToast.error(data.data.message);
//                         }
//                     });
//                 }
//             } catch (err) {
//                 console.log(err);
//             }
//         }, 1000),
//         []
//         // eslint-disable-next-line react-hooks/exhaustive-deps
//     );

//     const handleChange = (value: unknown, field: string, id: any, invoice: any) => {
//         setInvoice(
//             'items',
//             {
//                 invoicenumber,
//                 field: 'itemname',
//                 value: value.label,
//                 id,
//             },
//             id
//         );
//         setInvoice(
//             'items',
//             {
//                 invoicenumber,
//                 field: 'itemcode',
//                 value: value.value,
//                 id,
//             },
//             id
//         );
//     };

//     const customStyles = {
//         menuList: (provided: any, state: any) => ({
//             ...provided,
//             marginBottom: 10,
//         }),
//     };

//     useEffect(() => {
//         getDropDownValues();
//         // eslint-disable-next-line react-hooks/exhaustive-deps
//     }, []);

//     return (
//         <Form
//             ref={formRef}
//             className="form__el shadow p-3 mb-5 bg-white rounded"
//             style={{ padding: '0 1rem !important', marginTop: '1rem' }}
//             id={`form_${id}`}
//         >
//             <Form.Label className="mt-4  col-lg-2">
//                 Expense Type
//                 <Form.Select
//                     variant="primary"
//                     name="expensetype"
//                     value={snippetdata?.itemgroupcode || ''}
//                     id="expense__type"
//                     title="Select Expense Type"
//                     onChange={(key) => {
//                         setExpenseTypeValue(key.target.value);
//                         sendData('itemgroupcode', key.target.value, id, invoice);
//                         setInvoice(
//                             'items',
//                             {
//                                 invoicenumber,
//                                 field: 'itemgroupcode',
//                                 value: key.target.value,
//                                 id,
//                             },
//                             id
//                         );
//                     }}
//                 >
//                     <option disabled value="">
//                         Select
//                     </option>
//                     {expenseType?.map((et, key) => (
//                         <option key={`expense_type_option_${key}`} value={et.serviceType}>
//                             {et.serviceTypeDesc}
//                         </option>
//                     ))}
//                 </Form.Select>
//             </Form.Label>{' '}
//             <Form.Label className="mt-4  col-lg-2">
//                 Expense Code
//                 <div className="mb-3">
//                     <AsyncSelect
//                         key={`__asyncSelect__InvoiceFormSnippet__${id}`}
//                         cacheOptions
//                         styles={customStyles}
//                         noOptionsMessage={() => 'No results to display'}
//                         onChange={(e) => handleChange(e, 'expensecode', id, invoice)}
//                         loadOptions={(value, callback) => debouncedSearch({ value, groupCode: expenseTypeValue }, callback)}
//                         placeholder="Search Expense Code"
//                         components={{ MenuList: CustomVirtualList }}
//                     />
//                 </div>
//             </Form.Label>{' '}
//             <Form.Label className="mt-4  col-lg-2">
//                 Total Quantity
//                 <Form.Control
//                     type="number"
//                     onWheel={(e) => e.preventDefault()}
//                     name="totalquantity"
//                     value={snippetdata?.quantity || ''}
//                     placeholder="Enter Total Quantity"
//                     onChange={(e) => {
//                         sendData('quantity', e.target.valueAsNumber, id, invoice);
//                         setInvoice(
//                             'items',
//                             {
//                                 invoicenumber,
//                                 field: 'quantity',
//                                 value: e.target.valueAsNumber,
//                                 id,
//                             },
//                             id
//                         );
//                     }}
//                 />
//             </Form.Label>{' '}
//             <Form.Label className="mt-4  col-lg-2">
//                 Total Amount
//                 <Form.Control
//                     type="number"
//                     onWheel={(e) => e.preventDefault()}
//                     name="totalamount"
//                     value={snippetdata?.amount || ''}
//                     placeholder="Enter Total Amount "
//                     onChange={(e) => {
//                         sendData('amount', e.target.valueAsNumber, id, invoice);
//                         setInvoice(
//                             'items',
//                             {
//                                 invoicenumber,
//                                 field: 'amount',
//                                 value: e.target.valueAsNumber,
//                                 id,
//                             },
//                             id
//                         );
//                     }}
//                 />
//             </Form.Label>{' '}
//             <Button className="remove__btn" onClick={() => remove(id, invoice)}>
//                 X
//             </Button>
//         </Form>
//     );
// } -->
