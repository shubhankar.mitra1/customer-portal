// import Joi from 'joi';

// export const fieldValidateFirstStep = Joi.object({
//     // memberno: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{10,30}$')).required(),
//     // provider: Joi.string().min(25).max(25).required(),
//     servicetype: Joi.string().required(),
//     claimtype: Joi.object().required(),
//     checkindate: Joi.date().required(),
//     checkoutdate: Joi.date().min(Joi.ref('checkindate')).required(),
//     serviceamount: Joi.number().precision(2).required(),
//     icd: Joi.array().min(1).required(),
//     policycode: Joi.string().required(),
//     patientname: Joi.string().required(),
//     policyperiod: Joi.string().required(),
//     // firstenrollmentdate: Joi.string().required(),
//     // productcurrency: Joi.string().required(),
//     // countrycode: Joi.string().required(),
//     // familycode: Joi.string().required(),
//     currency: Joi.string().required(),
//     provider: Joi.object().required(),
// });

// export const fieldValidateSecondStep = Joi.object().pattern(/^/, [
//     Joi.object({
//         invoicenumber: Joi.string().required(),
//         invoicedate: Joi.string().required(),
//         invoiceamount: Joi.number().required(),
//         items: Joi.array(),
//     }),
// ]);

// export const fieldValidateThirdStep = Joi.object().pattern(/^/, [
//     Joi.object({
//         expensetype: Joi.string().required(),
//         expensecode: Joi.string().required(),
//         totalquantity: Joi.number().required(),
//         totalamount: Joi.number().precision(2).required(),
//     }),
// ]);

// // export const fieldValidateThirdStep = Joi.object({
// //   expensetype: Joi.string().required(),
// //   expensecode: Joi.string().required(),
// //   totalquantity: Joi.string().required(),
// //   totalamount: Joi.number().precision(2).required(),
// // });

// export const fieldValidateFourthStep = Joi.object({
//     uploadedFiles: Joi.array(),
// });
import * as z from 'zod';

export const ClaimIntimationSchema = z
    .object({
        username: z.string().min(1, { message: 'Username is required' }),
        memberNo: z.string().min(1, { message: 'Member Number is required' }),
        address: z.string().min(1, { message: 'Address is required' }),
        contact: z.string().min(1, { message: 'Contact is required' }),
        email: z.string().min(1, { message: 'Email is required' }).email('This is not a valid email.'),
        // firstNameInsured: z.string().min(1, { message: 'Contact is required' }),
        member: z.string().min(1, { message: 'Member selection is required' }),
        providerName: z.string().min(1, { message: 'Provider is required' }),
        admissionDate: z.coerce.date(),
        dischargeDate: z.coerce.date(),
        hospitalContact: z.string().min(1, { message: 'Contact is required' }),
        estimatedAmount: z.string().min(1, { message: 'Contact is required' }),
        diseaseName: z.string().min(1, { message: 'Contact is required' }),
        doctorName: z.string().min(1, { message: 'Contact is required' }),
        claimCategory: z.string().min(1, { message: 'Contact is required' }),
    })
    .refine((data) => data.dischargeDate > data.admissionDate, {
        message: 'Discharge Date cannot be earlier than Admission Date.',
        path: ['dischargeDate'],
    });
