/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/prop-types */
/* eslint-disable no-underscore-dangle */
// @ts-ignore
// @ts-nocheck

import { Card, Form, FormSelect, Row } from 'react-bootstrap';
import './styles.css';
import { useForm } from 'react-hook-form';
import { DevTool } from '@hookform/devtools';
import { FormEvent, useRef } from 'react';
import { useQueries, useMutation } from '@tanstack/react-query';
import { getRelation, getSelectedRelationDetails, saveMemberIntimationDetails } from '@api/SubmitClaim/submitClaim.api';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import isBetween from 'dayjs/plugin/isBetween';
import cogoToast from 'cogo-toast';
import { zodResolver } from '@hookform/resolvers/zod';
import { ClaimIntimationType, FamilyMemberDataType } from '../../../Types/types';
import { getPayLoad } from '../../../Payload';
import { getCapitallisedFromCamelCase, getErrorResultFromData, getResultFromData } from '../../../utils';
import { ClaimIntimationSchema } from './Validations';
import { isEmpty, omit } from 'lodash';
import { CLAIM_FORM_HEADERS } from '../../../constants';

dayjs.extend(customParseFormat);
dayjs.extend(isBetween);

const ClaimIntimation = () => {
    const { memberNo, memberName, address1, address2, insuredPhoneMobile, insuredEmailId, insuranceId, policyCode, employeeCode, tokenID } =
        JSON.parse(sessionStorage.getItem('user') || '{}');
    const {
        register,
        handleSubmit,
        control,
        setValue,
        getValues,
        formState: { errors },
    } = useForm({
        defaultValues: {
            username: memberName,
            memberNo,
            address: `${address1} ${address2}`,
            contact: insuredPhoneMobile,
            email: insuredEmailId,
            firstNameInsured: '',
            member: '',
            gender: '',
            dob: '',
            providerName: '',
            admissionDate: '',
            dischargeDate: '',
            hospitalContact: '',
            estimatedAmount: '',
            diseaseName: '',
            doctorName: '',
            claimCategory: '',
        },
        resolver: zodResolver(ClaimIntimationSchema),
    });

    const [familyMembers, { data: selectedMemberDetails, refetch: fetchSelectedMemberDetails }] = useQueries({
        queries: [
            {
                queryKey: ['familyMembers'],
                queryFn: () => getRelation(getPayLoad('familyMembers', undefined)),
                refetchOnWindowFocus: false,
            },
            {
                queryKey: ['fetchSelectedMemberDetailsForIntimation'],
                queryFn: () => getSelectedRelationDetails(getPayLoad('fetchSelectedMemberDetails', { memberShipNo: getValues('member') })),
                enabled: false,
                refetchOnWindowFocus: false,
                cacheTime: 0,
            },
        ],
    });

    const { mutate: postMemberIntimationDetails } = useMutation(saveMemberIntimationDetails);

    const admissionDateRef = useRef<HTMLInputElement>(null);
    const dischargeDateRef = useRef<HTMLInputElement>(null);

    const handleFormValues = (data: ClaimIntimationType) => {
        const {
            memberNo,
            member,
            providerName,
            admissionDate,
            dischargeDate,
            hospitalContact,
            estimatedAmount,
            diseaseName,
            doctorName,
            claimCategory,
        } = data;

        const distilledMemberData = getResultFromData(selectedMemberDetails);

        if (distilledMemberData) {
            const payload = {
                providerName,
                insuranceID: insuranceId,
                membershipNo: distilledMemberData.memberShipNo,
                memberName: member,
                policyCode,
                employeeCode,
                policyStartDate: dayjs(distilledMemberData.policyFrom, 'DD-MM-YYYY').format('DD/MM/YYYY'),
                policyEndDate: dayjs(distilledMemberData.policyUpto, 'DD-MM-YYYY').format('DD/MM/YYYY'),
                memberDateOfBirth: dayjs(distilledMemberData.memberDOB, 'DD-MM-YYYY').format('DD/MM/YYYY'),
                memberGender: distilledMemberData.genderCode,
                admissionDate: dayjs(admissionDate).format('DD/MM/YYYY'),
                dischargeDate: dayjs(dischargeDate).format('DD/MM/YYYY'),
                doctorName,
                diseaseName,
                estimatedAmount,
                roomCategory: '',
                memberMobileNo: insuredPhoneMobile,
                doctorMobileNo: hospitalContact,
                claimServiceType: 'IPD',
                claimCategory,
                entryBy: memberNo,
                tokenID,
            };
            const data = postMemberIntimationDetails(payload);
        }
    };

    const handleSelect = (e: FormEvent<HTMLInputElement | HTMLSelectElement>) => {
        switch ((e.target as HTMLInputElement).name) {
            case 'gender':
                setValue('gender', (e.target as HTMLInputElement).value);
                break;
            case 'member':
                setValue('member', (e.target as HTMLInputElement).value);
                fetchSelectedMemberDetails();
                break;
            case 'claimCategory':
                setValue('claimCategory', (e.target as HTMLInputElement).value);
                break;
            case 'admissionDate':
                // Validate policy period
                console.log(
                    dayjs((e.target as HTMLInputElement).value).isBetween(
                        selectedMemberDetails.policyFrom,
                        dayjs(selectedMemberDetails.policyUpto),
                        'day',
                        '[]'
                    )
                );
                // if(dayjs())
                setValue('admissionDate', (e.target as HTMLInputElement).value);
                break;
            case 'dischargeDate':
                // Validate policy period
                setValue('dischargeDate', (e.target as HTMLInputElement).value);
                break;
            default:
        }
    };

    if (familyMembers.isError) {
        cogoToast.error(getErrorResultFromData(familyMembers)?.message);
    }

    return (
        <main className="main">
            <Card className="rounded rounded-lg mt-4" style={{ width: '60%' }}>
                <Card.Title className="rounded-top rounded-lg p-2 cardTitleClaim">
                    <h5 className="m-0  ps-3">
                        <strong>Claim Intimation Form</strong>
                    </h5>
                </Card.Title>
                <Card.Body className="p-4" style={{ height: '41rem', overflowY: 'auto' }}>
                    <Form onSubmit={handleSubmit(handleFormValues)}>
                        <div className="p-3 border-bottom">
                            <div className="text-app-primary font-20">Particulars of the principal Insured</div>

                            <Row className="py-3">
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="username"
                                        placeholder="Member Name"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        style={{ fontSize: '0.8 rem !important' }}
                                        disabled
                                        {...register('username')}
                                    />
                                    {errors.username && <p className="text-sm text-danger errorText">{errors.username.message}</p>}
                                </Form.Group>

                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="memberNo"
                                        placeholder="Membership Number"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        disabled
                                        {...register('memberNo')}
                                    />
                                    {errors.memberNo && <p className="text-sm text-danger errorText">{errors.memberNo.message}</p>}
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="address"
                                        placeholder="Address of Communication"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        disabled
                                        {...register('address')}
                                    />
                                    {errors.address && <p className="text-sm text-danger errorText">{errors.address?.message}</p>}
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="contact"
                                        placeholder="Contact"
                                        type="number"
                                        className="ps-3 form-control inputSize"
                                        disabled
                                        {...register('contact')}
                                    />
                                    {errors.contact && <p className="text-sm text-danger errorText">{errors.contact?.message}</p>}
                                </Form.Group>

                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="email"
                                        placeholder="E-mail"
                                        type="email"
                                        className="ps-3 form-control inputSize"
                                        disabled
                                        {...register('email')}
                                    />
                                    {errors.email && <p className="text-sm text-danger errorText">{errors.email.message}</p>}
                                </Form.Group>
                            </Row>
                        </div>

                        <div className="p-3 border-bottom">
                            <div className="text-app-primary font-20">Details of the Insured Member whom the claim is preferred</div>
                            <Row className="py-3 flex-column">
                                <Form.Group className="col-md-4 py-2">
                                    <Form.Select
                                        defaultValue=""
                                        // variant="primary"
                                        id="member"
                                        title="Select Member"
                                        className="inputSize"
                                        style={{ color: 'grey' }}
                                        {...register('member')}
                                        disabled={!familyMembers?.data}
                                        onChange={handleSelect}
                                    >
                                        <option disabled value="">
                                            Select Member
                                        </option>
                                        {getResultFromData(familyMembers?.data)?.map((item: FamilyMemberDataType) => (
                                            <option key={item?.memberShipNo} value={item?.memberShipNo}>
                                                {item?.dependantName} - {item?.memberShipNo}
                                            </option>
                                        ))}
                                    </Form.Select>
                                    {errors.member && <p className="text-sm text-danger errorText">{errors.member.message}</p>}
                                </Form.Group>
                                <Form.Group className=" py-2">
                                    <table className="w-100">
                                        <thead>
                                            {Object.keys(omit(getResultFromData(selectedMemberDetails), ['genderCode']) || {})?.map((val) => (
                                                <th key={val} className="tableHeader">
                                                    {CLAIM_FORM_HEADERS[getCapitallisedFromCamelCase(val)] || getCapitallisedFromCamelCase(val)}
                                                </th>
                                            ))}
                                        </thead>
                                        <tbody>
                                            {Object.values(omit(getResultFromData(selectedMemberDetails), ['genderCode']) || {})?.map((val) => (
                                                <td key={val} className="tableData">
                                                    {' '}
                                                    {val}
                                                </td>
                                            ))}
                                        </tbody>
                                    </table>
                                </Form.Group>
                            </Row>
                        </div>

                        <div className="p-3">
                            <div className="text-app-primary font-20">Provider & Claim Details</div>
                            <Row className="py-3">
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="providerName"
                                        placeholder="Name &amp; Full Address Of The Provider"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        {...register('providerName')}
                                    />
                                    {errors.providerName && <p className="text-sm text-danger errorText">{errors.providerName.message}</p>}
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="admissionDate"
                                        disabled={!selectedMemberDetails}
                                        placeholder="Date of Admission"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        {...register('admissionDate')}
                                        onFocus={() => {
                                            if (admissionDateRef?.current?.type) {
                                                admissionDateRef.current.type = 'date';
                                            }
                                        }}
                                        onBlur={() => {
                                            if (admissionDateRef?.current?.type) {
                                                admissionDateRef.current.type = 'text';
                                            }
                                        }}
                                        ref={admissionDateRef}
                                        onChange={handleSelect}
                                    />
                                    {errors.admissionDate && <p className="text-sm text-danger errorText">{errors.admissionDate.message}</p>}
                                </Form.Group>

                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="hospitalName"
                                        disabled={!selectedMemberDetails}
                                        placeholder="Discharge Date"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        {...register('dischargeDate')}
                                        onFocus={() => {
                                            if (dischargeDateRef?.current?.type) {
                                                dischargeDateRef.current.type = 'date';
                                            }
                                        }}
                                        onBlur={() => {
                                            if (dischargeDateRef?.current?.type) {
                                                dischargeDateRef.current.type = 'text';
                                            }
                                        }}
                                        ref={dischargeDateRef}
                                        onChange={handleSelect}
                                    />
                                    {errors.dischargeDate && <p className="text-sm text-danger errorText">{errors.dischargeDate.message}</p>}
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="hospitalName"
                                        placeholder="Estimated Amount"
                                        type="number"
                                        className="ps-3 form-control inputSize"
                                        {...register('estimatedAmount')}
                                    />
                                    {errors.estimatedAmount && <p className="text-sm text-danger errorText">{errors.estimatedAmount.message}</p>}
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="hospitalName"
                                        placeholder="Disease Name"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        {...register('diseaseName')}
                                    />
                                    {errors.diseaseName && <p className="text-sm text-danger errorText">{errors.diseaseName.message}</p>}
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="hospitalName"
                                        placeholder="Doctor Name"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        {...register('doctorName')}
                                    />
                                    {errors.doctorName && <p className="text-sm text-danger errorText">{errors.doctorName.message}</p>}
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <Form.Select
                                        defaultValue=""
                                        // variant="primary"
                                        id="claimCategory"
                                        title="Select Claim Category"
                                        className="inputSize"
                                        style={{ color: 'grey' }}
                                        {...register('claimCategory')}
                                        onChange={handleSelect}
                                    >
                                        <option disabled value="">
                                            Select Claim Category
                                        </option>
                                        <option value="CAHSLESS">Cashless</option>
                                        <option value="REIMBURSEMENT">Reimbursement</option>
                                    </Form.Select>
                                    {errors.claimCategory && <p className="text-sm text-danger errorText">{errors.claimCategory.message}</p>}
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="hospitalContact"
                                        placeholder="Contact"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        {...register('hospitalContact')}
                                    />
                                    {errors.hospitalContact && <p className="text-sm text-danger errorText">{errors.hospitalContact.message}</p>}
                                </Form.Group>
                            </Row>
                        </div>
                        <Row className="submit-btn justify-content-center">
                            <button type="submit" className="btn btn-primary w-">
                                Submit
                            </button>
                        </Row>
                    </Form>
                    {/* </div> */}
                </Card.Body>
            </Card>
            <DevTool control={control} />
        </main>
    );
};

export default ClaimIntimation;
