// @ts-ignore
import { Button, Card } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { BarGeneralized } from '@components/Charts/chart';
import { AdobeIcon } from '@assets/adobeIcon';
import { faPhone, faLocationDot, faStar, faCircleQuestion, faArrowUpFromBracket } from '@fortawesome/free-solid-svg-icons';
import { ClaimsSummaryCardType, PlanBalancesCardType, DocumentCardType } from '../../Types/types';
import './styles.css';
import { injectID } from '../../utils';

export const ClaimsSummaryCard = ({
    name,
    location,
    country,
    contact,
    dateofservice,
    relation,
    reason,
    claimtype,
    coveragetype,
    networktype,
    status,
    hraclaimno,
    amountbilled,
    amountdeducted,
    plainpaid,
    paymentmode,
    paymentdetails,
}: ClaimsSummaryCardType) => {
    return (
        <main>
            <Card className="d-flex justify-content-between flex-row" data-type-card={status}>
                <section className="card--section--one">
                    <section className="card--header">
                        <div>
                            <h5 className="lab--name">{name}</h5>
                            <ul className="location--contact">
                                <li>
                                    <FontAwesomeIcon icon={faLocationDot} color="#6c63ff" /> {location},{country}
                                </li>
                                <li>
                                    <FontAwesomeIcon icon={faPhone} color="#6c63ff" /> {contact}{' '}
                                </li>
                            </ul>
                        </div>
                        <div className="network--type font-12">{networktype} NETWORK</div>
                    </section>
                    <hr style={{ margin: '0' }} />
                    <section className="second--part">
                        <section className="p-2">
                            <p className="m-0 font-12 text-muted ">Date of service</p>
                            {dateofservice}
                        </section>
                        <section className="p-2 ">
                            <p className="m-0 font-12 text-muted ">Relation</p>
                            {relation}
                        </section>
                        <section className="p-2 ">
                            <p className="m-0 font-12 text-muted ">Reason</p>
                            {reason}
                        </section>
                    </section>
                    <hr style={{ margin: '0' }} />
                    <section className="third--part">
                        <section className="p-2">
                            <p className="m-0 font-12 text-muted ">Claim Type</p>
                            {claimtype}
                        </section>
                        <section className="p-2">
                            <p className="m-0 font-12 text-muted ">Coverage Type</p>
                            {injectID(coveragetype)?.map((type: { data: string; id: string }) => (
                                <Button className="coverage--type font-12" key={type.id}>
                                    {type.data}
                                </Button>
                            ))}
                        </section>
                        {/* Buttons */}
                    </section>

                    <section className="d-flex">
                        <ul className="location--contact" style={{ gap: '0.5rem' }}>
                            <li>
                                <Button className="coverage--type font-12 mt-5">
                                    <FontAwesomeIcon icon={faStar} /> Benefit Details
                                </Button>
                            </li>
                            <li>
                                <Button className="coverage--type font-12 mt-5">
                                    <FontAwesomeIcon icon={faCircleQuestion} /> Question
                                </Button>
                            </li>
                            <li>
                                <Button className="coverage--type font-12 mt-5">
                                    <FontAwesomeIcon icon={faArrowUpFromBracket} /> View Document
                                </Button>
                            </li>
                        </ul>
                    </section>
                </section>
                <section className="card--section--two">
                    <div style={{ padding: '1rem' }}>
                        <section className="p-2">
                            <p className="m-0 font-12 text-muted ">HRA Claim No.</p>
                            {hraclaimno}
                        </section>
                        <section className="p-2">
                            <p className="m-0 font-12 text-muted ">Amount Billed</p>
                            {amountbilled}
                        </section>
                        <div className="d-flex justify-content-between">
                            <section className="p-2">
                                <p className="m-0 font-12 text-muted "> Deducted</p>
                                {amountdeducted}
                            </section>
                            <section className="p-2">
                                <p className="m-0 font-12 text-muted "> Plan Paid</p>
                                {plainpaid}
                            </section>
                        </div>
                    </div>

                    <div className="payment--details card-footer" data-type={status}>
                        <Button
                            className="coverage--type font-12"
                            style={{ backgroundColor: 'white', color: 'black', border: 'none', width: 'fit-content' }}
                        >
                            {claimtype}
                        </Button>
                        <div className="d-flex justify-content-between" style={{ color: 'white' }}>
                            <section className="p-2">
                                <p className="m-0 font-12 text-muted ">Payment By</p>
                                {paymentmode}
                            </section>
                            <section className="p-2">
                                <p className="m-0 font-12 text-muted ">Payment Details</p>
                                {paymentdetails}
                            </section>
                        </div>
                    </div>
                </section>
            </Card>
        </main>
    );
};

export const PlanBalancesCard = ({ name, figure, table }: PlanBalancesCardType) => {
    return (
        <main>
            <Card className="rounded rounded-lg pb-5">
                <h4 className="plan-heading">{name}</h4>
                <hr />
                <BarGeneralized {...structuredClone(figure)} />
            </Card>
        </main>
    );
};

export const DocumentsCard = ({ name, companyname, issuedate }: DocumentCardType) => {
    return (
        <main>
            <Card className="rounded rounded-lg doc--card">
                <AdobeIcon />
                <div>
                    <h4 className="claimsheaderSVG" style={{ textAlign: 'left' }}>
                        {name}
                    </h4>
                    <p className="font-12 opacity-50">{companyname}</p>
                </div>
                <section>
                    <p className="font-12 opacity-50">Issue Date</p>
                    <p className="font-14">{issuedate}</p>
                </section>
                <Button className="download--button--doc">Download</Button>
            </Card>
        </main>
    );
};
