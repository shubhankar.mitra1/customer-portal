/* eslint-disable no-nested-ternary */
// @ts-ignore
// @ts-nocheck
import { useEffect, useRef, useState } from 'react';
import _ from 'lodash';
import { QueryCache } from '@tanstack/react-query';
import SubLayout from '@layout/SubLayout';
import { Pie } from '@components/Charts/chart';
import { useQueries } from '@tanstack/react-query';
import { Button, ButtonGroup, Card, Form } from 'react-bootstrap';
import './styles.css';
import Select from 'react-select';
import { useImmer } from 'use-immer';
import { getByAgeGroup, getByClaimType, getByDurationOfStay, getByGender, getByLocation, getByTreatment } from '@api/PlanBalances/planBalances.api';
import { getStatus, memberClaimAnnualMade, memberClaimsProvider, populateRelation } from '@api/ClaimsSummary/claimSummary.api';
import cogoToast from 'cogo-toast';
import { ClaimsSummaryCardType } from '@srctypes/types';
import Spinner from '@components/Spinner/Spinner';
import { getPayLoad } from '../../Payload';
import { ClaimsSummaryCard, PlanBalancesCard, DocumentsCard } from './Cards';
import { PAID, INPROCESS, REJECTED } from '../../constants';

import { pieProps, claimsSummaryProps, PlanBalancesCardProps, DocumentsCardProps } from './props';
import ClaimIntimation from './SubmitIntimation/SubmitIntimation';
import SubmitClaim from './SubmitClaim/SubmitClaim';
import UploadDocuments from './UploadDocuments/UploadDocuments';
import { getResultFromData } from '../../utils';
import {
    getAgeGroupRefinedData,
    getClaimTypeRefinedData,
    getDurationOfStayRefinedData,
    getGenderRefinedData,
    getLocationRefinedData,
    getTreatmentRefinedData,
} from './helper';

const ClaimsSummary = () => {
    const [dropDownValues, setDropDownValues] = useState({
        relation: '',
        pageNo: 0,
        status: '',
        claimType: '',
    });
    const [
        { data: memberclaimannualmade },
        { refetch: getMemberClaims, data: memberClaimsData, isLoading, isFetching },
        { data: statusOptions },
        { data: relationOptions },
    ] = useQueries({
        queries: [
            {
                queryKey: ['memberclaimannualmade'],
                queryFn: () => memberClaimAnnualMade(getPayLoad('memberclaimannualmade', undefined)),
                refetchOnWindowFocus: false,
            },
            {
                queryKey: ['memberclaimsprovider', dropDownValues],
                queryFn: () =>
                    memberClaimsProvider(
                        getPayLoad('memberclaimsprovider', {
                            relation: dropDownValues.relation,
                            pageNo: dropDownValues.pageNo,
                            status: dropDownValues.status,
                            claimType: dropDownValues.claimType,
                        })
                    ),

                refetchOnWindowFocus: false,
                cacheTime: 0,
                staleTime: 0,
                select(data: any) {
                    const result = getResultFromData(data);

                    if (result) {
                        const dummyObj = { ...claimsSummaryProps[0] };
                        return result.map(
                            (item: {
                                amountBilled: any;
                                claimStatus: any;
                                claimType: any;
                                dateOfServices: any;
                                deductedAmount: any;
                                hospitalName: any;
                                hospitalState: any;
                                hraClaimNo: any;
                                netWork: any;
                                paidAmount: any;
                                paymentBy: any;
                                paymentDetails: any;
                                questions: any;
                                reason: any;
                                relation: any;
                                treatment: any;
                            }) => {
                                const {
                                    amountBilled,
                                    claimStatus,
                                    claimType,
                                    dateOfServices,
                                    deductedAmount,
                                    hospitalName,
                                    hospitalState,
                                    hraClaimNo,
                                    netWork,
                                    paidAmount,
                                    paymentBy,
                                    paymentDetails,
                                    questions,
                                    reason,
                                    relation,
                                    treatment,
                                } = item;
                                dummyObj.name = hospitalName;
                                dummyObj.location = hospitalState;
                                dummyObj.country = '';
                                dummyObj.contact = '';
                                dummyObj.dateofservice = dateOfServices;
                                dummyObj.relation = relation;
                                dummyObj.reason = reason;
                                dummyObj.claimtype = claimType;
                                dummyObj.coveragetype = [];
                                dummyObj.networktype = netWork;
                                dummyObj.status = claimStatus;
                                dummyObj.hraclaimno = hraClaimNo;
                                dummyObj.amountbilled = amountBilled;
                                dummyObj.amountdeducted = deductedAmount;
                                dummyObj.plainpaid = paidAmount;
                                dummyObj.paymentmode = paymentBy;
                                dummyObj.paymentdetails = paymentDetails;

                                return { ...dummyObj, id: crypto?.randomUUID?.() };
                            }
                        );
                    }
                    return null;
                },
            },
            {
                queryKey: ['populatestatus'],
                queryFn: () => getStatus(getPayLoad('populatestatus', undefined)),
                refetchOnWindowFocus: false,
            },
            {
                queryKey: ['memberclaimmadeonpoliciesrelation'],
                queryFn: () => populateRelation(getPayLoad('memberclaimmadeonpoliciesrelation', undefined)),
                refetchOnWindowFocus: false,
            },
        ],
    });

    const [series, setSeries] = useState([]);
    const [labels, setLabels] = useState([]);

    const claimRef = useRef<any>(null);
    const statusRef = useRef<any>(null);
    const relationRef = useRef<any>(null);

    const filterList = (context: string) => {
        switch (context) {
            case 'clear':
                getMemberClaims();

                break;
            case 'filter':
                getMemberClaims();

                break;
            default:
                cogoToast.error('Invalid Case in:filterList');
        }
    };
    useEffect(() => {
        if (memberclaimannualmade) {
            const annualData = getResultFromData(memberclaimannualmade);

            if (annualData) {
                setLabels(annualData.map((item: { relation: string }) => item.relation));
                setSeries(
                    annualData
                        .map((item: { totalBilling: string }) => item.totalBilling)
                        .map((item: string) => +item?.split('.')?.[0]?.split(',')?.join('') || 0)
                );
            } else {
                cogoToast.error('No Data Found');
            }
        }
    }, [memberclaimannualmade]);

    /**
     * setDropDownValues
     */

    return (
        <main className="claimsMain">
            <section>
                <div className="select">
                    <Select
                        isSearchable
                        ref={claimRef}
                        placeholder="Claim type"
                        options={['Cashless', 'Reimbursement'].map((item: string) => ({
                            isFixed: true,
                            label: item,
                            value: item.toUpperCase(),
                        }))}
                        onChange={(value) =>
                            setDropDownValues((draft) => ({
                                ...draft,
                                claimType: value?.value || '',
                            }))
                        }
                    />
                    <Select
                        isSearchable
                        placeholder="Status"
                        ref={statusRef}
                        options={[...(getResultFromData(statusOptions) || [])].map((item: { statuse: string }) => ({
                            isFixed: true,
                            label: item.statuse,
                            value: item.statuse.toUpperCase(),
                        }))}
                        onChange={(value) =>
                            setDropDownValues((draft) => ({
                                ...draft,
                                status: value?.value || '',
                            }))
                        }
                    />
                    <Select
                        isSearchable
                        placeholder="Relation"
                        ref={relationRef}
                        options={[...(getResultFromData(relationOptions) || [])].map((item: { relationWithProposer: string }) => ({
                            isFixed: true,
                            label: item.relationWithProposer,
                            value: item.relationWithProposer.toUpperCase(),
                        }))}
                        onChange={(value) =>
                            setDropDownValues((draft) => ({
                                ...draft,
                                relation: value?.value || '',
                            }))
                        }
                    />
                    {/* <button type="button" onClick={() => filterList('filter')} className="btn btn-primary">
                        Search
                    </button> */}

                    <button
                        type="button"
                        onClick={() => {
                            setDropDownValues(() => ({
                                relation: '',
                                pageNo: 0,
                                status: '',
                                claimType: '',
                            }));

                            claimRef.current.setValue([]);
                            statusRef.current.setValue([]);
                            relationRef.current.setValue([]);

                            filterList('clear');
                        }}
                        className="btn btn-primary"
                    >
                        Clear
                    </button>
                </div>
                <div className="mt-2 cardWidth">
                    {memberClaimsData ? (
                        memberClaimsData.map((item: JSX.IntrinsicAttributes & ClaimsSummaryCardType) => <ClaimsSummaryCard key={item.id} {...item} />)
                    ) : isLoading || isFetching ? (
                        <Spinner suspense={false} width="30px" height="30px" />
                    ) : (
                        <div>
                            <h1>No Data Found</h1>
                        </div>
                    )}
                </div>
            </section>
            <section className="pt-1" style={{ width: '28rem' }}>
                <Card className="rounded text-center">
                    <p className="claimsheaderSVG">Annuals Claims made</p>
                    <Pie {...pieProps(series, labels)} />

                    <p style={{ font: '14px', opacity: '0.5' }}>Total Billing</p>
                    <h1 className="claimsheaderSVG" style={{ marginTop: '0px' }}>
                        INR{' '}
                        {series.reduce((acc, item) => {
                            return acc + item;
                        }, 0)}
                    </h1>

                    <section className="amountDetails">
                        <section className="d-flex justify-content-between flex-column align-items-stretch">
                            {labels?.map((item, index) => {
                                return (
                                    <div className="d-flex gap-2 align-items-center justify-content-between" key={index}>
                                        <div className="d-flex" style={{ columnGap: '0.5rem' }}>
                                            <div className="rounds"> </div>
                                            <span style={{ opacity: '0.5' }}>{item}</span>
                                        </div>
                                        <h6 className="amount d-flex align-items-end">INR {series[index]}</h6>
                                    </div>
                                );
                            })}
                        </section>
                        <p style={{ font: '14px', opacity: '0.5' }} className="mt-4">
                            {' '}
                            Total Billed for Rahul Saxena as on 01/03/2021
                        </p>
                    </section>
                </Card>
            </section>
        </main>
    );
};

const MyClaims = () => {
    return (
        <main className="myClaimsMain">
            <section className="section--one--myclaims">
                <Card className="optionsCard">
                    <input type="text" placeholder="Search.." className="searchbox ps-3 form-control w-100" />
                    <Form className="w-100 mt-3">
                        <Form.Label>Claim Type</Form.Label>
                        <Select
                            isSearchable
                            placeholder="Claim Type"
                            options={['Cashless', 'Reimbursement'].map((item: string) => ({
                                isFixed: true,
                                label: item,
                                value: item.toLowerCase(),
                            }))}
                        />
                    </Form>
                    <hr className="w-100" />
                    {['Coverage Type', 'Members', 'Date Range', 'Current Status'].map((item, index) => (
                        <details key={item.toLowerCase()} className="w-100 mt-1">
                            <summary>{item}</summary>
                            {item === 'Coverage Type' && (
                                <ul className="coverage--type--options mt-3">
                                    {['Medical', 'Dental', 'Pharmacy'].map((option) => (
                                        <li key={option}>
                                            {' '}
                                            <Form.Label>
                                                <input type="checkbox" /> {option}
                                            </Form.Label>
                                        </li>
                                    ))}
                                </ul>
                            )}

                            {item === 'Members' && (
                                <ul className="coverage--type--options mt-3">
                                    {['Self', 'Mother', 'Father', 'Spouse', 'Son', 'Daughter'].map((member) => (
                                        <li key={member}>
                                            {' '}
                                            <Form.Label>
                                                <input type="checkbox" /> {member}
                                            </Form.Label>
                                        </li>
                                    ))}
                                </ul>
                            )}
                            {item === 'Date Range' && (
                                <>
                                    <Form.Control type="date" className="m-1 font-12" />
                                    <Form.Control type="date" className="m-1 font-12" />
                                </>
                            )}
                            {item === 'Current Status' && (
                                <ul className="coverage--type--options mt-3">
                                    {[PAID, INPROCESS, REJECTED].map((status) => (
                                        <li key={status}>
                                            {' '}
                                            <Form.Label>
                                                <input type="checkbox" /> {status}
                                            </Form.Label>
                                        </li>
                                    ))}
                                </ul>
                            )}
                            {index !== 3 && <hr className="w-100 m-0" />}
                        </details>
                    ))}
                </Card>
            </section>
            <section className="section--two--myclaims">
                <section className="header--two--myclaims">
                    <h5>Displaying Claims</h5>
                    <p className="font-12">4 of 4</p>
                    <p className="font-12 text-muted">From 04/02/2020 to 04/02/2021</p>
                    <p className="font-14 text-muted self">Sort By</p>
                    <Select
                        className="select--sort"
                        isSearchable
                        placeholder="Sort"
                        options={['Newest First'].map((item: string) => ({
                            isFixed: true,
                            label: item,
                            value: item.toLowerCase(),
                        }))}
                    />
                </section>
                <section className="content--two--myclaims">
                    {claimsSummaryProps.map((item) => (
                        <ClaimsSummaryCard key={item.id} {...item} />
                    ))}
                </section>
            </section>
        </main>
    );
};

const PlanBalances = () => {
    const [planBalancesCard, setPlanBalancesCard] = useImmer([]);

    // const [
    //     { data: claimTypeData },
    //     { data: treatmentData },
    //     { data: genderData },
    //     { data: locationData },
    //     { data: ageGroupData },
    //     { data: durationOfStayData },
    // ]
    useQueries({
        queries: [
            {
                queryKey: ['getByClaimType'],
                queryFn: () => getByClaimType(getPayLoad('getByClaimType', undefined)),
                refetchOnWindowFocus: false,
                onSuccess(claimTypeData: any) {
                    if (claimTypeData && planBalancesCard.find((item) => item.name === 'By Claim Type') === undefined) {
                        const isClaimTypeSucceeded = getResultFromData(claimTypeData);

                        if (isClaimTypeSucceeded) {
                            setPlanBalancesCard((planBalancesCardDraft) => {
                                planBalancesCardDraft.push({ ...getClaimTypeRefinedData(PlanBalancesCardProps, isClaimTypeSucceeded) });
                            });
                        }
                    }
                },
            },
            {
                queryKey: ['getByTreatment'],
                queryFn: () => getByTreatment(getPayLoad('getByTreatment', undefined)),
                refetchOnWindowFocus: false,
                onSuccess(treatmentData: any) {
                    if (treatmentData && planBalancesCard.find((item) => item.name === 'By Treatment') === undefined) {
                        const isTreatmentSucceeded = getResultFromData(treatmentData);
                        if (isTreatmentSucceeded) {
                            setPlanBalancesCard((planBalancesCardDraft) => {
                                planBalancesCardDraft.push({ ...getTreatmentRefinedData(PlanBalancesCardProps, isTreatmentSucceeded) });
                            });
                        }
                    }
                },
            },
            {
                queryKey: ['getByGender'],
                queryFn: () => getByGender(getPayLoad('getByGender', undefined)),
                refetchOnWindowFocus: false,
                onSuccess(genderData: any) {
                    if (genderData && planBalancesCard.find((item) => item.name === 'By Gender') === undefined) {
                        const isGenderSucceeded = getResultFromData(genderData);

                        if (isGenderSucceeded) {
                            setPlanBalancesCard((planBalancesCardDraft) => {
                                planBalancesCardDraft.push({ ...getGenderRefinedData(PlanBalancesCardProps, isGenderSucceeded) });
                            });
                        }
                    }
                },
            },
            {
                queryKey: ['getByLocation'],
                queryFn: () => getByLocation(getPayLoad('getByLocation', undefined)),
                refetchOnWindowFocus: false,
                onSuccess(locationData: any) {
                    if (locationData && planBalancesCard.find((item) => item.name === 'By Location') === undefined) {
                        const isLocationSucceeded = getResultFromData(locationData);

                        if (isLocationSucceeded) {
                            setPlanBalancesCard((planBalancesCardDraft) => {
                                planBalancesCardDraft.push({ ...getLocationRefinedData(PlanBalancesCardProps, isLocationSucceeded) });
                            });
                        }
                    }
                },
            },
            {
                queryKey: ['getByAgeGroup'],
                queryFn: () => getByAgeGroup(getPayLoad('getByAgeGroup', undefined)),
                refetchOnWindowFocus: false,
                onSuccess(ageGroupData: any) {
                    if (ageGroupData && planBalancesCard.find((item) => item.name === 'By Age Group') === undefined) {
                        const isAgeGroupSucceeded = getResultFromData(ageGroupData);

                        if (isAgeGroupSucceeded) {
                            setPlanBalancesCard((planBalancesCardDraft) => {
                                planBalancesCardDraft.push({ ...getAgeGroupRefinedData(PlanBalancesCardProps, isAgeGroupSucceeded) });
                            });
                        }
                    }
                },
            },
            {
                queryKey: ['getByDurationOfStay'],
                queryFn: () => getByDurationOfStay(getPayLoad('getByDurationOfStay', undefined)),
                refetchOnWindowFocus: false,
                onSuccess(durationOfStayData: any) {
                    if (durationOfStayData && planBalancesCard.find((item) => item.name === 'By Duration of Stay') === undefined) {
                        const isDurationOfStaySucceeded = getResultFromData(durationOfStayData);

                        if (isDurationOfStaySucceeded) {
                            setPlanBalancesCard((planBalancesCardDraft) => {
                                planBalancesCardDraft.push({ ...getDurationOfStayRefinedData(PlanBalancesCardProps, isDurationOfStaySucceeded) });
                            });
                        }
                    }
                },
            },
        ],
    });

    return (
        <main className="d-flex flex-column pt-4">
            <section className="plan--balances--header">
                <h5>Financial Account Summary</h5>
                <div className="d-flex align-items-baseline gap-3 sort--div">
                    <p className="font-14 text-muted">Sort By</p>
                    <Select
                        isSearchable
                        placeholder="Sort"
                        options={['2020 - 2021'].map((item: string) => ({
                            isFixed: true,
                            label: item,
                            value: item.toLowerCase(),
                        }))}
                        className="sort--div--select"
                    />
                </div>
            </section>

            <section className="plan--details">
                <details>
                    <summary className="details--section rounded">
                        <section>
                            <p className="font-12 opacity-50 text-white">Member ID</p>
                            <p className="text-white font-14">ABCD123400002</p>
                        </section>
                        <section>
                            {' '}
                            <p className="font-12 opacity-50 text-white">Name</p>
                            <p className="text-white font-14">Rahul Saxena</p>
                        </section>
                        <section>
                            {' '}
                            <p className="font-12 opacity-50 text-white">Scheme</p>
                            <p className="text-white font-14">Family Floater Plan</p>
                        </section>
                        <section>
                            {' '}
                            <p className="font-12 opacity-50 text-white">Overall value</p>
                            <p className="text-white font-14">INR 10,00,000</p>
                        </section>
                        <section>
                            {' '}
                            <p className="font-12 opacity-50 text-white">Start Date</p>
                            <p className="text-white font-14">15 Jul 2020</p>
                        </section>
                        <section>
                            {' '}
                            <p className="font-12 opacity-50 text-white">Expiry Date</p>
                            <p className="text-white font-14">14 Jul 2025</p>
                        </section>
                        <Button className="download--button font-12">Download</Button>
                    </summary>

                    <div className="details--details">
                        <div className="card-body">
                            <table className="table">
                                <thead className="">
                                    <tr className="">
                                        <td className="opacity-7 font-14">Member Id</td>
                                        <td className="opacity-7 font-14">Patient Name</td>
                                        <td className="opacity-7 font-14">Invoice</td>
                                        <td className="text-end opacity-7 font-14">Claim Amount</td>
                                        <td className="text-end opacity-7 font-14">Benefit Amount</td>
                                        <td className="text-end opacity-7 font-14">Indemnity</td>
                                        <td className="text-end opacity-7 font-14">Not Covered</td>
                                        <td className="opacity-7 font-14">Payee</td>
                                    </tr>
                                </thead>
                                <tbody className="">
                                    <tr className="">
                                        <td className=" font-16">12 May 2021</td>
                                        <td className=" font-16">Harshvardhana Saxena</td>
                                        <td className=" font-16">EOX1234000567</td>
                                        <td className="text-end  font-16">24,375.00</td>
                                        <td className="text-end  font-16">24,375.00</td>
                                        <td className="text-end  font-16">0.00</td>
                                        <td className="text-end  font-16">0.00</td>
                                        <td className=" font-16">City Wide Hospital</td>
                                    </tr>
                                    <tr className="">
                                        <td className=" font-16">12 May 2021</td>
                                        <td className=" font-16">Harshvardhana Saxena</td>
                                        <td className=" font-16">EOX1234000567</td>
                                        <td className="text-end  font-16">24,375.00</td>
                                        <td className="text-end  font-16">24,375.00</td>
                                        <td className="text-end  font-16">0.00</td>
                                        <td className="text-end  font-16">0.00</td>
                                        <td className=" font-16">City Wide Hospital</td>
                                    </tr>
                                    <tr className="">
                                        <td className=" font-16">12 May 2021</td>
                                        <td className=" font-16">Harshvardhana Saxena</td>
                                        <td className=" font-16">EOX1234000567</td>
                                        <td className="text-end  font-16">24,375.00</td>
                                        <td className="text-end  font-16">24,375.00</td>
                                        <td className="text-end  font-16">0.00</td>
                                        <td className="text-end  font-16">0.00</td>
                                        <td className=" font-16">City Wide Hospital</td>
                                    </tr>
                                    <tr className="">
                                        <td className=" font-16">12 May 2021</td>
                                        <td className=" font-16">Harshvardhana Saxena</td>
                                        <td className=" font-16">EOX1234000567</td>
                                        <td className="text-end  font-16">24,375.00</td>
                                        <td className="text-end  font-16">24,375.00</td>
                                        <td className="text-end  font-16">0.00</td>
                                        <td className="text-end  font-16">975.00</td>
                                        <td className=" font-16">City Wide Hospital</td>
                                    </tr>
                                    <tr className="">
                                        <td colSpan={3} className="opacity-50">
                                            TOTAL
                                        </td>
                                        <td className="text-end">1,20,545.00</td>
                                        <td colSpan={2} className="text-end">
                                            0.00
                                        </td>
                                        <td className="text-danger text-end">975.00</td>
                                    </tr>
                                    <tr className="">
                                        <td colSpan={3} className="opacity-50">
                                            BENEFIT BALANCE
                                        </td>
                                        <td className="text-success-custom text-end" style={{ color: 'green' }}>
                                            1,20,545.00
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </details>
            </section>
            {/* Layout Grid */}
            <section className="plan--cards mt-3">
                {/* PlanBalancesCardProps */}
                {planBalancesCard.map((props) => (
                    // eslint-disable-next-line react/prop-types
                    <PlanBalancesCard key={props.name} {...props} />
                ))}
            </section>
        </main>
    );
};

const Documents = () => {
    return (
        <main className="d-flex flex-column pt-4">
            <section className="plan--balances--header">
                <h5>Documents for the year 2020 - 21</h5>
                <div className="d-flex align-items-baseline gap-3 sort--div">
                    <p className="font-14 text-muted">Sort By</p>
                    <Select
                        isSearchable
                        placeholder="Sort"
                        options={['Default'].map((item: string) => ({
                            isFixed: true,
                            label: item,
                            value: item.toLowerCase(),
                        }))}
                        className="sort--div--select"
                    />
                </div>
            </section>
            <section className="documents">
                {DocumentsCardProps.map((item) => {
                    return <DocumentsCard key={item.id} {...item} />;
                })}
            </section>
        </main>
    );
};

const Claims = () => {
    const [active, setActive] = useState('summary');
    const intimationModalRef = useRef<HTMLDialogElement>(null);

    return (
        <SubLayout>
            <main style={{ padding: '1rem' }}>
                <section className="nav">
                    <nav>
                        <ButtonGroup size="lg" className="mb-2">
                            <Button
                                className="btnGrp-nav font-12 border-0"
                                onClick={() => setActive('summary')}
                                data-active={active === 'summary' ? active : 'null'}
                            >
                                Claims Summary
                            </Button>
                            <Button
                                className="btnGrp-nav font-12 border-0"
                                onClick={() => setActive('myclaims')}
                                data-active={active === 'myclaims' ? active : 'null'}
                            >
                                My Claims
                            </Button>
                            <Button
                                className="btnGrp-nav font-12 border-0"
                                onClick={() => setActive('balances')}
                                data-active={active === 'balances' ? active : 'null'}
                            >
                                Plan Balances
                            </Button>
                            <Button
                                className="btnGrp-nav font-12 border-0"
                                onClick={() => setActive('docs')}
                                data-active={active === 'docs' ? active : 'null'}
                            >
                                Documents
                            </Button>
                        </ButtonGroup>
                    </nav>
                    <section style={{ display: 'flex', gap: '1rem' }}>
                        <Button
                            className="btnGrp-nav font-12 border-0"
                            onClick={() => setActive('submit-intimation') /* intimationModalRef.current?.showModal() */}
                        >
                            Claim Intimation
                        </Button>
                        <Button className="btnGrp-nav font-12 border-0" onClick={() => setActive('submit-claim')}>
                            Claim Submission
                        </Button>
                        <Button
                            className="btnGrp-nav font-12 border-0"
                            onClick={() => setActive('upload') /* intimationModalRef.current?.showModal() */}
                        >
                            Upload Documents
                        </Button>
                    </section>
                    <dialog ref={intimationModalRef} className="dialog__modal">
                        <section style={{ width: '90vw', height: '81vh', overflowY: 'auto' }}>
                            <h1>hello</h1>
                        </section>
                    </dialog>
                </section>

                <section className="content">
                    {active === 'summary' ? <ClaimsSummary /> : null}
                    {active === 'myclaims' ? <MyClaims /> : null}
                    {active === 'balances' ? <PlanBalances /> : null}
                    {active === 'docs' ? <Documents /> : null}
                    {active === 'submit-intimation' ? <ClaimIntimation /> : null}
                    {active === 'submit-claim' ? <SubmitClaim /> : null}
                    {active === 'upload' ? <UploadDocuments /> : null}
                </section>
            </main>
        </SubLayout>
    );
};

export default Claims;
