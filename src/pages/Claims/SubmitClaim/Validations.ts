import * as z from 'zod';

export const ClaimSubmissionSchema = z
    .object({
        username: z.string().min(1, { message: 'Username is required' }),
        memberNo: z.string().min(1, { message: 'Member Number is required' }),
        address: z.string().min(1, { message: 'Address is required' }),
        contact: z.string().min(1, { message: 'Contact is required' }),
        email: z.string().min(1, { message: 'Email is required' }).email('This is not a valid email.'),
        // firstNameInsured: z.string().min(1, { message: 'Contact is required' }),
        member: z.string().min(1, { message: 'Member selection is required' }),
        providerName: z.string().min(1, { message: 'Provider is required' }),
        admissionDate: z.coerce.date(),
        dischargeDate: z.coerce.date(),
        hospitalContact: z.string().min(1, { message: 'Contact is required' }),
        estimatedAmount: z.string().min(1, { message: 'Contact is required' }),
        diseaseName: z.string().min(1, { message: 'Contact is required' }),
        doctorName: z.string().min(1, { message: 'Contact is required' }),
        claimCategory: z.string().min(1, { message: 'Contact is required' }),
    })
    .refine((data) => data.dischargeDate > data.admissionDate, {
        message: 'Discharge Date cannot be earlier than Admission Date.',
        path: ['dischargeDate'],
    });
