import { FormEvent, useEffect, useRef } from 'react';
import { Card, Form, Row } from 'react-bootstrap';
import './styles.css';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { DevTool } from '@hookform/devtools';
import {
    getClaimTypeAndSubTypeValues,
    getRelation,
    getSelectedRelationDetails,
    getServiceTypeValues,
    saveMemberClaimnDetails,
} from '@api/SubmitClaim/submitClaim.api';
import { useMutation, useQueries } from '@tanstack/react-query';
import { FamilyMemberDataType } from '../../../Types/types';
import { getPayLoad } from '../../../Payload';
import { getCapitallisedFromCamelCase, getErrorResultFromData, getResultFromData } from '../../../utils';
import dayjs from 'dayjs';
import { omit } from 'lodash';
import { CLAIM_FORM_HEADERS } from '../../../constants';
import cogoToast from 'cogo-toast';

const SubmitClaim = () => {
    const { memberNo, memberName, address1, address2, insuredPhoneMobile, insuredEmailId, insuranceId, policyCode, employeeCode, tokenID, tpaCode } =
        JSON.parse(sessionStorage.getItem('user') || '{}');
    const {
        register,
        handleSubmit,
        control,
        setValue,
        getValues,
        formState: { errors },
    } = useForm({
        defaultValues: {
            username: memberName,
            memberNo,
            address: `${address1} ${address2}`,
            contact: insuredPhoneMobile,
            email: insuredEmailId,
            firstNameInsured: '',
            member: '',
            gender: '',
            dob: '',
            providerName: '',
            admissionDate: '',
            dischargeDate: '',
            hospitalContact: '',
            estimatedAmount: '',
            diseaseName: '',
            doctorName: '',
            claimCategory: '',
            claimType: '',
            claimSubType: '',
            serviceType: '',
            tpaCode: '',
        },
        // resolver: zodResolver(ClaimIntimationSchema),
    });

    const [
        familyMembers,
        { data: selectedMemberDetails, refetch: fetchSelectedMemberDetails },
        { data: serviceTypeOptions },
        { data: claimTypeOptions, refetch: fetchClaimTypeOptions },
        { data: claimSubTypeOptions, refetch: fetchClaimSubTypeOptions },
    ] = useQueries({
        queries: [
            {
                queryKey: ['familyMembers'],
                queryFn: () => getRelation(getPayLoad('familyMembers', undefined)),
                refetchOnWindowFocus: false,
            },
            {
                queryKey: ['fetchSelectedMemberDetailsForClaim'],
                queryFn: () => getSelectedRelationDetails(getPayLoad('fetchSelectedMemberDetails', { memberShipNo: getValues('member') })),
                enabled: false,
                refetchOnWindowFocus: false,
                cacheTime: 0,
            },
            {
                queryKey: ['fetchServiceType'],
                queryFn: () => getServiceTypeValues(getPayLoad('fetchServiceType', undefined)),
                refetchOnWindowFocus: false,
            },
            {
                queryKey: ['fetchClaimType'],
                queryFn: () => getClaimTypeAndSubTypeValues({ ...getPayLoad('fetchClaimType', undefined), parentParamID: getValues('serviceType') }),
                enabled: false,
                refetchOnWindowFocus: false,
            },
            {
                queryKey: ['fetchClaimSubType'],
                queryFn: () => getClaimTypeAndSubTypeValues({ ...getPayLoad('fetchClaimSubType', undefined), parentParamID: getValues('claimType') }),
                enabled: false,
                refetchOnWindowFocus: false,
            },
        ],
    });

    const { mutate: postMemberClaimDetails, data: memberClaimSavedData } = useMutation(saveMemberClaimnDetails);

    const admissionDateRef = useRef<HTMLInputElement>(null);
    const dischargeDateRef = useRef<HTMLInputElement>(null);

    const handleFormValues = (data) => {
        const {
            memberNo,
            member,
            providerName,
            admissionDate,
            dischargeDate,
            hospitalContact,
            estimatedAmount,
            diseaseName,
            doctorName,
            claimCategory,
            // tpaCode,
            claimSubType,
            claimType,
        } = data;

        const distilledMemberData = getResultFromData(selectedMemberDetails);

        /**
         * 


{
    "providerName": "Provider Hospital",
    "insuranceID": "2018111904592049591771010",
    "membershipNo": "00K02664502",
    "memberName": "NIMISH KUMAR  MISHRA",
    "tpaCode":"00K",
    "employeeCode": "E0001",
    "policyCode": "LK-P0000000019",
    "admissionDate": "26/07/2023",
    "dischargeDate": "27/07/2023",
    "diseaseName": "cataract",
    "estimatedAmount": "5000",
    "claimServiceType": "IPD",
    "claimType": "HOSPITALIZATION",
    "claimSubType": "PRE-HOSPITALIZATION",
    "claimCategory": "CASHLESS",
    "entryBy": "00K02664500",
    "tokenID": "b3cd15e3-09cf-4ab7-b5b4-481b5e902d1d"
}     */

        if (distilledMemberData) {
            const payload = {
                providerName,
                insuranceID: insuranceId,
                membershipNo: distilledMemberData.memberShipNo,
                memberName: member,
                policyCode,
                employeeCode,
                // policyStartDate: dayjs(distilledMemberData.policyFrom, 'DD-MM-YYYY').format('DD/MM/YYYY'),
                // policyEndDate: dayjs(distilledMemberData.policyUpto, 'DD-MM-YYYY').format('DD/MM/YYYY'),
                // memberDateOfBirth: dayjs(distilledMemberData.memberDOB, 'DD-MM-YYYY').format('DD/MM/YYYY'),
                // memberGender: distilledMemberData.genderCode,
                admissionDate: dayjs(admissionDate).format('DD/MM/YYYY'),
                dischargeDate: dayjs(dischargeDate).format('DD/MM/YYYY'),
                // doctorName,
                diseaseName,
                estimatedAmount,
                // roomCategory: '',
                memberMobileNo: insuredPhoneMobile,
                doctorMobileNo: hospitalContact,
                claimServiceType: 'IPD',
                claimType,
                claimSubType,
                // claimCategory,
                entryBy: memberNo,
                tpaCode,
                tokenID,
            };
            postMemberClaimDetails(payload);
        }
    };

    const handleSelect = (e: FormEvent<HTMLInputElement | HTMLSelectElement>) => {
        switch ((e.target as HTMLInputElement).name) {
            case 'gender':
                setValue('gender', (e.target as HTMLInputElement).value);
                break;
            case 'member':
                setValue('member', (e.target as HTMLInputElement).value);
                fetchSelectedMemberDetails();
                break;
            case 'claimCategory':
                setValue('claimCategory', (e.target as HTMLInputElement).value);
                break;
            case 'admissionDate':
                // Validate policy period
                setValue('admissionDate', (e.target as HTMLInputElement).value);
                break;
            case 'dischargeDate':
                // Validate policy period
                setValue('dischargeDate', (e.target as HTMLInputElement).value);
                break;
            case 'serviceType':
                setValue('serviceType', (e.target as HTMLInputElement).value);
                fetchClaimTypeOptions();
                break;
            case 'claimType':
                setValue('claimType', (e.target as HTMLInputElement).value);
                fetchClaimSubTypeOptions();
                break;
            case 'claimSubType':
                setValue('claimSubType', (e.target as HTMLInputElement).value);
                break;
            default:
        }
    };

    useEffect(() => {
        if (memberClaimSavedData) {
            const result = getResultFromData(memberClaimSavedData);
            if (result) {
                cogoToast.success(result.message);
            } else {
                cogoToast.error('Something went wrong');
            }
        }
    }, [memberClaimSavedData]);

    console.log(serviceTypeOptions);
    return (
        <main className="main">
            <Card className="rounded rounded-lg mt-4" style={{ width: '60%' }}>
                <Card.Title className="rounded-top rounded-lg p-2 cardTitleClaim">
                    <h5 className="m-0  ps-3">
                        <strong>Claim Submission Form</strong>
                    </h5>
                </Card.Title>
                <Card.Body className="p-4" style={{ height: '41rem', overflowY: 'auto' }}>
                    <Form onSubmit={handleSubmit(handleFormValues)}>
                        <div className="p-3 border-bottom">
                            <div className="text-app-primary font-20">Particulars of the principal Insured</div>

                            <Row className="py-3">
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="username"
                                        placeholder="Member Name"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        style={{ fontSize: '0.8 rem !important' }}
                                        disabled
                                        {...register('username')}
                                    />
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="memberNo"
                                        placeholder="Membership Number"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        disabled
                                        {...register('memberNo')}
                                    />
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="address"
                                        placeholder="Address of Communication"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        disabled
                                        {...register('address')}
                                    />
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="contact"
                                        placeholder="Contact"
                                        type="number"
                                        className="ps-3 form-control inputSize"
                                        disabled
                                        {...register('contact')}
                                    />
                                </Form.Group>

                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="email"
                                        placeholder="E-mail"
                                        type="email"
                                        className="ps-3 form-control inputSize"
                                        disabled
                                        {...register('email')}
                                    />
                                </Form.Group>
                            </Row>
                        </div>

                        <div className="p-3 border-bottom">
                            <div className="text-app-primary font-20">Details of the Insured Member whom the claim is preferred</div>
                            <Row className="py-3 flex-column">
                                <Form.Group className="col-md-4 py-2">
                                    <Form.Select
                                        defaultValue=""
                                        // variant="primary"
                                        id="member"
                                        title="Select Member"
                                        className="inputSize"
                                        style={{ color: 'grey' }}
                                        {...register('member')}
                                        disabled={!familyMembers?.data}
                                        onChange={handleSelect}
                                    >
                                        <option disabled value="">
                                            Select Member
                                        </option>
                                        {getResultFromData(familyMembers?.data)?.map((item: FamilyMemberDataType) => (
                                            <option key={item?.memberShipNo} value={item?.memberShipNo}>
                                                {item?.dependantName} - {item?.memberShipNo}
                                            </option>
                                        ))}
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="py-2">
                                    <table className="w-100">
                                        <thead>
                                            {Object.keys(omit(getResultFromData(selectedMemberDetails), ['genderCode']) || {})?.map((val) => (
                                                <th key={val} className="tableHeader">
                                                    {CLAIM_FORM_HEADERS[getCapitallisedFromCamelCase(val)] || getCapitallisedFromCamelCase(val)}
                                                </th>
                                            ))}
                                        </thead>
                                        <tbody>
                                            {Object.values(omit(getResultFromData(selectedMemberDetails), ['genderCode']) || {})?.map((val) => (
                                                <td key={val} className="tableData">
                                                    {' '}
                                                    {val as string}
                                                </td>
                                            ))}
                                        </tbody>
                                    </table>
                                </Form.Group>
                            </Row>
                        </div>

                        <div className="p-3">
                            <div className="text-app-primary font-20">Provider & Claim Details</div>
                            <Row className="py-3">
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="providerName"
                                        placeholder="Name &amp; Full Address Of The Provider"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        {...register('providerName')}
                                    />
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="admissionDate"
                                        placeholder="Date of Admission"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        {...register('admissionDate')}
                                        onFocus={() => {
                                            if (admissionDateRef?.current?.type) {
                                                admissionDateRef.current.type = 'date';
                                            }
                                        }}
                                        onBlur={() => {
                                            if (admissionDateRef?.current?.type) {
                                                admissionDateRef.current.type = 'text';
                                            }
                                        }}
                                        ref={admissionDateRef}
                                        onChange={handleSelect}
                                    />
                                </Form.Group>

                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="hospitalName"
                                        placeholder="Discharge Date"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        {...register('dischargeDate')}
                                        onFocus={() => {
                                            if (dischargeDateRef?.current?.type) {
                                                dischargeDateRef.current.type = 'date';
                                            }
                                        }}
                                        onBlur={() => {
                                            if (dischargeDateRef?.current?.type) {
                                                dischargeDateRef.current.type = 'text';
                                            }
                                        }}
                                        ref={dischargeDateRef}
                                        onChange={handleSelect}
                                    />
                                </Form.Group>

                                <Form.Group className="col-md-4 py-2">
                                    <Form.Select
                                        defaultValue=""
                                        // variant="primary"
                                        id="serviceType"
                                        title="Select Service Type"
                                        className="inputSize"
                                        style={{ color: 'grey' }}
                                        {...register('serviceType')}
                                        disabled={!serviceTypeOptions}
                                        onChange={handleSelect}
                                    >
                                        <option disabled value="">
                                            Select Service Type
                                        </option>
                                        {getResultFromData(serviceTypeOptions)?.map((item) => (
                                            <option key={item?.service} value={item?.service}>
                                                {item?.service}
                                            </option>
                                        ))}
                                    </Form.Select>
                                </Form.Group>

                                <Form.Group className="col-md-4 py-2">
                                    <Form.Select
                                        defaultValue=""
                                        // variant="primary"
                                        id="claimType"
                                        title="Select Claim Type"
                                        className="inputSize"
                                        style={{ color: 'grey' }}
                                        {...register('claimType')}
                                        disabled={getValues('serviceType') === '' || !claimTypeOptions}
                                        onChange={handleSelect}
                                    >
                                        <option disabled value="">
                                            Select Claim Type
                                        </option>
                                        {getResultFromData(claimTypeOptions)?.map((item) => (
                                            <option key={item?.claimAndSubType} value={item?.claimAndSubType}>
                                                {item?.claimAndSubType}
                                            </option>
                                        ))}
                                    </Form.Select>
                                </Form.Group>

                                <Form.Group className="col-md-4 py-2">
                                    <Form.Select
                                        defaultValue=""
                                        // variant="primary"
                                        id="claimSubType"
                                        title="Select Claim Subtype"
                                        className="inputSize"
                                        style={{ color: 'grey' }}
                                        {...register('claimSubType')}
                                        disabled={getValues('claimType') === '' || !claimSubTypeOptions}
                                        onChange={handleSelect}
                                    >
                                        <option disabled value="">
                                            Select Claim Subtype
                                        </option>
                                        {getResultFromData(claimSubTypeOptions)?.map((item) => (
                                            <option key={item?.claimAndSubType} value={item?.claimAndSubType}>
                                                {item?.claimAndSubType}
                                            </option>
                                        ))}
                                    </Form.Select>
                                </Form.Group>

                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="hospitalName"
                                        placeholder="Claim Amount"
                                        type="number"
                                        className="ps-3 form-control inputSize"
                                        {...register('estimatedAmount')}
                                    />
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="hospitalName"
                                        placeholder="Disease Name"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        {...register('diseaseName')}
                                    />
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="hospitalName"
                                        placeholder="Doctor Name"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        {...register('doctorName')}
                                    />
                                </Form.Group>
                                {/* <Form.Group className="col-md-4 py-2">
                                    <Form.Select
                                        defaultValue=""
                                        // variant="primary"
                                        id="claimCategory"
                                        title="Select Claim Category"
                                        className="inputSize"
                                        style={{ color: 'grey' }}
                                        {...register('claimCategory')}
                                        onChange={handleSelect}
                                    >
                                        <option disabled value="">
                                            Select Claim Category
                                        </option>
                                        <option value="CAHSLESS">Cashless</option>
                                        <option value="REIMBURSEMENT">Reimbursement</option>
                                    </Form.Select>
                                </Form.Group> */}
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="hospitalContact"
                                        placeholder="Contact"
                                        type="tel"
                                        className="ps-3 form-control inputSize"
                                        {...register('hospitalContact')}
                                    />
                                </Form.Group>
                                <Form.Group className="col-md-4 py-2">
                                    <input
                                        id="tpaCode"
                                        placeholder="TPA Code"
                                        type="text"
                                        className="ps-3 form-control inputSize"
                                        {...register('tpaCode')}
                                    />
                                </Form.Group>
                            </Row>
                        </div>
                        <Row className="submit-btn justify-content-center">
                            <button type="submit" className="btn btn-primary w-">
                                Submit
                            </button>
                        </Row>
                    </Form>
                </Card.Body>
            </Card>
            <DevTool control={control} />
        </main>
    );
};

export default SubmitClaim;
