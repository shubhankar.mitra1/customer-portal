import { INPROCESS, REJECTED, PAID } from '../../constants';

export const pieProps = (series, labels) => {
    return {
        options: {
            plotOptions: {
                pie: {
                    donut: {
                        labels: {
                            show: true,
                            total: {
                                show: true,
                            },
                        },
                    },
                },
            },
            chart: {
                toolbar: {
                    show: false,
                },
            },
            labels,
            dataLabels: { enabled: true },
            legend: { show: false },
            stroke: { width: 0 },
            colors: ['#6C63FF', '#002776', '#01C76A', '#FF4AA4'],
        },
        series,
        type: 'donut',
        height: 250,
        width: '100%',
    };
};

export const claimsSummaryProps = [
    {
        id: crypto?.randomUUID?.(),

        name: 'Clinic Laboratory Name',
        location: 'Hyderabad',
        country: 'IN',
        contact: '02154368970',
        dateofservice: '22/02/2021',
        relation: 'Father',
        reason: 'Lorem Ipsum Reason',
        claimtype: 'Reimbursement',
        coveragetype: ['Medical', 'Dental', 'Pharmacy'],
        networktype: 'OUT',
        status: PAID,
        hraclaimno: '123456',
        amountbilled: 'INR 40,000',
        amountdeducted: 'INR 2,000',
        plainpaid: 'INR 38,000',
        paymentmode: 'Cheque',
        paymentdetails: 'Cheque Number',
    },
    {
        id: crypto?.randomUUID?.(),

        name: 'Clinic Laboratory Name',
        location: 'Hyderabad',
        country: 'IN',
        contact: '02154368970',
        dateofservice: '22/02/2021',
        relation: 'Father',
        reason: 'Lorem Ipsum Reason',
        claimtype: 'Reimbursement',
        coveragetype: ['Medical', 'Dental', 'Pharmacy'],
        networktype: 'OUT',
        status: REJECTED,
        hraclaimno: '123456',
        amountbilled: 'INR 40,000',
        amountdeducted: 'INR 2,000',
        plainpaid: 'INR 38,000',
        paymentmode: 'Cheque',
        paymentdetails: 'Cheque Number',
    },
    {
        id: crypto?.randomUUID?.(),

        name: 'Clinic Laboratory Name',
        location: 'Hyderabad',
        country: 'IN',
        contact: '02154368970',
        dateofservice: '22/02/2021',
        relation: 'Father',
        reason: 'Lorem Ipsum Reason',
        claimtype: 'Reimbursement',
        coveragetype: ['Medical', 'Dental', 'Pharmacy'],
        networktype: 'OUT',
        status: INPROCESS,
        hraclaimno: '123456',
        amountbilled: 'INR 40,000',
        amountdeducted: 'INR 2,000',
        plainpaid: 'INR 38,000',
        paymentmode: 'Cheque',
        paymentdetails: 'Cheque Number',
    },
];

export const PlanBalancesCardProps = [
    {
        name: 'By Claim Type',
        figure: {
            options: {
                chart: {
                    parentHeightOffset: 0,
                    toolbar: {
                        show: false,
                    },
                },
                plotOptions: {
                    bar: {
                        horizontal: true,
                        barHeight: '30%',
                        endingShape: 'rounded',
                        distributed: true,
                    },
                },
                grid: {
                    xaxis: {
                        lines: {
                            show: true,
                        },
                    },
                    yaxis: {
                        lines: { show: false },
                    },
                    padding: {
                        top: 0,
                    },
                },
                colors: ['#ff4aa4d9', '#8078ffd9'],
                dataLabels: {
                    enabled: true,
                },
                xaxis: {
                    categories: ['Reimbursement', 'Cashless'],
                    min: 0,
                    max: 100,
                    tickAmount: 4,
                    title: {
                        text: '% in count',
                        style: {
                            color: undefined,
                            cssClass: 'apexcharts-xaxis-title',
                            fontFamily: 'Helvetica, Arial, sans-serif',
                            fontSize: '12px',
                            fontWeight: 600,
                        },
                    },
                },
            },
            series: [
                {
                    data: [25, 75],
                },
            ],
            type: 'bar',
            height: '300',
            width: '100%',
        },
        table: {},
    },
    {
        name: 'By Gender',
        figure: {
            options: {
                chart: {
                    parentHeightOffset: 0,
                    toolbar: {
                        show: false,
                    },
                },
                plotOptions: {
                    bar: {
                        horizontal: true,
                        barHeight: '30%',
                        endingShape: 'rounded',
                        distributed: true,
                    },
                },
                grid: {
                    xaxis: {
                        lines: {
                            show: true,
                        },
                    },
                    yaxis: {
                        lines: {
                            show: false,
                        },
                    },
                    padding: {
                        top: 0,
                    },
                },
                colors: ['rgba(128, 120, 255, 0.85)', 'rgba(128, 120, 255, 0.85)', 'rgba(83, 29, 171, 0.85)'],
                dataLabels: {
                    enabled: true,
                },
                xaxis: {
                    categories: ['Male', 'Female', 'Other'],
                    min: 0,
                    max: 100,
                    tickAmount: 4,
                    title: {
                        text: '% in count',
                        style: {
                            fontSize: '12px',
                            fontFamily: 'Helvetica, Arial, sans-serif',
                            fontWeight: 600,
                            cssClass: 'apexcharts-xaxis-title',
                        },
                    },
                    convertedCatToNumeric: false,
                },
            },
            series: [
                {
                    data: [45, 50, 5],
                },
            ],
            type: 'bar',
            height: '300',
            width: '100%',
        },
        table: {},
    },
    {
        name: 'By Treatment',
        figure: {
            options: {
                chart: {
                    parentHeightOffset: 0,
                    toolbar: {
                        show: false,
                    },
                },
                plotOptions: {
                    bar: {
                        horizontal: true,
                        barHeight: '30%',
                        endingShape: 'rounded',
                        distributed: true,
                    },
                },
                grid: {
                    xaxis: {
                        lines: {
                            show: true,
                        },
                    },
                    yaxis: {
                        lines: {
                            show: false,
                        },
                    },
                    padding: {
                        top: 0,
                    },
                },
                colors: ['rgba(128, 120, 255, 0.85)', 'rgba(128, 120, 255, 0.85)'],
                dataLabels: {
                    enabled: true,
                },
                xaxis: {
                    categories: ['Surgical', 'Non-Surgical'],
                    min: 0,
                    max: 100,
                    tickAmount: 4,
                    title: {
                        text: '% in count',
                        style: {
                            fontSize: '12px',
                            fontFamily: 'Helvetica, Arial, sans-serif',
                            fontWeight: 600,
                            cssClass: 'apexcharts-xaxis-title',
                        },
                    },
                    convertedCatToNumeric: false,
                },
            },
            series: [
                {
                    data: [45, 50],
                },
            ],
            type: 'bar',
            height: '300',
            width: '100%',
        },
        table: {},
    },
    {
        name: 'By Age Group',
        figure: {
            options: {
                chart: {
                    parentHeightOffset: 0,
                    toolbar: {
                        show: false,
                    },
                },
                plotOptions: {
                    bar: {
                        horizontal: true,
                        barHeight: '30%',
                        endingShape: 'rounded',
                        distributed: true,
                    },
                },
                grid: {
                    xaxis: {
                        lines: {
                            show: true,
                        },
                    },
                    yaxis: {
                        lines: {
                            show: false,
                        },
                    },
                    padding: {
                        top: 0,
                    },
                },
                colors: [
                    'rgba(83, 29, 171, 0.85)',
                    'rgba(83, 29, 171, 0.85)',
                    'rgba(128, 120, 255, 0.85)',
                    'rgba(83, 29, 171, 0.85)',
                    'rgba(128, 120, 255, 0.85)',
                ],
                dataLabels: {
                    enabled: true,
                },
                xaxis: {
                    categories: ['0 - 25', '26 - 35', '36 - 50', '50 - 60', '61 +'],
                    min: 0,
                    max: 100,
                    tickAmount: 4,
                    title: {
                        text: '% in count',
                        style: {
                            fontSize: '12px',
                            fontFamily: 'Helvetica, Arial, sans-serif',
                            fontWeight: 600,
                            cssClass: 'apexcharts-xaxis-title',
                        },
                    },
                    convertedCatToNumeric: false,
                },
            },
            series: [
                {
                    data: [15, 20, 50, 5, 40],
                },
            ],
            type: 'bar',
            height: '300',
            width: '100%',
        },
        table: {},
    },
    {
        name: 'By Location',
        figure: {
            options: {
                chart: {
                    parentHeightOffset: 0,
                    toolbar: {
                        show: false,
                    },
                },
                plotOptions: {
                    bar: {
                        horizontal: true,
                        barHeight: '30%',
                        endingShape: 'rounded',
                        distributed: true,
                    },
                },
                grid: {
                    xaxis: {
                        lines: {
                            show: true,
                        },
                    },
                    yaxis: {
                        lines: {
                            show: false,
                        },
                    },
                    padding: {
                        top: 0,
                    },
                },
                colors: [
                    'rgba(83, 29, 171, 0.85)',
                    'rgba(83, 29, 171, 0.85)',
                    'rgba(128, 120, 255, 0.85)',
                    'rgba(83, 29, 171, 0.85)',
                    'rgba(128, 120, 255, 0.85)',
                ],
                dataLabels: {
                    enabled: true,
                },
                xaxis: {
                    categories: ['Lucknow', 'Kanpur', 'Prayagraj', 'Agra', 'Bareilly'],
                    min: 0,
                    max: 100,
                    tickAmount: 4,
                    title: {
                        text: '% in count',
                        style: {
                            fontSize: '12px',
                            fontFamily: 'Helvetica, Arial, sans-serif',
                            fontWeight: 600,
                            cssClass: 'apexcharts-xaxis-title',
                        },
                    },
                    convertedCatToNumeric: false,
                },
            },
            series: [
                {
                    data: [15, 20, 50, 5, 40],
                },
            ],
            type: 'bar',
            height: '300',
            width: '100%',
        },
        table: {},
    },
    {
        name: 'By Duration of Stay',
        figure: {
            options: {
                chart: {
                    parentHeightOffset: 0,
                    toolbar: {
                        show: false,
                    },
                },
                plotOptions: {
                    bar: {
                        horizontal: true,
                        barHeight: '30%',
                        endingShape: 'rounded',
                        distributed: true,
                    },
                },
                grid: {
                    xaxis: {
                        lines: {
                            show: true,
                        },
                    },
                    yaxis: {
                        lines: {
                            show: false,
                        },
                    },
                    padding: {
                        top: 0,
                    },
                },
                colors: [
                    'rgba(83, 29, 171, 0.85)',
                    'rgba(83, 29, 171, 0.85)',
                    'rgba(128, 120, 255, 0.85)',
                    'rgba(83, 29, 171, 0.85)',
                    'rgba(128, 120, 255, 0.85)',
                ],
                dataLabels: {
                    enabled: true,
                },
                xaxis: {
                    categories: ['0 - 3 days', '4 - 6 days', '1 - 2 weeks', '3 - 4 weeks', '1 month +'],
                    min: 0,
                    max: 100,
                    tickAmount: 4,
                    title: {
                        text: '% in count',
                        style: {
                            fontSize: '12px',
                            fontFamily: 'Helvetica, Arial, sans-serif',
                            fontWeight: 600,
                            cssClass: 'apexcharts-xaxis-title',
                        },
                    },
                    convertedCatToNumeric: false,
                },
            },
            series: [
                {
                    data: [15, 20, 50, 5, 40],
                },
            ],
            type: 'bar',
            height: '300',
            width: '100%',
        },
        table: {},
    },
];

export const DocumentsCardProps = [
    {
        id: crypto?.randomUUID?.(),
        name: 'Health Insurance Statement',
        companyname: 'Company Name',
        issuedate: '12 Jun 2021 8:30 pm',
    },
    {
        id: crypto?.randomUUID?.(),

        name: 'Health Insurance Statement',
        companyname: 'Company Name',
        issuedate: '12 Jun 2021 8:30 pm',
    },
    {
        id: crypto?.randomUUID?.(),

        name: 'Health Insurance Statement',
        companyname: 'Company Name',
        issuedate: '12 Jun 2021 8:30 pm',
    },
    {
        id: crypto?.randomUUID?.(),

        name: 'Health Insurance Statement',
        companyname: 'Company Name',
        issuedate: '12 Jun 2021 8:30 pm',
    },
    {
        id: crypto?.randomUUID?.(),

        name: 'Health Insurance Statement',
        companyname: 'Company Name',
        issuedate: '12 Jun 2021 8:30 pm',
    },
    {
        id: crypto?.randomUUID?.(),

        name: 'Health Insurance Statement',
        companyname: 'Company Name',
        issuedate: '12 Jun 2021 8:30 pm',
    },
    {
        id: crypto?.randomUUID?.(),

        name: 'Health Insurance Statement',
        companyname: 'Company Name',
        issuedate: '12 Jun 2021 8:30 pm',
    },
    {
        id: crypto?.randomUUID?.(),

        name: 'Health Insurance Statement',
        companyname: 'Company Name',
        issuedate: '12 Jun 2021 8:30 pm',
    },
    {
        id: crypto?.randomUUID?.(),

        name: 'Health Insurance Statement',
        companyname: 'Company Name',
        issuedate: '12 Jun 2021 8:30 pm',
    },
    {
        id: crypto?.randomUUID?.(),

        name: 'Health Insurance Statement',
        companyname: 'Company Name',
        issuedate: '12 Jun 2021 8:30 pm',
    },
];
