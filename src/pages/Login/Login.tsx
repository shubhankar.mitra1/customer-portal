/* eslint-disable no-void */
/* eslint-disable no-nested-ternary */
import { useState, useRef, useEffect } from 'react';
import { Button, Card, Form, InputGroup } from 'react-bootstrap';
import { EoxegenLogo } from '@assets/index';
import { useAuth } from '@hooks/useAuth';
import './styles.css';
import { LoginAPI, verifyWithOTP } from '@api/Login/login.api';
import { useQuery } from '@tanstack/react-query';
import { Navigate } from 'react-router-dom';
import Spinner from '@components/Spinner/Spinner';
import Base64 from 'crypto-js/enc-base64';
import Utf8 from 'crypto-js/enc-utf8';
import AES from 'crypto-js/aes';
import CryptoJSCore from 'crypto-js/core';
import Pkcs7 from 'crypto-js/pad-pkcs7';
import cogoToast from 'cogo-toast';
import { getResultFromData } from '../../utils';

// import _ from '../../lodash.config';

// eslint-disable-next-line @typescript-eslint/no-var-requires
type LoginFunc = {
    login?: (args: object) => void;
    user?: object | null;
};

const Login = () => {
    const [openLicense, setOpenLicense] = useState(false);
    const [OTP, setOTP] = useState<number | undefined>(undefined);
    const [isLoggedIn, setIsLoggedIn] = useState<boolean | undefined>(undefined);
    const OTPref = useRef<HTMLButtonElement>(null);
    const modalRef = useRef<HTMLDialogElement>(null);
    const emailRef = useRef<HTMLInputElement>(null);
    const passwordRef = useRef<HTMLInputElement>(null);

    const { login, user }: LoginFunc = useAuth();

    const { refetch, isFetching } = useQuery(['login'], handleSubmit, {
        refetchOnWindowFocus: false,
        enabled: false,
    });

    if (user) {
        return <Navigate to="/home" />;
    }

    async function handleSubmit() {
        const details = { email: emailRef.current?.value, password: passwordRef.current?.value };
        if (!details.email || !details.password) {
            setIsLoggedIn(undefined);

            return;
        }

        const payLoad = {
            searchvalue: details.email,
            password: encryptPassword(details.password),
            // sendType: 'S',
        };

        const validateFirstStep = await LoginAPI(payLoad);

        if (validateFirstStep.ok) {
            cogoToast.success(getResultFromData(validateFirstStep).message).then(() => modalRef.current?.showModal());
            return true;
        }
        return false;
    }

    /**
     *
     * @param {*} password => User input password
     * @returns Hashed password
     */
    const encryptPassword = (password: string) => {
        const publickey = Base64.parse(import.meta.env.VITE_hd_publicKey);
        const algoKey = Base64.parse(import.meta.env.VITE_hd_algoKey);
        const utfStringified = Utf8.parse(password).toString();
        const aesEncrypted = AES.encrypt(utfStringified, publickey, {
            mode: CryptoJSCore.mode.CBC,
            padding: Pkcs7,
            iv: algoKey,
        });
        return aesEncrypted.ciphertext.toString(Base64);
    };

    /**
     *
     * @performs Validates the OTP and logs in the user
     */
    const handleOTP = async () => {
        console.log('reaching');
        if (String(OTP).length !== 6) {
            cogoToast.error('must be 6 digits');
            return;
        }

        const payLoad = {
            searchvalue: emailRef.current?.value,
            OTP,
        };

        // OTPref.current.disabled = true;
        const validatewithotp = await verifyWithOTP(payLoad);

        if (validatewithotp.ok) {
            if (login) {
                login(getResultFromData(validatewithotp));
                // OTPref.current.disabled = false;
                modalRef.current?.close();
            }
        } else {
            cogoToast.error('something went wrong');
            // OTPref.current.disabled = false;
        }
    };

    const rememberMe = (e) => {
        console.log(e.target.checked);
        if (e.target.checked) {
            console.log(emailRef.current?.value, passwordRef.current?.value);
            if (emailRef?.current?.value !== '' && passwordRef?.current?.value !== '') {
                localStorage.setItem('creds', JSON.stringify({ userid: emailRef?.current?.value, password: passwordRef?.current?.value }));
            }
        } else {
            localStorage.removeItem('creds');
        }
    };

    return (
        <main className="login--main">
            <section className="logo">
                <EoxegenLogo />
                <span className="text-white font-25 ps-3">eOxegen</span>
            </section>
            <section className="login--card">
                <Card className="border-0 rounded rounded-lg">
                    <Card.Title className="text-center">
                        <strong className="sign-in-header">Sign in to your account</strong>
                    </Card.Title>
                    <Card.Body>
                        {!openLicense ? (
                            <Form className="mt-4">
                                <Form.Label>USERID / MOBILE / EMAIL</Form.Label>
                                <Form.Control
                                    ref={emailRef}
                                    type="text"
                                    className="rounded rounded-lg"
                                    onKeyDown={(e) => {
                                        if (e.key === 'Enter') {
                                            refetch();
                                        }
                                    }}
                                    required
                                />
                                <Form.Label className="mt-4">PASSWORD</Form.Label>
                                <Form.Control
                                    ref={passwordRef}
                                    type="password"
                                    className="rounded rounded-lg"
                                    onKeyDown={(e) => {
                                        if (e.key === 'Enter') {
                                            refetch();
                                        }
                                    }}
                                    required
                                />
                                <section className="options">
                                    <Form.Label className="mt-4">
                                        <InputGroup>
                                            <InputGroup.Checkbox onChange={rememberMe} />
                                            Remember Me
                                        </InputGroup>
                                    </Form.Label>
                                    <a href="/" onClick={(e) => e.preventDefault()}>
                                        Forgot Password
                                    </a>
                                </section>
                                <Button disabled={isLoggedIn === false} className="mt-2 rounded rounded-lg w-100 btn-bg" onClick={() => refetch()}>
                                    {isFetching ? <Spinner suspense={false} /> : 'Sign In'}
                                </Button>

                                <hr />
                            </Form>
                        ) : (
                            <section className="customerAgreement">
                                <h4>Customer Agreement</h4>
                                <p>Last updated on 20th June,2020</p>
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
                                magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
                                gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing
                                elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos
                                et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor
                                sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
                                dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita
                                kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
                                sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                                At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem
                                ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
                                ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
                                Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
                                consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
                                voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus
                                est Lorem ipsum dolor sit amet.
                            </section>
                        )}
                        <section className="options">
                            <a
                                href="/"
                                onClick={(e) => {
                                    e.preventDefault();
                                    setOpenLicense((sol) => !sol);
                                }}
                            >
                                Read customer agreement
                            </a>
                            <div
                                className={`${openLicense ? 'arrow-up' : 'arrow-right'}`}
                                onClick={() => setOpenLicense((sol) => !sol)}
                                onKeyDown={() => {}}
                                role="button"
                                tabIndex={0}
                            >
                                {' '}
                            </div>
                        </section>
                        <dialog ref={modalRef} className="dialog__modal">
                            <section>
                                <label>
                                    Enter OTP
                                    <input
                                        id="otp"
                                        name="otp"
                                        type="number"
                                        value={OTP}
                                        className="input"
                                        onChange={(e) => setOTP(e.target.valueAsNumber)}
                                        onKeyDown={(e) => (e.key === 'Enter' ? handleOTP() : void 0)}
                                    />
                                </label>
                                <button
                                    type="button"
                                    onClick={handleOTP}
                                    // eslint-disable-next-line no-void
                                    ref={OTPref}
                                    data-otp="OTP"
                                    className="btn btn-primary"
                                    tabIndex={0}
                                >
                                    Submit
                                </button>
                            </section>
                        </dialog>
                    </Card.Body>
                </Card>
            </section>
        </main>
    );
};

export default Login;
