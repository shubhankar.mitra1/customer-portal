import PolicyDetailsCards from './PolicyDetailsCards';

const PolicyDetails = ({ data = [] }) => {
    return (
        <div className="">
            <h1 style={{ fontSize: '1.5rem', color: '#002776', fontWeight: '700' }}>Members Covered In Policy</h1>
            <div className="policyDetailsCard-container">
                {data?.map((item) => (
                    <PolicyDetailsCards key={item.memberNo} data={item} />
                ))}
            </div>
        </div>
    );
};

export default PolicyDetails;
