import CustomisedTable from '@components/CustomisedTable/CustomisedTable';
import { getCapitallisedFromCamelCase } from '../../utils';
import './styles.css';
import _ from '../../lodash.config';

type PolicyCardCollapsibleData = { [key: string]: string | number | []; actualMemberShipNo: string; memberMainBenefitMT: [] };

const getTableHeaders = (unfilteredData: any[], omit: string[]) => {
    return Object.keys(_.omit(unfilteredData[0], omit)).map(getCapitallisedFromCamelCase);
};

const getTableData = (unfilteredData: any[], omit: string[]) => {
    return unfilteredData.map((data) => _.omit(data, omit)) as [];
};

export const PolicyCardCollapsible = ({ data }: { data: [] }) => {
    console.log(data);
    return (
        <main>
            {data?.map((details: PolicyCardCollapsibleData) => {
                return (
                    <section className="d-flex justify-content-between card--outer" key={details.actualMemberShipNo}>
                        <div className="policy--details rounded">
                            {Object.entries(
                                _.omit(details, [
                                    'memberMainBenefitMT',
                                    'actualMemberShipNo',
                                    'bloodGroup',
                                    'corporateID',
                                    'countryCode',
                                    'empCode',
                                    'insuranceID',
                                    'memberAge',
                                    'memberRelationCode',
                                    'photoPath',
                                    'signaturePath',
                                    'tpaID',
                                    'productCurrency',
                                    'insuranceName',
                                    'policyStartDate',
                                    'policyExpireDate',
                                ])
                            ).map(([name, value]) => (
                                <section key={name}>
                                    <p className="font-12 opacity-50 text-white">
                                        {name !== 'schemeName' ? getCapitallisedFromCamelCase(name) : 'Category'}
                                    </p>
                                    <p className="text-white font-14"> {value}</p>
                                </section>
                            ))}
                        </div>
                        <CustomisedTable
                            tableHeaders={getTableHeaders(details.memberMainBenefitMT, [
                                'benefitBalanceStatusColorCode',
                                'insurer',
                                'watingPeriod',
                                'product',
                                'clauseCodeDependent',
                                'coPayment',
                                'coShare',
                                'catagory',
                            ])}
                            tableData={getTableData(details.memberMainBenefitMT, [
                                'benefitBalanceStatusColorCode',
                                'insurer',
                                'watingPeriod',
                                'product',
                                'clauseCodeDependent',
                                'coPayment',
                                'coShare',
                                'catagory',
                            ])}
                        />
                    </section>
                );
            })}
        </main>
    );
};
