// @ts-ignore

import { useState } from 'react';
import { Button, Card } from 'react-bootstrap';
import { familyFloaterPNG } from '@assets/index';
import { PolicyCardType } from '../../Types/types';
import './styles.css';

export const PolicyCard = ({
    policynumber,
    scheme,
    amount,
    renewdate,
    policystartdate,
    policyterm,
    membercount,
    handleView,
    active,
}: PolicyCardType) => {
    return (
        <Card
            className="rounded rounded-lg border-0"
            style={{ width: 'fit-content', display: active === false ? 'flex' : active === policynumber ? 'flex' : 'none' }}
        >
            <Card className="upper--card pt-3 pb-3 ps-4 pe-4">
                <Card.Title className="text-white font-18 title">{policynumber}</Card.Title>
                <Card.Subtitle className="text-white font-14 opacity-50">{scheme === 'I' ? 'Individual' : 'Family Floater'}</Card.Subtitle>
                <Card.Body className="p-5 pb-0">
                    <img src={familyFloaterPNG} width={300} height={150} alt="family floater" style={{ marginBottom: '1rem' }} />
                    <h2 className="text-white text-center">{amount}</h2>
                </Card.Body>
            </Card>
            <Card className="border-0">
                <Card.Body className="lower--card">
                    <section>
                        <p className="font-12 opacity-50 mb-0">Renewal on</p>
                        <p className="font-13 det">{renewdate}</p>
                    </section>
                    <section style={{ justifySelf: 'flex-end' }}>
                        <p className="font-12 opacity-50 mb-0">Policy Start Date</p>
                        <p className="font-13 det">{policystartdate}</p>
                    </section>
                    <section>
                        <p className="font-12 opacity-50 mb-0">Poicy Term</p>
                        <p className="font-13 det">{policyterm}</p>
                    </section>
                    <section style={{ justifySelf: 'flex-end' }}>
                        <p className="font-12 opacity-50 mb-0">Family Members</p>
                        <p className="font-13 det">{membercount}</p>
                    </section>
                </Card.Body>
                <Button className="edit--btn" onClick={() => handleView(policynumber)}>
                    View Profile
                </Button>
            </Card>
        </Card>
    );
};
