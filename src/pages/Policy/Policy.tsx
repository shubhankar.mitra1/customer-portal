import { useState } from 'react';
import SubLayout from '@layout/SubLayout';
import { useQueries, useQuery } from '@tanstack/react-query';
import { getPolicyDetails, getMemberCoveredPolicy } from '@api/Policy/policy.api';
import Spinner from '@components/Spinner/Spinner';
// import { PolicyCardCollapsible } from './PolicyCardCollapsible';
import { getResultFromData } from '../../utils';
import { getPayLoad } from '../../Payload';
import './styles.css';
import { PolicyCard } from './PolicyCard';
import PolicyDetails from './PolicyDetails';
import { get } from 'lodash';

const Policy = () => {
    const [showView, setShowView] = useState(false);
    const [active, setActive] = useState(false);

    const [{ data, isLoading }, { data: memberCoveredData }] = useQueries({
        queries: [
            {
                queryKey: ['policy'],
                queryFn: () => getPolicyDetails(getPayLoad('policy', undefined)),
                select: (apiData) => {
                    const result = getResultFromData(apiData);
                    if (result) {
                        return result.map((item) => {
                            return {
                                policynumber: item.policyNo,
                                scheme: item.policyType,
                                amount: item.policyValue,
                                renewdate: item.policyEndDate,
                                policystartdate: item.policyStartDate,
                                policyterm: `${item.policyStartDate} - ${item.policyEndDate}`,
                                membercount: result.length,
                            };
                        });
                    }
                    return null;
                },
            },
            {
                queryKey: ['membercoveredpolicy'],
                queryFn: () => getMemberCoveredPolicy(getPayLoad('membercoveredpolicy', undefined)),
                select: (apiData) => {
                    const result = getResultFromData(apiData);
                    if (result) {
                        return result;
                    }

                    return null;
                },
            },
        ],
    });

    if (isLoading) {
        return (
            <div className="spinner">
                <Spinner suspense={false} width="30px" height="30px" />
            </div>
        );
    }

    const handleView = (policyNumber) => {
        setActive(policyNumber);
        //call the api
        setShowView(true);
    };

    return (
        <SubLayout>
            <main className="d-flex justify-content-center policy--layout">
                {!isLoading && data.map((item) => <PolicyCard key={item.policynumber} {...item} handleView={handleView} active={active} />)}
                {showView ? <PolicyDetails data={memberCoveredData} /> : null}
            </main>
        </SubLayout>
    );
};

export default Policy;
