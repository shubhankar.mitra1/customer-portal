import userProfile from '@assets/user-profile.png';

const PolicyDetailsCards = ({ data }) => {
    return (
        <div>
            <div className="card-details">
                <div>
                    <img src={userProfile} alt="profile-img" className="profile-img" />
                    <div>
                        <h4>{data.memberName}</h4>
                        <span style={{ fontSize: '10px', color: 'gray' }}>Relation</span>
                        <span style={{ fontSize: '15px', marginLeft: '7px' }}>{data.relation}</span>
                    </div>
                </div>
                <div className="user-details">
                    <div>
                        <p>Member ID</p>
                        <p className="font-12">{data.memberNo}</p>
                    </div>
                    <div>
                        <p>D.O.B</p>
                        <p className="font-12">{data.dateOfBirth}</p>
                    </div>
                    <div>
                        <p>Contact</p>
                        <p className="font-12">{data.contactNo}</p>
                    </div>
                    <div>
                        <p>Email</p>
                        <p className="font-12">{data.email}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PolicyDetailsCards;
