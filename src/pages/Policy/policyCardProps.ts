export const PolicyCardProps = [
    {
        policynumber: '#2990901740661',
        scheme: 'Family Floater Plan',
        amount: 'INR 10,00,000',
        renewdate: '20 Jan 2022',
        policystartdate: '21 Jan 2021',
        policyterm: '5-62 Years',
        membercount: 6,
    },
    {
        policynumber: '#2990901740662',
        scheme: 'Family Floater Plan',
        amount: 'INR 10,00,000',
        renewdate: '20 Jan 2022',
        policystartdate: '21 Jan 2021',
        policyterm: '5-62 Years',
        membercount: 6,
    },
    {
        policynumber: '#2990901740663',
        scheme: 'Family Floater Plan',
        amount: 'INR 10,00,000',
        renewdate: '20 Jan 2022',
        policystartdate: '21 Jan 2021',
        policyterm: '5-62 Years',
        membercount: 6,
    },
];
