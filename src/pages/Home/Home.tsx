/* eslint-disable no-restricted-syntax */
/* eslint-disable no-nested-ternary */
import SubLayout from '@layout/SubLayout';
import { Card, Form, Row } from 'react-bootstrap';
import ClaimsCard from '@components/ClaimsCard/ClaimsCard';
import { UserCard } from '@assets/index';
import { motion } from 'framer-motion';
import { useForm } from 'react-hook-form';
import { Bar } from '@components/Charts/chart';
import { useQueries, useQuery } from '@tanstack/react-query';
import Spinner from '@components/Spinner/Spinner';
import { getGraphRelation, getAmountData, getGraphDataFromApi, getRecentClaims } from '@api/Home/home.api';
import { INPROCESS, REJECTED, PAID, DURATION } from '../../constants';
import './styles.css';
import { getNormalizedNumberFromString, getResultFromData } from '../../utils';
import { getPayLoad } from '../../Payload';
import { DevTool } from '@hookform/devtools';
import { useEffect, useState } from 'react';

const Home = () => {
    // const { actualMemberNo, tokenID } = JSON.parse(sessionStorage.getItem('user') || '{}');
    const [graphData, setGraphData] = useState([]);
    const [categories, setCategories] = useState([]);
    const [values, setValues] = useState([]);

    const [recentReimbursement, recentPreauth, graphRelationOptions, graphTotalAmount, graphPrincipalData] = useQueries({
        queries: [
            {
                queryKey: ['recentReimbursement'],
                queryFn: () => getRecentClaims(getPayLoad('recentClaims', undefined)),
                refetchOnWindowFocus: false,
            },
            {
                queryKey: ['recentPreauth'],
                queryFn: () => getRecentClaims(getPayLoad('recentPreauth', undefined)),
                refetchOnWindowFocus: false,
            },
            {
                queryKey: ['memberclaimmadeonpoliciesrelation'],
                queryFn: () => getGraphRelation(getPayLoad('memberclaimmadeonpoliciesrelation', undefined)),
                refetchOnWindowFocus: false,
            },
            {
                queryKey: ['memberclaimmadepoliciestotalamount'],
                queryFn: () =>
                    getAmountData(
                        getPayLoad('memberclaimmadepoliciestotalamount', { relation: getValues('member'), dateType: getValues('duration') })
                    ),
                refetchOnWindowFocus: false,
                cacheTime: 0,
                // enabled: false,
            },
            {
                queryKey: ['memberclaimmadepoliciestotalamountgr'],
                queryFn: () =>
                    getGraphDataFromApi(
                        getPayLoad('memberclaimmadepoliciestotalamountgr', { relation: getValues('member'), dateType: getValues('duration') })
                    ),
                refetchOnWindowFocus: false,

                onSuccess(data) {
                    const result = getResultFromData(data);
                    if (result) {
                        const months = [...new Set(result.map((item) => item.month))];
                        setCategories(months);

                        setValues(
                            months.map((month) => {
                                let sum = 0;
                                result.map((object) => {
                                    if (object.month === month) {
                                        sum += getNormalizedNumberFromString(object.totalclaimAmount);
                                    }
                                });
                                return sum;
                            })
                        );
                    } else {
                        setCategories([]);
                        setValues([]);
                    }
                },
            },
        ],
    });

    const { register, setValue, getValues, control, watch } = useForm({
        defaultValues: {
            member: '',
            duration: '',
        },
        // resolver: zodResolver(ClaimIntimationSchema),
    });

    const getGraphData = () => {
        const selectedMember = getValues('member');
        const duration = getValues('duration');

        if (selectedMember && duration) {
            graphTotalAmount.refetch();
            graphPrincipalData.refetch();
        }
    };

    return (
        <SubLayout>
            <main id="home-main">
                <section className="section--one">
                    <section className="section--claims">
                        <motion.div
                            whileHover={{
                                scale: 1.004,
                                transition: { duration: 0.5 },
                            }}
                            whileTap={{ scale: 0.998 }}
                        >
                            <Card className="rounded rounded-lg p-3 h-100">
                                <Card.Title className="cardTitle">
                                    <strong>Recent Reimbursement</strong>
                                </Card.Title>
                                <Card.Body className="pb-0 gap">
                                    <div className="recent--claims">
                                        {recentReimbursement?.isLoading ? (
                                            <Spinner suspense={false} />
                                        ) : recentReimbursement?.data ? (
                                            <div className="cardDisplay">
                                                {getResultFromData(recentReimbursement?.data)?.map((claimdata) => (
                                                    <ClaimsCard
                                                        key={claimdata.patientName}
                                                        name={claimdata.patientName}
                                                        date={claimdata.dateOfAdmission}
                                                        context={claimdata.claimCategory}
                                                        status={
                                                            claimdata.claimStatus === 'UNDER PROCESS'
                                                                ? INPROCESS
                                                                : claimdata.claimStatus === 'INTIMATION CLOSED'
                                                                ? REJECTED
                                                                : PAID
                                                        }
                                                        amount={claimdata.claimAmount}
                                                        displayPic={UserCard}
                                                    />
                                                ))}
                                            </div>
                                        ) : recentReimbursement?.error ? (
                                            'Data not found'
                                        ) : null}
                                    </div>
                                </Card.Body>
                            </Card>
                        </motion.div>
                    </section>
                    <section className="section--policies">
                        <motion.div>
                            <Card className="rounded rounded-lg p-3">
                                <Card.Title className="cardTitle">
                                    <strong>Claims made on Policies</strong>
                                </Card.Title>
                                <Card.Body className="pb-0 gap">
                                    <Bar
                                        options={{
                                            chart: {
                                                id: 'basic-bar',
                                            },

                                            xaxis: {
                                                categories,
                                            },
                                            plotOptions: { bar: { columnWidth: '45%' } },
                                        }}
                                        series={[
                                            {
                                                name: 'sales',
                                                data: values,
                                            },
                                        ]}
                                        width="750"
                                        height="370"
                                    />
                                    <Row className="py-3 rounded graph-config">
                                        <Form className="d-flex align-items-center" style={{ columnGap: '1rem' }}>
                                            <Form.Group className="col-md-4 py-2">
                                                <Form.Select
                                                    defaultValue=""
                                                    // variant="primary"
                                                    id="member"
                                                    title="Select Member"
                                                    className="inputSize"
                                                    style={{ color: 'grey' }}
                                                    {...register('member')}
                                                    onChange={(e) => {
                                                        setValue('member', e.target.value.toUpperCase());
                                                        setValue('duration', '');
                                                    }}
                                                >
                                                    <option disabled value="">
                                                        Select Member
                                                    </option>
                                                    {getResultFromData(graphRelationOptions?.data)?.map((item) => (
                                                        <option key={item?.relationWithProposer} value={item?.relationWithProposer.toUpperCase()}>
                                                            {item?.relationWithProposer}
                                                        </option>
                                                    ))}
                                                </Form.Select>
                                            </Form.Group>
                                            <Form.Group className="col-md-4 py-2">
                                                <Form.Select
                                                    defaultValue=""
                                                    // variant="primary"
                                                    id="duration"
                                                    title="Select Duration"
                                                    className="inputSize"
                                                    style={{ color: 'grey' }}
                                                    {...register('duration')}
                                                    onChange={(e) => {
                                                        setValue('duration', e.target.value);

                                                        getGraphData();
                                                    }}
                                                    disabled={watch('member') === ''}
                                                >
                                                    <option disabled value="">
                                                        Select Duration
                                                    </option>
                                                    {Object.entries(DURATION).map(([desc, value]) => (
                                                        <option key={value} value={value}>
                                                            {desc}
                                                        </option>
                                                    ))}
                                                </Form.Select>
                                            </Form.Group>
                                            <section className="divider" />
                                            <h4 style={{ color: 'white' }}>INR {getResultFromData(graphTotalAmount?.data)?.totalclaimAmount}</h4>
                                            <DevTool control={control} />
                                        </Form>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </motion.div>
                    </section>
                    <section className="nedfact">
                        <motion.div
                            whileHover={{
                                scale: 1.004,
                                transition: { duration: 0.5 },
                            }}
                            whileTap={{ scale: 0.998 }}
                        >
                            <Card className="rounded rounded-lg p-3 h-100">
                                <Card.Title className="cardTitle">
                                    <strong>Recent Preauth</strong>
                                </Card.Title>
                                <Card.Body className="pb-0 gap">
                                    <div className="recent--claims">
                                        {recentPreauth?.isLoading ? (
                                            <Spinner suspense={false} />
                                        ) : recentPreauth?.data ? (
                                            <div className="cardDisplay">
                                                {getResultFromData(recentPreauth?.data)?.map((claimdata) => (
                                                    <ClaimsCard
                                                        key={claimdata.patientName}
                                                        name={claimdata.patientName}
                                                        date={claimdata.dateOfAdmission}
                                                        context={claimdata.claimCategory}
                                                        status={
                                                            claimdata.claimStatus === 'UNDER PROCESS'
                                                                ? INPROCESS
                                                                : claimdata.claimStatus === 'INTIMATION CLOSED'
                                                                ? REJECTED
                                                                : PAID
                                                        }
                                                        amount={claimdata.claimAmount}
                                                        displayPic={UserCard}
                                                    />
                                                ))}
                                            </div>
                                        ) : recentPreauth?.error ? (
                                            'Data not found'
                                        ) : null}
                                    </div>
                                </Card.Body>
                            </Card>
                        </motion.div>
                    </section>
                </section>
                <section className="section--one">
                    <section className="endorsements">
                        <motion.div
                            whileHover={{
                                scale: 1.004,
                                transition: { duration: 0.5 },
                            }}
                            whileTap={{ scale: 0.998 }}
                        >
                            <Card className="rounded rounded-lg p-3">
                                <Card.Title className="cardTitle">
                                    <strong>Endorsements</strong>
                                </Card.Title>
                                <Card.Body className="pb-0 gap">
                                    <ClaimsCard
                                        name="Shilpa Saxena"
                                        date="10/10/1997"
                                        context="Reimbursement"
                                        status={INPROCESS}
                                        amount="50000"
                                        displayPic={UserCard}
                                    />
                                </Card.Body>
                            </Card>
                        </motion.div>
                    </section>
                    <section className="fitnesstrack">
                        <motion.div
                            whileHover={{
                                scale: 1.004,
                                transition: { duration: 0.5 },
                            }}
                            whileTap={{ scale: 0.998 }}
                        >
                            <Card className="rounded rounded-lg p-3 nedfactCard">
                                <Card.Title className="cardTitle">
                                    <strong>&nbsp;&nbsp;&nbsp;Track your daily fitness</strong>
                                </Card.Title>
                                <Card.Body className="pb-0">
                                    {' '}
                                    <div className="fitnessTrack">
                                        <Card>
                                            <Card.Body className="p-0 rounded rounded-lg">
                                                <section className="top--sec">top</section>
                                                <section className="bottom--sec bg-app-primary d-flex justify-content-around text-white p-3 pt-2 card-footer">
                                                    <section>
                                                        <strong>Steps</strong>
                                                    </section>
                                                    <section>
                                                        <strong>9000</strong>
                                                        <div className="text-muted font-12">3000 to go</div>
                                                    </section>
                                                </section>
                                            </Card.Body>
                                        </Card>
                                        <Card>
                                            <Card.Body className="p-0 rounded rounded-lg">
                                                <section className="top--sec">top</section>
                                                <section className="bottom--sec bg-app-primary d-flex justify-content-around text-white p-3 pt-2 card-footer">
                                                    <section>
                                                        <strong>Steps</strong>
                                                    </section>
                                                    <section>
                                                        <strong>9000</strong>
                                                        <div className="text-muted font-12">3000 to go</div>
                                                    </section>
                                                </section>
                                            </Card.Body>
                                        </Card>
                                        <Card>
                                            <Card.Body className="p-0 rounded rounded-lg">
                                                <section className="top--sec">top</section>
                                                <section className="bottom--sec bg-app-primary d-flex justify-content-around text-white p-3 pt-2 card-footer">
                                                    <section>
                                                        <strong>Steps</strong>
                                                    </section>
                                                    <section>
                                                        <strong>9000</strong>
                                                        <div className="text-muted font-12">3000 to go</div>
                                                    </section>
                                                </section>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                </Card.Body>
                            </Card>
                        </motion.div>
                    </section>
                    <section className="gamezone">
                        <motion.div
                            whileHover={{
                                scale: 1.004,
                                transition: { duration: 0.5 },
                            }}
                            whileTap={{ scale: 0.998 }}
                        >
                            <Card className="rounded rounded-lg p-3 nedfactCard">
                                <Card.Title className="cardTitle">
                                    <strong>GameZone</strong>
                                </Card.Title>
                                <Card.Body className="pb-0 gap">
                                    <ClaimsCard
                                        name="Shilpa Saxena"
                                        date="10/10/1997"
                                        context="Reimbursement"
                                        status={INPROCESS}
                                        amount="50000"
                                        displayPic={UserCard}
                                    />
                                </Card.Body>
                            </Card>
                        </motion.div>
                    </section>
                </section>
            </main>
        </SubLayout>
    );
};

export default Home;
