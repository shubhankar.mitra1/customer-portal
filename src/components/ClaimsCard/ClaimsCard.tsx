import { Card } from 'react-bootstrap';
import { ClaimsCardType } from '../../Types/types';
import './styles.css';

const ClaimsCard = ({ name, date, context, status, amount, displayPic }: ClaimsCardType) => {
    return (
        <Card className="rounded rounded-lg ">
            <section className="section--one--card">
                <img src={displayPic} width={48} height={48} alt="displaypic" />
                <div className="cardDetails">
                    <p className="font-18">{name}</p>
                    <p className="font-12">{date}</p>
                    <p className="font-12">{context}</p>
                </div>
            </section>
            <section className="section--two--card rounded rounded-lg" data-color={status}>
                <div>
                    <p className="font-12">Amount created</p>
                    <p className="font-18">{amount}</p>
                </div>
                <div className="status font-12">{status}</div>
            </section>
        </Card>
    );
};

export default ClaimsCard;
