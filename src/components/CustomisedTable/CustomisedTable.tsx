import { Table } from 'react-bootstrap';
import { AnimatePresence, motion } from 'framer-motion';
import './styles.css';

const CustomisedTable = ({ tableHeaders, tableData }: { tableHeaders: string[]; tableData: [] }) => {
    return (
        <AnimatePresence>
            <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }} style={{ width: '100%' }}>
                <Table striped bordered hover responsive>
                    <thead>
                        <tr>
                            {tableHeaders?.map((th) => (
                                <th className="font-14" key={th}>
                                    {th}
                                </th>
                            ))}
                        </tr>
                    </thead>
                    <tbody>
                        {tableData?.map((td) => {
                            return (
                                <tr key={Math.random()}>
                                    {Object.values(td).map((_tableData) => (
                                        <td className="font-14" key={Math.random()}>
                                            {_tableData as string}
                                        </td>
                                    ))}
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
            </motion.div>
        </AnimatePresence>
    );
};

export default CustomisedTable;
