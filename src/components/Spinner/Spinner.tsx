import './styles.css';

type PropType = {
    suspense: boolean;
    width?: string;
    height?: string;
};

const Spinner = ({ suspense = false, width = '10px', height = '10px' }: PropType) => {
    return (
        <div className="loader" style={{ visibility: `${suspense ? 'hidden' : 'visible'}`, width, height }}>
            {' '}
        </div>
    );
};

export default Spinner;
