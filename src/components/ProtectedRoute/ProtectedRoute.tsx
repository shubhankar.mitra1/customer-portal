import { Navigate } from 'react-router-dom';
import { useAuth } from '../../hooks/useAuth';

type TreeProp = {
    children: JSX.Element;
};

type User = {
    user?: object;
};

export const ProtectedRoute = ({ children }: TreeProp) => {
    const { user }: User = useAuth();
    if (!user) {
        // user is not authenticated
        return <Navigate to="/" />;
    }
    return children;
};
