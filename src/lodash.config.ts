import lodash from 'lodash';
import deepdash from 'deepdash-es';

export default deepdash(lodash);
